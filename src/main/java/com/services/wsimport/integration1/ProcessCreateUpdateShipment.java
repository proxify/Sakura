
package com.services.wsimport.integration1;

import java.math.BigDecimal;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="apiVersionNumber" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="InitMsgList" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ActionCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ShipmentInformation" type="{http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/}ShipmentInformation"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "apiVersionNumber",
    "initMsgList",
    "actionCode",
    "shipmentInformation"
})
@XmlRootElement(name = "processCreateUpdateShipment")
public class ProcessCreateUpdateShipment {

    @XmlElement(required = true)
    protected BigDecimal apiVersionNumber;
    @XmlElementRef(name = "InitMsgList", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/types/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> initMsgList;
    @XmlElement(name = "ActionCode", required = true)
    protected String actionCode;
    @XmlElement(name = "ShipmentInformation", required = true)
    protected ShipmentInformation shipmentInformation;

    /**
     * Gets the value of the apiVersionNumber property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getApiVersionNumber() {
        return apiVersionNumber;
    }

    /**
     * Sets the value of the apiVersionNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setApiVersionNumber(BigDecimal value) {
        this.apiVersionNumber = value;
    }

    /**
     * Gets the value of the initMsgList property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getInitMsgList() {
        return initMsgList;
    }

    /**
     * Sets the value of the initMsgList property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setInitMsgList(JAXBElement<String> value) {
        this.initMsgList = value;
    }

    /**
     * Gets the value of the actionCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getActionCode() {
        return actionCode;
    }

    /**
     * Sets the value of the actionCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setActionCode(String value) {
        this.actionCode = value;
    }

    /**
     * Gets the value of the shipmentInformation property.
     * 
     * @return
     *     possible object is
     *     {@link ShipmentInformation }
     *     
     */
    public ShipmentInformation getShipmentInformation() {
        return shipmentInformation;
    }

    /**
     * Sets the value of the shipmentInformation property.
     * 
     * @param value
     *     allowed object is
     *     {@link ShipmentInformation }
     *     
     */
    public void setShipmentInformation(ShipmentInformation value) {
        this.shipmentInformation = value;
    }

}

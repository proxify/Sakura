
package com.services.wsimport.integration1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for InvLotInterface complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="InvLotInterface">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="LotNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SubinventoryCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Locator" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TransactionQuantity" type="{http://xmlns.oracle.com/adf/svc/types/}MeasureType" minOccurs="0"/>
 *         &lt;element name="SecondaryTransactionQuantity" type="{http://xmlns.oracle.com/adf/svc/types/}MeasureType" minOccurs="0"/>
 *         &lt;element name="Serials" type="{http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/}InvSerialsInterface" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "InvLotInterface", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", propOrder = {
    "lotNumber",
    "subinventoryCode",
    "locator",
    "transactionQuantity",
    "secondaryTransactionQuantity",
    "serials"
})
public class InvLotInterface {

    @XmlElement(name = "LotNumber")
    protected String lotNumber;
    @XmlElementRef(name = "SubinventoryCode", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> subinventoryCode;
    @XmlElementRef(name = "Locator", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> locator;
    @XmlElement(name = "TransactionQuantity")
    protected MeasureType transactionQuantity;
    @XmlElementRef(name = "SecondaryTransactionQuantity", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<MeasureType> secondaryTransactionQuantity;
    @XmlElement(name = "Serials")
    protected List<InvSerialsInterface> serials;

    /**
     * Gets the value of the lotNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLotNumber() {
        return lotNumber;
    }

    /**
     * Sets the value of the lotNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLotNumber(String value) {
        this.lotNumber = value;
    }

    /**
     * Gets the value of the subinventoryCode property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getSubinventoryCode() {
        return subinventoryCode;
    }

    /**
     * Sets the value of the subinventoryCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setSubinventoryCode(JAXBElement<String> value) {
        this.subinventoryCode = value;
    }

    /**
     * Gets the value of the locator property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getLocator() {
        return locator;
    }

    /**
     * Sets the value of the locator property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setLocator(JAXBElement<String> value) {
        this.locator = value;
    }

    /**
     * Gets the value of the transactionQuantity property.
     * 
     * @return
     *     possible object is
     *     {@link MeasureType }
     *     
     */
    public MeasureType getTransactionQuantity() {
        return transactionQuantity;
    }

    /**
     * Sets the value of the transactionQuantity property.
     * 
     * @param value
     *     allowed object is
     *     {@link MeasureType }
     *     
     */
    public void setTransactionQuantity(MeasureType value) {
        this.transactionQuantity = value;
    }

    /**
     * Gets the value of the secondaryTransactionQuantity property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link MeasureType }{@code >}
     *     
     */
    public JAXBElement<MeasureType> getSecondaryTransactionQuantity() {
        return secondaryTransactionQuantity;
    }

    /**
     * Sets the value of the secondaryTransactionQuantity property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link MeasureType }{@code >}
     *     
     */
    public void setSecondaryTransactionQuantity(JAXBElement<MeasureType> value) {
        this.secondaryTransactionQuantity = value;
    }

    /**
     * Gets the value of the serials property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the serials property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSerials().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link InvSerialsInterface }
     * 
     * 
     */
    public List<InvSerialsInterface> getSerials() {
        if (serials == null) {
            serials = new ArrayList<InvSerialsInterface>();
        }
        return this.serials;
    }

}

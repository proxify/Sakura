
package com.services.wsimport.integration1;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for ShipmentInformation complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ShipmentInformation">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Shipment" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ShipFromOrganization" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CustomerNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ShipToPartySiteNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Planned" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="InitialShipDate" type="{http://xmlns.oracle.com/adf/svc/types/}dateTime-Timestamp" minOccurs="0"/>
 *         &lt;element name="GrossWeight" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="NetWeight" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="WeightUOM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EquipmentType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Equipment" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Volume" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="VolumeUOM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CarrierPartyNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ServiceLevel" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ModeOfTransport" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Waybill" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SealNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PlannedDeliveryDate" type="{http://xmlns.oracle.com/adf/svc/types/}dateTime-Timestamp" minOccurs="0"/>
 *         &lt;element name="FreightTerms" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="LoadingSequenceRule" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FOB" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FOBSiteNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CODAmount" type="{http://xmlns.oracle.com/adf/svc/types/}AmountType" minOccurs="0"/>
 *         &lt;element name="CODCurrency" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CODPaidBy" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CODRemitTo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ConfirmedDate" type="{http://xmlns.oracle.com/adf/svc/types/}dateTime-Timestamp" minOccurs="0"/>
 *         &lt;element name="Description" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Dock" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AutomaticallyPack" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EnableAutoship" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ProblemContactReference" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ProrateWeight" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AttributeCategory" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Attribute1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Attribute2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Attribute3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Attribute4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Attribute5" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Attribute6" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Attribute7" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Attribute8" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Attribute9" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Attribute10" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Attribute11" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Attribute12" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Attribute13" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Attribute14" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Attribute15" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Attribute16" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Attribute17" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Attribute18" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Attribute19" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Attribute20" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AttributeNumber1" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="AttributeNumber2" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="AttributeNumber3" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="AttributeNumber4" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="AttributeNumber5" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="AttributeNumber6" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="AttributeNumber7" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="AttributeNumber8" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="AttributeNumber9" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="AttributeNumber10" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="AttributeDate1" type="{http://xmlns.oracle.com/adf/svc/types/}dateTime-Timestamp" minOccurs="0"/>
 *         &lt;element name="AttributeDate2" type="{http://xmlns.oracle.com/adf/svc/types/}dateTime-Timestamp" minOccurs="0"/>
 *         &lt;element name="AttributeDate3" type="{http://xmlns.oracle.com/adf/svc/types/}dateTime-Timestamp" minOccurs="0"/>
 *         &lt;element name="AttributeDate4" type="{http://xmlns.oracle.com/adf/svc/types/}dateTime-Timestamp" minOccurs="0"/>
 *         &lt;element name="AttributeDate5" type="{http://xmlns.oracle.com/adf/svc/types/}dateTime-Timestamp" minOccurs="0"/>
 *         &lt;element name="AttributeTimestamp1" type="{http://xmlns.oracle.com/adf/svc/types/}dateTime-Timestamp" minOccurs="0"/>
 *         &lt;element name="AttributeTimestamp2" type="{http://xmlns.oracle.com/adf/svc/types/}dateTime-Timestamp" minOccurs="0"/>
 *         &lt;element name="AttributeTimestamp3" type="{http://xmlns.oracle.com/adf/svc/types/}dateTime-Timestamp" minOccurs="0"/>
 *         &lt;element name="AttributeTimestamp4" type="{http://xmlns.oracle.com/adf/svc/types/}dateTime-Timestamp" minOccurs="0"/>
 *         &lt;element name="AttributeTimestamp5" type="{http://xmlns.oracle.com/adf/svc/types/}dateTime-Timestamp" minOccurs="0"/>
 *         &lt;element name="ShippingMarks" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="RoutingInstructions" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TransportationReason" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="GlobalAttribute1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="GlobalAttribute10" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="GlobalAttribute11" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="GlobalAttribute12" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="GlobalAttribute13" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="GlobalAttribute14" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="GlobalAttribute15" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="GlobalAttribute16" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="GlobalAttribute17" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="GlobalAttribute18" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="GlobalAttribute19" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="GlobalAttribute2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="GlobalAttribute20" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="GlobalAttribute3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="GlobalAttribute4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="GlobalAttribute5" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="GlobalAttribute6" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="GlobalAttribute7" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="GlobalAttribute8" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="GlobalAttribute9" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="GlobalAttributeCategory" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="GlobalAttributeDate1" type="{http://xmlns.oracle.com/adf/svc/types/}date-Date" minOccurs="0"/>
 *         &lt;element name="GlobalAttributeDate2" type="{http://xmlns.oracle.com/adf/svc/types/}date-Date" minOccurs="0"/>
 *         &lt;element name="GlobalAttributeDate3" type="{http://xmlns.oracle.com/adf/svc/types/}date-Date" minOccurs="0"/>
 *         &lt;element name="GlobalAttributeDate4" type="{http://xmlns.oracle.com/adf/svc/types/}date-Date" minOccurs="0"/>
 *         &lt;element name="GlobalAttributeDate5" type="{http://xmlns.oracle.com/adf/svc/types/}date-Date" minOccurs="0"/>
 *         &lt;element name="GlobalAttributeNumber1" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="GlobalAttributeNumber2" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="GlobalAttributeNumber3" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="GlobalAttributeNumber4" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="GlobalAttributeNumber5" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="ShipmentNotes" type="{http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/}ShipmentNotes" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ShipmentInformation", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", propOrder = {
    "shipment",
    "shipFromOrganization",
    "customerNumber",
    "shipToPartySiteNumber",
    "planned",
    "initialShipDate",
    "grossWeight",
    "netWeight",
    "weightUOM",
    "equipmentType",
    "equipment",
    "volume",
    "volumeUOM",
    "carrierPartyNumber",
    "serviceLevel",
    "modeOfTransport",
    "waybill",
    "sealNumber",
    "plannedDeliveryDate",
    "freightTerms",
    "loadingSequenceRule",
    "fob",
    "fobSiteNumber",
    "codAmount",
    "codCurrency",
    "codPaidBy",
    "codRemitTo",
    "confirmedDate",
    "description",
    "dock",
    "automaticallyPack",
    "enableAutoship",
    "problemContactReference",
    "prorateWeight",
    "attributeCategory",
    "attribute1",
    "attribute2",
    "attribute3",
    "attribute4",
    "attribute5",
    "attribute6",
    "attribute7",
    "attribute8",
    "attribute9",
    "attribute10",
    "attribute11",
    "attribute12",
    "attribute13",
    "attribute14",
    "attribute15",
    "attribute16",
    "attribute17",
    "attribute18",
    "attribute19",
    "attribute20",
    "attributeNumber1",
    "attributeNumber2",
    "attributeNumber3",
    "attributeNumber4",
    "attributeNumber5",
    "attributeNumber6",
    "attributeNumber7",
    "attributeNumber8",
    "attributeNumber9",
    "attributeNumber10",
    "attributeDate1",
    "attributeDate2",
    "attributeDate3",
    "attributeDate4",
    "attributeDate5",
    "attributeTimestamp1",
    "attributeTimestamp2",
    "attributeTimestamp3",
    "attributeTimestamp4",
    "attributeTimestamp5",
    "shippingMarks",
    "routingInstructions",
    "transportationReason",
    "globalAttribute1",
    "globalAttribute10",
    "globalAttribute11",
    "globalAttribute12",
    "globalAttribute13",
    "globalAttribute14",
    "globalAttribute15",
    "globalAttribute16",
    "globalAttribute17",
    "globalAttribute18",
    "globalAttribute19",
    "globalAttribute2",
    "globalAttribute20",
    "globalAttribute3",
    "globalAttribute4",
    "globalAttribute5",
    "globalAttribute6",
    "globalAttribute7",
    "globalAttribute8",
    "globalAttribute9",
    "globalAttributeCategory",
    "globalAttributeDate1",
    "globalAttributeDate2",
    "globalAttributeDate3",
    "globalAttributeDate4",
    "globalAttributeDate5",
    "globalAttributeNumber1",
    "globalAttributeNumber2",
    "globalAttributeNumber3",
    "globalAttributeNumber4",
    "globalAttributeNumber5",
    "shipmentNotes"
})
public class ShipmentInformation {

    @XmlElementRef(name = "Shipment", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> shipment;
    @XmlElementRef(name = "ShipFromOrganization", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> shipFromOrganization;
    @XmlElementRef(name = "CustomerNumber", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> customerNumber;
    @XmlElementRef(name = "ShipToPartySiteNumber", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> shipToPartySiteNumber;
    @XmlElementRef(name = "Planned", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> planned;
    @XmlElementRef(name = "InitialShipDate", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<XMLGregorianCalendar> initialShipDate;
    @XmlElementRef(name = "GrossWeight", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<BigDecimal> grossWeight;
    @XmlElementRef(name = "NetWeight", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<BigDecimal> netWeight;
    @XmlElementRef(name = "WeightUOM", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> weightUOM;
    @XmlElementRef(name = "EquipmentType", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> equipmentType;
    @XmlElementRef(name = "Equipment", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> equipment;
    @XmlElementRef(name = "Volume", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<BigDecimal> volume;
    @XmlElementRef(name = "VolumeUOM", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> volumeUOM;
    @XmlElementRef(name = "CarrierPartyNumber", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> carrierPartyNumber;
    @XmlElementRef(name = "ServiceLevel", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> serviceLevel;
    @XmlElementRef(name = "ModeOfTransport", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> modeOfTransport;
    @XmlElementRef(name = "Waybill", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> waybill;
    @XmlElementRef(name = "SealNumber", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> sealNumber;
    @XmlElementRef(name = "PlannedDeliveryDate", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<XMLGregorianCalendar> plannedDeliveryDate;
    @XmlElementRef(name = "FreightTerms", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> freightTerms;
    @XmlElementRef(name = "LoadingSequenceRule", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> loadingSequenceRule;
    @XmlElementRef(name = "FOB", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> fob;
    @XmlElementRef(name = "FOBSiteNumber", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> fobSiteNumber;
    @XmlElementRef(name = "CODAmount", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<AmountType> codAmount;
    @XmlElementRef(name = "CODCurrency", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> codCurrency;
    @XmlElementRef(name = "CODPaidBy", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> codPaidBy;
    @XmlElementRef(name = "CODRemitTo", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> codRemitTo;
    @XmlElementRef(name = "ConfirmedDate", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<XMLGregorianCalendar> confirmedDate;
    @XmlElementRef(name = "Description", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> description;
    @XmlElementRef(name = "Dock", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> dock;
    @XmlElementRef(name = "AutomaticallyPack", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> automaticallyPack;
    @XmlElementRef(name = "EnableAutoship", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> enableAutoship;
    @XmlElementRef(name = "ProblemContactReference", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> problemContactReference;
    @XmlElementRef(name = "ProrateWeight", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> prorateWeight;
    @XmlElementRef(name = "AttributeCategory", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> attributeCategory;
    @XmlElementRef(name = "Attribute1", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> attribute1;
    @XmlElementRef(name = "Attribute2", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> attribute2;
    @XmlElementRef(name = "Attribute3", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> attribute3;
    @XmlElementRef(name = "Attribute4", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> attribute4;
    @XmlElementRef(name = "Attribute5", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> attribute5;
    @XmlElementRef(name = "Attribute6", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> attribute6;
    @XmlElementRef(name = "Attribute7", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> attribute7;
    @XmlElementRef(name = "Attribute8", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> attribute8;
    @XmlElementRef(name = "Attribute9", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> attribute9;
    @XmlElementRef(name = "Attribute10", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> attribute10;
    @XmlElementRef(name = "Attribute11", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> attribute11;
    @XmlElementRef(name = "Attribute12", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> attribute12;
    @XmlElementRef(name = "Attribute13", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> attribute13;
    @XmlElementRef(name = "Attribute14", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> attribute14;
    @XmlElementRef(name = "Attribute15", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> attribute15;
    @XmlElementRef(name = "Attribute16", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> attribute16;
    @XmlElementRef(name = "Attribute17", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> attribute17;
    @XmlElementRef(name = "Attribute18", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> attribute18;
    @XmlElementRef(name = "Attribute19", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> attribute19;
    @XmlElementRef(name = "Attribute20", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> attribute20;
    @XmlElementRef(name = "AttributeNumber1", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<BigDecimal> attributeNumber1;
    @XmlElementRef(name = "AttributeNumber2", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<BigDecimal> attributeNumber2;
    @XmlElementRef(name = "AttributeNumber3", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<BigDecimal> attributeNumber3;
    @XmlElementRef(name = "AttributeNumber4", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<BigDecimal> attributeNumber4;
    @XmlElementRef(name = "AttributeNumber5", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<BigDecimal> attributeNumber5;
    @XmlElementRef(name = "AttributeNumber6", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<BigDecimal> attributeNumber6;
    @XmlElementRef(name = "AttributeNumber7", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<BigDecimal> attributeNumber7;
    @XmlElementRef(name = "AttributeNumber8", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<BigDecimal> attributeNumber8;
    @XmlElementRef(name = "AttributeNumber9", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<BigDecimal> attributeNumber9;
    @XmlElementRef(name = "AttributeNumber10", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<BigDecimal> attributeNumber10;
    @XmlElementRef(name = "AttributeDate1", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<XMLGregorianCalendar> attributeDate1;
    @XmlElementRef(name = "AttributeDate2", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<XMLGregorianCalendar> attributeDate2;
    @XmlElementRef(name = "AttributeDate3", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<XMLGregorianCalendar> attributeDate3;
    @XmlElementRef(name = "AttributeDate4", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<XMLGregorianCalendar> attributeDate4;
    @XmlElementRef(name = "AttributeDate5", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<XMLGregorianCalendar> attributeDate5;
    @XmlElementRef(name = "AttributeTimestamp1", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<XMLGregorianCalendar> attributeTimestamp1;
    @XmlElementRef(name = "AttributeTimestamp2", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<XMLGregorianCalendar> attributeTimestamp2;
    @XmlElementRef(name = "AttributeTimestamp3", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<XMLGregorianCalendar> attributeTimestamp3;
    @XmlElementRef(name = "AttributeTimestamp4", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<XMLGregorianCalendar> attributeTimestamp4;
    @XmlElementRef(name = "AttributeTimestamp5", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<XMLGregorianCalendar> attributeTimestamp5;
    @XmlElementRef(name = "ShippingMarks", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> shippingMarks;
    @XmlElementRef(name = "RoutingInstructions", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> routingInstructions;
    @XmlElementRef(name = "TransportationReason", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> transportationReason;
    @XmlElementRef(name = "GlobalAttribute1", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> globalAttribute1;
    @XmlElementRef(name = "GlobalAttribute10", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> globalAttribute10;
    @XmlElementRef(name = "GlobalAttribute11", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> globalAttribute11;
    @XmlElementRef(name = "GlobalAttribute12", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> globalAttribute12;
    @XmlElementRef(name = "GlobalAttribute13", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> globalAttribute13;
    @XmlElementRef(name = "GlobalAttribute14", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> globalAttribute14;
    @XmlElementRef(name = "GlobalAttribute15", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> globalAttribute15;
    @XmlElementRef(name = "GlobalAttribute16", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> globalAttribute16;
    @XmlElementRef(name = "GlobalAttribute17", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> globalAttribute17;
    @XmlElementRef(name = "GlobalAttribute18", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> globalAttribute18;
    @XmlElementRef(name = "GlobalAttribute19", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> globalAttribute19;
    @XmlElementRef(name = "GlobalAttribute2", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> globalAttribute2;
    @XmlElementRef(name = "GlobalAttribute20", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> globalAttribute20;
    @XmlElementRef(name = "GlobalAttribute3", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> globalAttribute3;
    @XmlElementRef(name = "GlobalAttribute4", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> globalAttribute4;
    @XmlElementRef(name = "GlobalAttribute5", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> globalAttribute5;
    @XmlElementRef(name = "GlobalAttribute6", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> globalAttribute6;
    @XmlElementRef(name = "GlobalAttribute7", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> globalAttribute7;
    @XmlElementRef(name = "GlobalAttribute8", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> globalAttribute8;
    @XmlElementRef(name = "GlobalAttribute9", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> globalAttribute9;
    @XmlElementRef(name = "GlobalAttributeCategory", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> globalAttributeCategory;
    @XmlElementRef(name = "GlobalAttributeDate1", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<XMLGregorianCalendar> globalAttributeDate1;
    @XmlElementRef(name = "GlobalAttributeDate2", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<XMLGregorianCalendar> globalAttributeDate2;
    @XmlElementRef(name = "GlobalAttributeDate3", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<XMLGregorianCalendar> globalAttributeDate3;
    @XmlElementRef(name = "GlobalAttributeDate4", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<XMLGregorianCalendar> globalAttributeDate4;
    @XmlElementRef(name = "GlobalAttributeDate5", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<XMLGregorianCalendar> globalAttributeDate5;
    @XmlElementRef(name = "GlobalAttributeNumber1", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<BigDecimal> globalAttributeNumber1;
    @XmlElementRef(name = "GlobalAttributeNumber2", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<BigDecimal> globalAttributeNumber2;
    @XmlElementRef(name = "GlobalAttributeNumber3", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<BigDecimal> globalAttributeNumber3;
    @XmlElementRef(name = "GlobalAttributeNumber4", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<BigDecimal> globalAttributeNumber4;
    @XmlElementRef(name = "GlobalAttributeNumber5", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<BigDecimal> globalAttributeNumber5;
    @XmlElement(name = "ShipmentNotes")
    protected List<ShipmentNotes> shipmentNotes;

    /**
     * Gets the value of the shipment property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getShipment() {
        return shipment;
    }

    /**
     * Sets the value of the shipment property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setShipment(JAXBElement<String> value) {
        this.shipment = value;
    }

    /**
     * Gets the value of the shipFromOrganization property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getShipFromOrganization() {
        return shipFromOrganization;
    }

    /**
     * Sets the value of the shipFromOrganization property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setShipFromOrganization(JAXBElement<String> value) {
        this.shipFromOrganization = value;
    }

    /**
     * Gets the value of the customerNumber property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCustomerNumber() {
        return customerNumber;
    }

    /**
     * Sets the value of the customerNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCustomerNumber(JAXBElement<String> value) {
        this.customerNumber = value;
    }

    /**
     * Gets the value of the shipToPartySiteNumber property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getShipToPartySiteNumber() {
        return shipToPartySiteNumber;
    }

    /**
     * Sets the value of the shipToPartySiteNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setShipToPartySiteNumber(JAXBElement<String> value) {
        this.shipToPartySiteNumber = value;
    }

    /**
     * Gets the value of the planned property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPlanned() {
        return planned;
    }

    /**
     * Sets the value of the planned property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPlanned(JAXBElement<String> value) {
        this.planned = value;
    }

    /**
     * Gets the value of the initialShipDate property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public JAXBElement<XMLGregorianCalendar> getInitialShipDate() {
        return initialShipDate;
    }

    /**
     * Sets the value of the initialShipDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public void setInitialShipDate(JAXBElement<XMLGregorianCalendar> value) {
        this.initialShipDate = value;
    }

    /**
     * Gets the value of the grossWeight property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}
     *     
     */
    public JAXBElement<BigDecimal> getGrossWeight() {
        return grossWeight;
    }

    /**
     * Sets the value of the grossWeight property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}
     *     
     */
    public void setGrossWeight(JAXBElement<BigDecimal> value) {
        this.grossWeight = value;
    }

    /**
     * Gets the value of the netWeight property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}
     *     
     */
    public JAXBElement<BigDecimal> getNetWeight() {
        return netWeight;
    }

    /**
     * Sets the value of the netWeight property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}
     *     
     */
    public void setNetWeight(JAXBElement<BigDecimal> value) {
        this.netWeight = value;
    }

    /**
     * Gets the value of the weightUOM property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getWeightUOM() {
        return weightUOM;
    }

    /**
     * Sets the value of the weightUOM property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setWeightUOM(JAXBElement<String> value) {
        this.weightUOM = value;
    }

    /**
     * Gets the value of the equipmentType property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getEquipmentType() {
        return equipmentType;
    }

    /**
     * Sets the value of the equipmentType property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setEquipmentType(JAXBElement<String> value) {
        this.equipmentType = value;
    }

    /**
     * Gets the value of the equipment property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getEquipment() {
        return equipment;
    }

    /**
     * Sets the value of the equipment property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setEquipment(JAXBElement<String> value) {
        this.equipment = value;
    }

    /**
     * Gets the value of the volume property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}
     *     
     */
    public JAXBElement<BigDecimal> getVolume() {
        return volume;
    }

    /**
     * Sets the value of the volume property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}
     *     
     */
    public void setVolume(JAXBElement<BigDecimal> value) {
        this.volume = value;
    }

    /**
     * Gets the value of the volumeUOM property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getVolumeUOM() {
        return volumeUOM;
    }

    /**
     * Sets the value of the volumeUOM property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setVolumeUOM(JAXBElement<String> value) {
        this.volumeUOM = value;
    }

    /**
     * Gets the value of the carrierPartyNumber property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCarrierPartyNumber() {
        return carrierPartyNumber;
    }

    /**
     * Sets the value of the carrierPartyNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCarrierPartyNumber(JAXBElement<String> value) {
        this.carrierPartyNumber = value;
    }

    /**
     * Gets the value of the serviceLevel property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getServiceLevel() {
        return serviceLevel;
    }

    /**
     * Sets the value of the serviceLevel property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setServiceLevel(JAXBElement<String> value) {
        this.serviceLevel = value;
    }

    /**
     * Gets the value of the modeOfTransport property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getModeOfTransport() {
        return modeOfTransport;
    }

    /**
     * Sets the value of the modeOfTransport property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setModeOfTransport(JAXBElement<String> value) {
        this.modeOfTransport = value;
    }

    /**
     * Gets the value of the waybill property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getWaybill() {
        return waybill;
    }

    /**
     * Sets the value of the waybill property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setWaybill(JAXBElement<String> value) {
        this.waybill = value;
    }

    /**
     * Gets the value of the sealNumber property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getSealNumber() {
        return sealNumber;
    }

    /**
     * Sets the value of the sealNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setSealNumber(JAXBElement<String> value) {
        this.sealNumber = value;
    }

    /**
     * Gets the value of the plannedDeliveryDate property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public JAXBElement<XMLGregorianCalendar> getPlannedDeliveryDate() {
        return plannedDeliveryDate;
    }

    /**
     * Sets the value of the plannedDeliveryDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public void setPlannedDeliveryDate(JAXBElement<XMLGregorianCalendar> value) {
        this.plannedDeliveryDate = value;
    }

    /**
     * Gets the value of the freightTerms property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getFreightTerms() {
        return freightTerms;
    }

    /**
     * Sets the value of the freightTerms property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setFreightTerms(JAXBElement<String> value) {
        this.freightTerms = value;
    }

    /**
     * Gets the value of the loadingSequenceRule property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getLoadingSequenceRule() {
        return loadingSequenceRule;
    }

    /**
     * Sets the value of the loadingSequenceRule property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setLoadingSequenceRule(JAXBElement<String> value) {
        this.loadingSequenceRule = value;
    }

    /**
     * Gets the value of the fob property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getFOB() {
        return fob;
    }

    /**
     * Sets the value of the fob property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setFOB(JAXBElement<String> value) {
        this.fob = value;
    }

    /**
     * Gets the value of the fobSiteNumber property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getFOBSiteNumber() {
        return fobSiteNumber;
    }

    /**
     * Sets the value of the fobSiteNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setFOBSiteNumber(JAXBElement<String> value) {
        this.fobSiteNumber = value;
    }

    /**
     * Gets the value of the codAmount property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link AmountType }{@code >}
     *     
     */
    public JAXBElement<AmountType> getCODAmount() {
        return codAmount;
    }

    /**
     * Sets the value of the codAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link AmountType }{@code >}
     *     
     */
    public void setCODAmount(JAXBElement<AmountType> value) {
        this.codAmount = value;
    }

    /**
     * Gets the value of the codCurrency property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCODCurrency() {
        return codCurrency;
    }

    /**
     * Sets the value of the codCurrency property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCODCurrency(JAXBElement<String> value) {
        this.codCurrency = value;
    }

    /**
     * Gets the value of the codPaidBy property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCODPaidBy() {
        return codPaidBy;
    }

    /**
     * Sets the value of the codPaidBy property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCODPaidBy(JAXBElement<String> value) {
        this.codPaidBy = value;
    }

    /**
     * Gets the value of the codRemitTo property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCODRemitTo() {
        return codRemitTo;
    }

    /**
     * Sets the value of the codRemitTo property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCODRemitTo(JAXBElement<String> value) {
        this.codRemitTo = value;
    }

    /**
     * Gets the value of the confirmedDate property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public JAXBElement<XMLGregorianCalendar> getConfirmedDate() {
        return confirmedDate;
    }

    /**
     * Sets the value of the confirmedDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public void setConfirmedDate(JAXBElement<XMLGregorianCalendar> value) {
        this.confirmedDate = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDescription(JAXBElement<String> value) {
        this.description = value;
    }

    /**
     * Gets the value of the dock property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDock() {
        return dock;
    }

    /**
     * Sets the value of the dock property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDock(JAXBElement<String> value) {
        this.dock = value;
    }

    /**
     * Gets the value of the automaticallyPack property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getAutomaticallyPack() {
        return automaticallyPack;
    }

    /**
     * Sets the value of the automaticallyPack property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setAutomaticallyPack(JAXBElement<String> value) {
        this.automaticallyPack = value;
    }

    /**
     * Gets the value of the enableAutoship property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getEnableAutoship() {
        return enableAutoship;
    }

    /**
     * Sets the value of the enableAutoship property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setEnableAutoship(JAXBElement<String> value) {
        this.enableAutoship = value;
    }

    /**
     * Gets the value of the problemContactReference property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getProblemContactReference() {
        return problemContactReference;
    }

    /**
     * Sets the value of the problemContactReference property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setProblemContactReference(JAXBElement<String> value) {
        this.problemContactReference = value;
    }

    /**
     * Gets the value of the prorateWeight property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getProrateWeight() {
        return prorateWeight;
    }

    /**
     * Sets the value of the prorateWeight property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setProrateWeight(JAXBElement<String> value) {
        this.prorateWeight = value;
    }

    /**
     * Gets the value of the attributeCategory property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getAttributeCategory() {
        return attributeCategory;
    }

    /**
     * Sets the value of the attributeCategory property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setAttributeCategory(JAXBElement<String> value) {
        this.attributeCategory = value;
    }

    /**
     * Gets the value of the attribute1 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getAttribute1() {
        return attribute1;
    }

    /**
     * Sets the value of the attribute1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setAttribute1(JAXBElement<String> value) {
        this.attribute1 = value;
    }

    /**
     * Gets the value of the attribute2 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getAttribute2() {
        return attribute2;
    }

    /**
     * Sets the value of the attribute2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setAttribute2(JAXBElement<String> value) {
        this.attribute2 = value;
    }

    /**
     * Gets the value of the attribute3 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getAttribute3() {
        return attribute3;
    }

    /**
     * Sets the value of the attribute3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setAttribute3(JAXBElement<String> value) {
        this.attribute3 = value;
    }

    /**
     * Gets the value of the attribute4 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getAttribute4() {
        return attribute4;
    }

    /**
     * Sets the value of the attribute4 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setAttribute4(JAXBElement<String> value) {
        this.attribute4 = value;
    }

    /**
     * Gets the value of the attribute5 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getAttribute5() {
        return attribute5;
    }

    /**
     * Sets the value of the attribute5 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setAttribute5(JAXBElement<String> value) {
        this.attribute5 = value;
    }

    /**
     * Gets the value of the attribute6 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getAttribute6() {
        return attribute6;
    }

    /**
     * Sets the value of the attribute6 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setAttribute6(JAXBElement<String> value) {
        this.attribute6 = value;
    }

    /**
     * Gets the value of the attribute7 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getAttribute7() {
        return attribute7;
    }

    /**
     * Sets the value of the attribute7 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setAttribute7(JAXBElement<String> value) {
        this.attribute7 = value;
    }

    /**
     * Gets the value of the attribute8 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getAttribute8() {
        return attribute8;
    }

    /**
     * Sets the value of the attribute8 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setAttribute8(JAXBElement<String> value) {
        this.attribute8 = value;
    }

    /**
     * Gets the value of the attribute9 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getAttribute9() {
        return attribute9;
    }

    /**
     * Sets the value of the attribute9 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setAttribute9(JAXBElement<String> value) {
        this.attribute9 = value;
    }

    /**
     * Gets the value of the attribute10 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getAttribute10() {
        return attribute10;
    }

    /**
     * Sets the value of the attribute10 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setAttribute10(JAXBElement<String> value) {
        this.attribute10 = value;
    }

    /**
     * Gets the value of the attribute11 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getAttribute11() {
        return attribute11;
    }

    /**
     * Sets the value of the attribute11 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setAttribute11(JAXBElement<String> value) {
        this.attribute11 = value;
    }

    /**
     * Gets the value of the attribute12 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getAttribute12() {
        return attribute12;
    }

    /**
     * Sets the value of the attribute12 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setAttribute12(JAXBElement<String> value) {
        this.attribute12 = value;
    }

    /**
     * Gets the value of the attribute13 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getAttribute13() {
        return attribute13;
    }

    /**
     * Sets the value of the attribute13 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setAttribute13(JAXBElement<String> value) {
        this.attribute13 = value;
    }

    /**
     * Gets the value of the attribute14 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getAttribute14() {
        return attribute14;
    }

    /**
     * Sets the value of the attribute14 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setAttribute14(JAXBElement<String> value) {
        this.attribute14 = value;
    }

    /**
     * Gets the value of the attribute15 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getAttribute15() {
        return attribute15;
    }

    /**
     * Sets the value of the attribute15 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setAttribute15(JAXBElement<String> value) {
        this.attribute15 = value;
    }

    /**
     * Gets the value of the attribute16 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getAttribute16() {
        return attribute16;
    }

    /**
     * Sets the value of the attribute16 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setAttribute16(JAXBElement<String> value) {
        this.attribute16 = value;
    }

    /**
     * Gets the value of the attribute17 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getAttribute17() {
        return attribute17;
    }

    /**
     * Sets the value of the attribute17 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setAttribute17(JAXBElement<String> value) {
        this.attribute17 = value;
    }

    /**
     * Gets the value of the attribute18 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getAttribute18() {
        return attribute18;
    }

    /**
     * Sets the value of the attribute18 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setAttribute18(JAXBElement<String> value) {
        this.attribute18 = value;
    }

    /**
     * Gets the value of the attribute19 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getAttribute19() {
        return attribute19;
    }

    /**
     * Sets the value of the attribute19 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setAttribute19(JAXBElement<String> value) {
        this.attribute19 = value;
    }

    /**
     * Gets the value of the attribute20 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getAttribute20() {
        return attribute20;
    }

    /**
     * Sets the value of the attribute20 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setAttribute20(JAXBElement<String> value) {
        this.attribute20 = value;
    }

    /**
     * Gets the value of the attributeNumber1 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}
     *     
     */
    public JAXBElement<BigDecimal> getAttributeNumber1() {
        return attributeNumber1;
    }

    /**
     * Sets the value of the attributeNumber1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}
     *     
     */
    public void setAttributeNumber1(JAXBElement<BigDecimal> value) {
        this.attributeNumber1 = value;
    }

    /**
     * Gets the value of the attributeNumber2 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}
     *     
     */
    public JAXBElement<BigDecimal> getAttributeNumber2() {
        return attributeNumber2;
    }

    /**
     * Sets the value of the attributeNumber2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}
     *     
     */
    public void setAttributeNumber2(JAXBElement<BigDecimal> value) {
        this.attributeNumber2 = value;
    }

    /**
     * Gets the value of the attributeNumber3 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}
     *     
     */
    public JAXBElement<BigDecimal> getAttributeNumber3() {
        return attributeNumber3;
    }

    /**
     * Sets the value of the attributeNumber3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}
     *     
     */
    public void setAttributeNumber3(JAXBElement<BigDecimal> value) {
        this.attributeNumber3 = value;
    }

    /**
     * Gets the value of the attributeNumber4 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}
     *     
     */
    public JAXBElement<BigDecimal> getAttributeNumber4() {
        return attributeNumber4;
    }

    /**
     * Sets the value of the attributeNumber4 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}
     *     
     */
    public void setAttributeNumber4(JAXBElement<BigDecimal> value) {
        this.attributeNumber4 = value;
    }

    /**
     * Gets the value of the attributeNumber5 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}
     *     
     */
    public JAXBElement<BigDecimal> getAttributeNumber5() {
        return attributeNumber5;
    }

    /**
     * Sets the value of the attributeNumber5 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}
     *     
     */
    public void setAttributeNumber5(JAXBElement<BigDecimal> value) {
        this.attributeNumber5 = value;
    }

    /**
     * Gets the value of the attributeNumber6 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}
     *     
     */
    public JAXBElement<BigDecimal> getAttributeNumber6() {
        return attributeNumber6;
    }

    /**
     * Sets the value of the attributeNumber6 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}
     *     
     */
    public void setAttributeNumber6(JAXBElement<BigDecimal> value) {
        this.attributeNumber6 = value;
    }

    /**
     * Gets the value of the attributeNumber7 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}
     *     
     */
    public JAXBElement<BigDecimal> getAttributeNumber7() {
        return attributeNumber7;
    }

    /**
     * Sets the value of the attributeNumber7 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}
     *     
     */
    public void setAttributeNumber7(JAXBElement<BigDecimal> value) {
        this.attributeNumber7 = value;
    }

    /**
     * Gets the value of the attributeNumber8 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}
     *     
     */
    public JAXBElement<BigDecimal> getAttributeNumber8() {
        return attributeNumber8;
    }

    /**
     * Sets the value of the attributeNumber8 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}
     *     
     */
    public void setAttributeNumber8(JAXBElement<BigDecimal> value) {
        this.attributeNumber8 = value;
    }

    /**
     * Gets the value of the attributeNumber9 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}
     *     
     */
    public JAXBElement<BigDecimal> getAttributeNumber9() {
        return attributeNumber9;
    }

    /**
     * Sets the value of the attributeNumber9 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}
     *     
     */
    public void setAttributeNumber9(JAXBElement<BigDecimal> value) {
        this.attributeNumber9 = value;
    }

    /**
     * Gets the value of the attributeNumber10 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}
     *     
     */
    public JAXBElement<BigDecimal> getAttributeNumber10() {
        return attributeNumber10;
    }

    /**
     * Sets the value of the attributeNumber10 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}
     *     
     */
    public void setAttributeNumber10(JAXBElement<BigDecimal> value) {
        this.attributeNumber10 = value;
    }

    /**
     * Gets the value of the attributeDate1 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public JAXBElement<XMLGregorianCalendar> getAttributeDate1() {
        return attributeDate1;
    }

    /**
     * Sets the value of the attributeDate1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public void setAttributeDate1(JAXBElement<XMLGregorianCalendar> value) {
        this.attributeDate1 = value;
    }

    /**
     * Gets the value of the attributeDate2 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public JAXBElement<XMLGregorianCalendar> getAttributeDate2() {
        return attributeDate2;
    }

    /**
     * Sets the value of the attributeDate2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public void setAttributeDate2(JAXBElement<XMLGregorianCalendar> value) {
        this.attributeDate2 = value;
    }

    /**
     * Gets the value of the attributeDate3 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public JAXBElement<XMLGregorianCalendar> getAttributeDate3() {
        return attributeDate3;
    }

    /**
     * Sets the value of the attributeDate3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public void setAttributeDate3(JAXBElement<XMLGregorianCalendar> value) {
        this.attributeDate3 = value;
    }

    /**
     * Gets the value of the attributeDate4 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public JAXBElement<XMLGregorianCalendar> getAttributeDate4() {
        return attributeDate4;
    }

    /**
     * Sets the value of the attributeDate4 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public void setAttributeDate4(JAXBElement<XMLGregorianCalendar> value) {
        this.attributeDate4 = value;
    }

    /**
     * Gets the value of the attributeDate5 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public JAXBElement<XMLGregorianCalendar> getAttributeDate5() {
        return attributeDate5;
    }

    /**
     * Sets the value of the attributeDate5 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public void setAttributeDate5(JAXBElement<XMLGregorianCalendar> value) {
        this.attributeDate5 = value;
    }

    /**
     * Gets the value of the attributeTimestamp1 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public JAXBElement<XMLGregorianCalendar> getAttributeTimestamp1() {
        return attributeTimestamp1;
    }

    /**
     * Sets the value of the attributeTimestamp1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public void setAttributeTimestamp1(JAXBElement<XMLGregorianCalendar> value) {
        this.attributeTimestamp1 = value;
    }

    /**
     * Gets the value of the attributeTimestamp2 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public JAXBElement<XMLGregorianCalendar> getAttributeTimestamp2() {
        return attributeTimestamp2;
    }

    /**
     * Sets the value of the attributeTimestamp2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public void setAttributeTimestamp2(JAXBElement<XMLGregorianCalendar> value) {
        this.attributeTimestamp2 = value;
    }

    /**
     * Gets the value of the attributeTimestamp3 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public JAXBElement<XMLGregorianCalendar> getAttributeTimestamp3() {
        return attributeTimestamp3;
    }

    /**
     * Sets the value of the attributeTimestamp3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public void setAttributeTimestamp3(JAXBElement<XMLGregorianCalendar> value) {
        this.attributeTimestamp3 = value;
    }

    /**
     * Gets the value of the attributeTimestamp4 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public JAXBElement<XMLGregorianCalendar> getAttributeTimestamp4() {
        return attributeTimestamp4;
    }

    /**
     * Sets the value of the attributeTimestamp4 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public void setAttributeTimestamp4(JAXBElement<XMLGregorianCalendar> value) {
        this.attributeTimestamp4 = value;
    }

    /**
     * Gets the value of the attributeTimestamp5 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public JAXBElement<XMLGregorianCalendar> getAttributeTimestamp5() {
        return attributeTimestamp5;
    }

    /**
     * Sets the value of the attributeTimestamp5 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public void setAttributeTimestamp5(JAXBElement<XMLGregorianCalendar> value) {
        this.attributeTimestamp5 = value;
    }

    /**
     * Gets the value of the shippingMarks property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getShippingMarks() {
        return shippingMarks;
    }

    /**
     * Sets the value of the shippingMarks property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setShippingMarks(JAXBElement<String> value) {
        this.shippingMarks = value;
    }

    /**
     * Gets the value of the routingInstructions property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getRoutingInstructions() {
        return routingInstructions;
    }

    /**
     * Sets the value of the routingInstructions property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setRoutingInstructions(JAXBElement<String> value) {
        this.routingInstructions = value;
    }

    /**
     * Gets the value of the transportationReason property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getTransportationReason() {
        return transportationReason;
    }

    /**
     * Sets the value of the transportationReason property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setTransportationReason(JAXBElement<String> value) {
        this.transportationReason = value;
    }

    /**
     * Gets the value of the globalAttribute1 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getGlobalAttribute1() {
        return globalAttribute1;
    }

    /**
     * Sets the value of the globalAttribute1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setGlobalAttribute1(JAXBElement<String> value) {
        this.globalAttribute1 = value;
    }

    /**
     * Gets the value of the globalAttribute10 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getGlobalAttribute10() {
        return globalAttribute10;
    }

    /**
     * Sets the value of the globalAttribute10 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setGlobalAttribute10(JAXBElement<String> value) {
        this.globalAttribute10 = value;
    }

    /**
     * Gets the value of the globalAttribute11 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getGlobalAttribute11() {
        return globalAttribute11;
    }

    /**
     * Sets the value of the globalAttribute11 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setGlobalAttribute11(JAXBElement<String> value) {
        this.globalAttribute11 = value;
    }

    /**
     * Gets the value of the globalAttribute12 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getGlobalAttribute12() {
        return globalAttribute12;
    }

    /**
     * Sets the value of the globalAttribute12 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setGlobalAttribute12(JAXBElement<String> value) {
        this.globalAttribute12 = value;
    }

    /**
     * Gets the value of the globalAttribute13 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getGlobalAttribute13() {
        return globalAttribute13;
    }

    /**
     * Sets the value of the globalAttribute13 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setGlobalAttribute13(JAXBElement<String> value) {
        this.globalAttribute13 = value;
    }

    /**
     * Gets the value of the globalAttribute14 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getGlobalAttribute14() {
        return globalAttribute14;
    }

    /**
     * Sets the value of the globalAttribute14 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setGlobalAttribute14(JAXBElement<String> value) {
        this.globalAttribute14 = value;
    }

    /**
     * Gets the value of the globalAttribute15 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getGlobalAttribute15() {
        return globalAttribute15;
    }

    /**
     * Sets the value of the globalAttribute15 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setGlobalAttribute15(JAXBElement<String> value) {
        this.globalAttribute15 = value;
    }

    /**
     * Gets the value of the globalAttribute16 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getGlobalAttribute16() {
        return globalAttribute16;
    }

    /**
     * Sets the value of the globalAttribute16 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setGlobalAttribute16(JAXBElement<String> value) {
        this.globalAttribute16 = value;
    }

    /**
     * Gets the value of the globalAttribute17 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getGlobalAttribute17() {
        return globalAttribute17;
    }

    /**
     * Sets the value of the globalAttribute17 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setGlobalAttribute17(JAXBElement<String> value) {
        this.globalAttribute17 = value;
    }

    /**
     * Gets the value of the globalAttribute18 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getGlobalAttribute18() {
        return globalAttribute18;
    }

    /**
     * Sets the value of the globalAttribute18 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setGlobalAttribute18(JAXBElement<String> value) {
        this.globalAttribute18 = value;
    }

    /**
     * Gets the value of the globalAttribute19 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getGlobalAttribute19() {
        return globalAttribute19;
    }

    /**
     * Sets the value of the globalAttribute19 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setGlobalAttribute19(JAXBElement<String> value) {
        this.globalAttribute19 = value;
    }

    /**
     * Gets the value of the globalAttribute2 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getGlobalAttribute2() {
        return globalAttribute2;
    }

    /**
     * Sets the value of the globalAttribute2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setGlobalAttribute2(JAXBElement<String> value) {
        this.globalAttribute2 = value;
    }

    /**
     * Gets the value of the globalAttribute20 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getGlobalAttribute20() {
        return globalAttribute20;
    }

    /**
     * Sets the value of the globalAttribute20 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setGlobalAttribute20(JAXBElement<String> value) {
        this.globalAttribute20 = value;
    }

    /**
     * Gets the value of the globalAttribute3 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getGlobalAttribute3() {
        return globalAttribute3;
    }

    /**
     * Sets the value of the globalAttribute3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setGlobalAttribute3(JAXBElement<String> value) {
        this.globalAttribute3 = value;
    }

    /**
     * Gets the value of the globalAttribute4 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getGlobalAttribute4() {
        return globalAttribute4;
    }

    /**
     * Sets the value of the globalAttribute4 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setGlobalAttribute4(JAXBElement<String> value) {
        this.globalAttribute4 = value;
    }

    /**
     * Gets the value of the globalAttribute5 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getGlobalAttribute5() {
        return globalAttribute5;
    }

    /**
     * Sets the value of the globalAttribute5 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setGlobalAttribute5(JAXBElement<String> value) {
        this.globalAttribute5 = value;
    }

    /**
     * Gets the value of the globalAttribute6 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getGlobalAttribute6() {
        return globalAttribute6;
    }

    /**
     * Sets the value of the globalAttribute6 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setGlobalAttribute6(JAXBElement<String> value) {
        this.globalAttribute6 = value;
    }

    /**
     * Gets the value of the globalAttribute7 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getGlobalAttribute7() {
        return globalAttribute7;
    }

    /**
     * Sets the value of the globalAttribute7 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setGlobalAttribute7(JAXBElement<String> value) {
        this.globalAttribute7 = value;
    }

    /**
     * Gets the value of the globalAttribute8 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getGlobalAttribute8() {
        return globalAttribute8;
    }

    /**
     * Sets the value of the globalAttribute8 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setGlobalAttribute8(JAXBElement<String> value) {
        this.globalAttribute8 = value;
    }

    /**
     * Gets the value of the globalAttribute9 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getGlobalAttribute9() {
        return globalAttribute9;
    }

    /**
     * Sets the value of the globalAttribute9 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setGlobalAttribute9(JAXBElement<String> value) {
        this.globalAttribute9 = value;
    }

    /**
     * Gets the value of the globalAttributeCategory property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getGlobalAttributeCategory() {
        return globalAttributeCategory;
    }

    /**
     * Sets the value of the globalAttributeCategory property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setGlobalAttributeCategory(JAXBElement<String> value) {
        this.globalAttributeCategory = value;
    }

    /**
     * Gets the value of the globalAttributeDate1 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public JAXBElement<XMLGregorianCalendar> getGlobalAttributeDate1() {
        return globalAttributeDate1;
    }

    /**
     * Sets the value of the globalAttributeDate1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public void setGlobalAttributeDate1(JAXBElement<XMLGregorianCalendar> value) {
        this.globalAttributeDate1 = value;
    }

    /**
     * Gets the value of the globalAttributeDate2 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public JAXBElement<XMLGregorianCalendar> getGlobalAttributeDate2() {
        return globalAttributeDate2;
    }

    /**
     * Sets the value of the globalAttributeDate2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public void setGlobalAttributeDate2(JAXBElement<XMLGregorianCalendar> value) {
        this.globalAttributeDate2 = value;
    }

    /**
     * Gets the value of the globalAttributeDate3 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public JAXBElement<XMLGregorianCalendar> getGlobalAttributeDate3() {
        return globalAttributeDate3;
    }

    /**
     * Sets the value of the globalAttributeDate3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public void setGlobalAttributeDate3(JAXBElement<XMLGregorianCalendar> value) {
        this.globalAttributeDate3 = value;
    }

    /**
     * Gets the value of the globalAttributeDate4 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public JAXBElement<XMLGregorianCalendar> getGlobalAttributeDate4() {
        return globalAttributeDate4;
    }

    /**
     * Sets the value of the globalAttributeDate4 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public void setGlobalAttributeDate4(JAXBElement<XMLGregorianCalendar> value) {
        this.globalAttributeDate4 = value;
    }

    /**
     * Gets the value of the globalAttributeDate5 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public JAXBElement<XMLGregorianCalendar> getGlobalAttributeDate5() {
        return globalAttributeDate5;
    }

    /**
     * Sets the value of the globalAttributeDate5 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public void setGlobalAttributeDate5(JAXBElement<XMLGregorianCalendar> value) {
        this.globalAttributeDate5 = value;
    }

    /**
     * Gets the value of the globalAttributeNumber1 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}
     *     
     */
    public JAXBElement<BigDecimal> getGlobalAttributeNumber1() {
        return globalAttributeNumber1;
    }

    /**
     * Sets the value of the globalAttributeNumber1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}
     *     
     */
    public void setGlobalAttributeNumber1(JAXBElement<BigDecimal> value) {
        this.globalAttributeNumber1 = value;
    }

    /**
     * Gets the value of the globalAttributeNumber2 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}
     *     
     */
    public JAXBElement<BigDecimal> getGlobalAttributeNumber2() {
        return globalAttributeNumber2;
    }

    /**
     * Sets the value of the globalAttributeNumber2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}
     *     
     */
    public void setGlobalAttributeNumber2(JAXBElement<BigDecimal> value) {
        this.globalAttributeNumber2 = value;
    }

    /**
     * Gets the value of the globalAttributeNumber3 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}
     *     
     */
    public JAXBElement<BigDecimal> getGlobalAttributeNumber3() {
        return globalAttributeNumber3;
    }

    /**
     * Sets the value of the globalAttributeNumber3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}
     *     
     */
    public void setGlobalAttributeNumber3(JAXBElement<BigDecimal> value) {
        this.globalAttributeNumber3 = value;
    }

    /**
     * Gets the value of the globalAttributeNumber4 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}
     *     
     */
    public JAXBElement<BigDecimal> getGlobalAttributeNumber4() {
        return globalAttributeNumber4;
    }

    /**
     * Sets the value of the globalAttributeNumber4 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}
     *     
     */
    public void setGlobalAttributeNumber4(JAXBElement<BigDecimal> value) {
        this.globalAttributeNumber4 = value;
    }

    /**
     * Gets the value of the globalAttributeNumber5 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}
     *     
     */
    public JAXBElement<BigDecimal> getGlobalAttributeNumber5() {
        return globalAttributeNumber5;
    }

    /**
     * Sets the value of the globalAttributeNumber5 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}
     *     
     */
    public void setGlobalAttributeNumber5(JAXBElement<BigDecimal> value) {
        this.globalAttributeNumber5 = value;
    }

    /**
     * Gets the value of the shipmentNotes property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the shipmentNotes property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getShipmentNotes().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ShipmentNotes }
     * 
     * 
     */
    public List<ShipmentNotes> getShipmentNotes() {
        if (shipmentNotes == null) {
            shipmentNotes = new ArrayList<ShipmentNotes>();
        }
        return this.shipmentNotes;
    }

}


package com.services.wsimport.integration1;

import java.math.BigDecimal;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.services.wsimport.integration1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _ProcessControl_QNAME = new QName("http://xmlns.oracle.com/adf/svc/types/", "processControl");
    private final static QName _InvLotInterface_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "invLotInterface");
    private final static QName _ShipmentInterfaceResult_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "shipmentInterfaceResult");
    private final static QName _ShipmentInformation_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "shipmentInformation");
    private final static QName _Fault_QNAME = new QName("http://xmlns.oracle.com/oracleas/schema/oracle-fault-11_0", "Fault");
    private final static QName _Types_QNAME = new QName("commonj.sdo", "types");
    private final static QName _Datagraph_QNAME = new QName("commonj.sdo", "datagraph");
    private final static QName _FreightCostInterfaceResult_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "freightCostInterfaceResult");
    private final static QName _ErrorMessage_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "errorMessage");
    private final static QName _ShippingCost_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "shippingCost");
    private final static QName _InvSerialsInterface_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "invSerialsInterface");
    private final static QName _InvLotInterfaceResult_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "invLotInterfaceResult");
    private final static QName _InvSerialsInterfaceResult_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "invSerialsInterfaceResult");
    private final static QName _ShipmentLineInterfaceResult_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "shipmentLineInterfaceResult");
    private final static QName _ShipmentDocumentResponseLine_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "shipmentDocumentResponseLine");
    private final static QName _ContainerInterface_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "containerInterface");
    private final static QName _FindControl_QNAME = new QName("http://xmlns.oracle.com/adf/svc/types/", "findControl");
    private final static QName _Type_QNAME = new QName("commonj.sdo", "type");
    private final static QName _FindCriteria_QNAME = new QName("http://xmlns.oracle.com/adf/svc/types/", "findCriteria");
    private final static QName _ShipmentLineInterface_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "shipmentLineInterface");
    private final static QName _ShipmentInterface_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "shipmentInterface");
    private final static QName _DataObject_QNAME = new QName("commonj.sdo", "dataObject");
    private final static QName _ShipmentDocumentResponseHeader_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "shipmentDocumentResponseHeader");
    private final static QName _ShipmentNotes_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "shipmentNotes");
    private final static QName _ShipmentResponse_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "shipmentResponse");
    private final static QName _FreightCostInterface_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "freightCostInterface");
    private final static QName _ContainerInterfaceResult_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "containerInterfaceResult");
    private final static QName _ServiceErrorMessage_QNAME = new QName("http://xmlns.oracle.com/adf/svc/errors/", "ServiceErrorMessage");
    private final static QName _ProcessShipmentActionDeferInventoryUpdates_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/types/", "DeferInventoryUpdates");
    private final static QName _ProcessShipmentActionInitMsgList_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/types/", "InitMsgList");
    private final static QName _ProcessShipmentActionActionType_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/types/", "ActionType");
    private final static QName _ProcessShipmentActionShipConfirmRule_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/types/", "ShipConfirmRule");
    private final static QName _ProcessShipmentActionCloseShipment_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/types/", "CloseShipment");
    private final static QName _ProcessShipmentActionCreateBillOfLading_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/types/", "CreateBillOfLading");
    private final static QName _ProcessShipmentActionDocumentJobSet_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/types/", "DocumentJobSet");
    private final static QName _ProcessShipmentActionCreateShipmentForStagedQuantity_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/types/", "CreateShipmentForStagedQuantity");
    private final static QName _ProcessShipmentActionPackingSlipStatus_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/types/", "PackingSlipStatus");
    private final static QName _ProcessShipmentActionActualShipDate_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/types/", "ActualShipDate");
    private final static QName _ProcessShipmentActionOverrideWeightVolume_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/types/", "OverrideWeightVolume");
    private final static QName _ProcessShipmentActionOutboundMessageCode_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/types/", "OutboundMessageCode");
    private final static QName _InvSerialsInterfaceCAttribute20_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "CAttribute20");
    private final static QName _InvSerialsInterfaceAttributeDate1_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "AttributeDate1");
    private final static QName _InvSerialsInterfaceAttributeDate5_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "AttributeDate5");
    private final static QName _InvSerialsInterfaceAttributeDate4_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "AttributeDate4");
    private final static QName _InvSerialsInterfaceAttributeDate3_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "AttributeDate3");
    private final static QName _InvSerialsInterfaceAttributeDate2_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "AttributeDate2");
    private final static QName _InvSerialsInterfaceAttributeNumber2_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "AttributeNumber2");
    private final static QName _InvSerialsInterfaceDAttribute10_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "DAttribute10");
    private final static QName _InvSerialsInterfaceAttributeNumber1_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "AttributeNumber1");
    private final static QName _InvSerialsInterfaceAttributeNumber4_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "AttributeNumber4");
    private final static QName _InvSerialsInterfaceAttributeNumber3_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "AttributeNumber3");
    private final static QName _InvSerialsInterfaceAttribute10_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "Attribute10");
    private final static QName _InvSerialsInterfaceAttributeNumber6_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "AttributeNumber6");
    private final static QName _InvSerialsInterfaceAttributeNumber5_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "AttributeNumber5");
    private final static QName _InvSerialsInterfaceAttributeNumber8_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "AttributeNumber8");
    private final static QName _InvSerialsInterfaceAttributeNumber7_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "AttributeNumber7");
    private final static QName _InvSerialsInterfaceAttribute14_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "Attribute14");
    private final static QName _InvSerialsInterfaceCAttribute19_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "CAttribute19");
    private final static QName _InvSerialsInterfaceAttribute13_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "Attribute13");
    private final static QName _InvSerialsInterfaceAttributeNumber9_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "AttributeNumber9");
    private final static QName _InvSerialsInterfaceCAttribute18_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "CAttribute18");
    private final static QName _InvSerialsInterfaceAttribute12_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "Attribute12");
    private final static QName _InvSerialsInterfaceAttribute11_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "Attribute11");
    private final static QName _InvSerialsInterfaceCAttribute1_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "CAttribute1");
    private final static QName _InvSerialsInterfaceCAttribute2_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "CAttribute2");
    private final static QName _InvSerialsInterfaceCAttribute3_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "CAttribute3");
    private final static QName _InvSerialsInterfaceCAttribute4_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "CAttribute4");
    private final static QName _InvSerialsInterfaceCAttribute5_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "CAttribute5");
    private final static QName _InvSerialsInterfaceCAttribute6_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "CAttribute6");
    private final static QName _InvSerialsInterfaceCAttribute7_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "CAttribute7");
    private final static QName _InvSerialsInterfaceCAttribute8_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "CAttribute8");
    private final static QName _InvSerialsInterfaceAttribute18_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "Attribute18");
    private final static QName _InvSerialsInterfaceAttribute17_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "Attribute17");
    private final static QName _InvSerialsInterfaceAttribute16_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "Attribute16");
    private final static QName _InvSerialsInterfaceAttribute15_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "Attribute15");
    private final static QName _InvSerialsInterfaceAttribute19_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "Attribute19");
    private final static QName _InvSerialsInterfaceAttribute1_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "Attribute1");
    private final static QName _InvSerialsInterfaceAttribute20_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "Attribute20");
    private final static QName _InvSerialsInterfaceAttribute3_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "Attribute3");
    private final static QName _InvSerialsInterfaceAttributeCategory_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "AttributeCategory");
    private final static QName _InvSerialsInterfaceAttribute2_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "Attribute2");
    private final static QName _InvSerialsInterfaceSubinventoryCode_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "SubinventoryCode");
    private final static QName _InvSerialsInterfaceAttribute9_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "Attribute9");
    private final static QName _InvSerialsInterfaceDAttribute5_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "DAttribute5");
    private final static QName _InvSerialsInterfaceAttribute8_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "Attribute8");
    private final static QName _InvSerialsInterfaceDAttribute4_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "DAttribute4");
    private final static QName _InvSerialsInterfaceDAttribute7_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "DAttribute7");
    private final static QName _InvSerialsInterfaceDAttribute6_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "DAttribute6");
    private final static QName _InvSerialsInterfaceAttribute5_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "Attribute5");
    private final static QName _InvSerialsInterfaceDAttribute1_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "DAttribute1");
    private final static QName _InvSerialsInterfaceToSerialNumber_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "ToSerialNumber");
    private final static QName _InvSerialsInterfaceAttribute4_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "Attribute4");
    private final static QName _InvSerialsInterfaceAttribute7_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "Attribute7");
    private final static QName _InvSerialsInterfaceDAttribute3_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "DAttribute3");
    private final static QName _InvSerialsInterfaceSerialAttributeCategory_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "SerialAttributeCategory");
    private final static QName _InvSerialsInterfaceAttribute6_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "Attribute6");
    private final static QName _InvSerialsInterfaceDAttribute2_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "DAttribute2");
    private final static QName _InvSerialsInterfaceDAttribute9_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "DAttribute9");
    private final static QName _InvSerialsInterfaceDAttribute8_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "DAttribute8");
    private final static QName _InvSerialsInterfaceNAttribute5_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "NAttribute5");
    private final static QName _InvSerialsInterfaceNAttribute4_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "NAttribute4");
    private final static QName _InvSerialsInterfaceNAttribute3_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "NAttribute3");
    private final static QName _InvSerialsInterfaceNAttribute2_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "NAttribute2");
    private final static QName _InvSerialsInterfaceNAttribute1_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "NAttribute1");
    private final static QName _InvSerialsInterfaceNAttribute9_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "NAttribute9");
    private final static QName _InvSerialsInterfaceNAttribute8_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "NAttribute8");
    private final static QName _InvSerialsInterfaceNAttribute7_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "NAttribute7");
    private final static QName _InvSerialsInterfaceNAttribute6_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "NAttribute6");
    private final static QName _InvSerialsInterfaceCAttribute9_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "CAttribute9");
    private final static QName _InvSerialsInterfaceAttributeNumber10_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "AttributeNumber10");
    private final static QName _InvSerialsInterfaceNAttribute10_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "NAttribute10");
    private final static QName _InvSerialsInterfaceLocator_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "Locator");
    private final static QName _InvSerialsInterfaceCAttribute15_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "CAttribute15");
    private final static QName _InvSerialsInterfaceCAttribute14_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "CAttribute14");
    private final static QName _InvSerialsInterfaceCAttribute17_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "CAttribute17");
    private final static QName _InvSerialsInterfaceCAttribute16_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "CAttribute16");
    private final static QName _InvSerialsInterfaceCAttribute11_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "CAttribute11");
    private final static QName _InvSerialsInterfaceCAttribute10_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "CAttribute10");
    private final static QName _InvSerialsInterfaceCAttribute13_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "CAttribute13");
    private final static QName _InvSerialsInterfaceCAttribute12_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "CAttribute12");
    private final static QName _InvSerialsInterfaceAttributeTimestamp1_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "AttributeTimestamp1");
    private final static QName _InvSerialsInterfaceAttributeTimestamp5_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "AttributeTimestamp5");
    private final static QName _InvSerialsInterfaceAttributeTimestamp4_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "AttributeTimestamp4");
    private final static QName _InvSerialsInterfaceTAttribute1_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "TAttribute1");
    private final static QName _InvSerialsInterfaceAttributeTimestamp3_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "AttributeTimestamp3");
    private final static QName _InvSerialsInterfaceTAttribute2_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "TAttribute2");
    private final static QName _InvSerialsInterfaceAttributeTimestamp2_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "AttributeTimestamp2");
    private final static QName _InvSerialsInterfaceTAttribute3_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "TAttribute3");
    private final static QName _InvSerialsInterfaceTAttribute4_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "TAttribute4");
    private final static QName _InvSerialsInterfaceTAttribute5_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "TAttribute5");
    private final static QName _FreightCostInterfaceConversionDate_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "ConversionDate");
    private final static QName _FreightCostInterfaceConversionRate_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "ConversionRate");
    private final static QName _FreightCostInterfaceFreightCostName_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "FreightCostName");
    private final static QName _FreightCostInterfaceConversionTypeCode_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "ConversionTypeCode");
    private final static QName _FetchShipmentDocumentsToActualShipDate_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/types/", "ToActualShipDate");
    private final static QName _FetchShipmentDocumentsToCreationDate_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/types/", "ToCreationDate");
    private final static QName _FetchShipmentDocumentsFromActualShipDate_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/types/", "FromActualShipDate");
    private final static QName _FetchShipmentDocumentsShipment_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/types/", "Shipment");
    private final static QName _FetchShipmentDocumentsToConfirmedDate_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/types/", "ToConfirmedDate");
    private final static QName _FetchShipmentDocumentsFromCreationDate_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/types/", "FromCreationDate");
    private final static QName _FetchShipmentDocumentsFromConfirmedDate_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/types/", "FromConfirmedDate");
    private final static QName _ShipmentLineInterfaceLoadingSequence_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "LoadingSequence");
    private final static QName _ShipmentLineInterfaceSubinventory_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "Subinventory");
    private final static QName _ShipmentLineInterfaceProductFiscClassification_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "ProductFiscClassification");
    private final static QName _ShipmentLineInterfaceWeightUOMName_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "WeightUOMName");
    private final static QName _ShipmentLineInterfacePackingInstructions_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "PackingInstructions");
    private final static QName _ShipmentLineInterfaceTransactionBusinessCategory_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "TransactionBusinessCategory");
    private final static QName _ShipmentLineInterfaceNetWeight_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "NetWeight");
    private final static QName _ShipmentLineInterfaceExemptCertificateNumber_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "ExemptCertificateNumber");
    private final static QName _ShipmentLineInterfaceDeliveryDetailId_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "DeliveryDetailId");
    private final static QName _ShipmentLineInterfaceShippedQuantity_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "ShippedQuantity");
    private final static QName _ShipmentLineInterfaceIntendedUse_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "IntendedUse");
    private final static QName _ShipmentLineInterfaceItemNumber_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "ItemNumber");
    private final static QName _ShipmentLineInterfaceTaxInvoiceDate_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "TaxInvoiceDate");
    private final static QName _ShipmentLineInterfaceSecondaryShippedQuantity_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "SecondaryShippedQuantity");
    private final static QName _ShipmentLineInterfaceAssessableValue_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "AssessableValue");
    private final static QName _ShipmentLineInterfaceProductCategory_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "ProductCategory");
    private final static QName _ShipmentLineInterfaceTaxClassificationCode_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "TaxClassificationCode");
    private final static QName _ShipmentLineInterfaceDefaultTaxationCountry_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "DefaultTaxationCountry");
    private final static QName _ShipmentLineInterfaceUserDefinedFiscalClassification_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "UserDefinedFiscalClassification");
    private final static QName _ShipmentLineInterfaceRevision_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "Revision");
    private final static QName _ShipmentLineInterfaceVolume_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "Volume");
    private final static QName _ShipmentLineInterfaceProductType_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "ProductType");
    private final static QName _ShipmentLineInterfaceFirstPartyTaxRegistrationNumber_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "FirstPartyTaxRegistrationNumber");
    private final static QName _ShipmentLineInterfaceTaxInvoiceNumber_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "TaxInvoiceNumber");
    private final static QName _ShipmentLineInterfaceLocationOfFinalDischarge_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "LocationOfFinalDischarge");
    private final static QName _ShipmentLineInterfaceDocumentSubType_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "DocumentSubType");
    private final static QName _ShipmentLineInterfaceGrossWeight_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "GrossWeight");
    private final static QName _ShipmentLineInterfaceExemptReasonCode_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "ExemptReasonCode");
    private final static QName _ShipmentLineInterfaceThirdPartyTaxRegistrationNumber_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "ThirdPartyTaxRegistrationNumber");
    private final static QName _ShipmentLineInterfaceVolumeUOMName_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "VolumeUOMName");
    private final static QName _ShipmentLineInterfaceShippingInstructions_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "ShippingInstructions");
    private final static QName _ShipmentInformationFreightTerms_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "FreightTerms");
    private final static QName _ShipmentInformationCODAmount_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "CODAmount");
    private final static QName _ShipmentInformationAutomaticallyPack_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "AutomaticallyPack");
    private final static QName _ShipmentInformationDock_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "Dock");
    private final static QName _ShipmentInformationGlobalAttributeCategory_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "GlobalAttributeCategory");
    private final static QName _ShipmentInformationPlanned_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "Planned");
    private final static QName _ShipmentInformationWeightUOM_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "WeightUOM");
    private final static QName _ShipmentInformationGlobalAttribute9_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "GlobalAttribute9");
    private final static QName _ShipmentInformationCODRemitTo_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "CODRemitTo");
    private final static QName _ShipmentInformationCODCurrency_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "CODCurrency");
    private final static QName _ShipmentInformationDescription_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "Description");
    private final static QName _ShipmentInformationGlobalAttribute18_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "GlobalAttribute18");
    private final static QName _ShipmentInformationGlobalAttribute19_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "GlobalAttribute19");
    private final static QName _ShipmentInformationModeOfTransport_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "ModeOfTransport");
    private final static QName _ShipmentInformationProrateWeight_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "ProrateWeight");
    private final static QName _ShipmentInformationGlobalAttribute20_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "GlobalAttribute20");
    private final static QName _ShipmentInformationCODPaidBy_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "CODPaidBy");
    private final static QName _ShipmentInformationGlobalAttribute1_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "GlobalAttribute1");
    private final static QName _ShipmentInformationGlobalAttribute2_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "GlobalAttribute2");
    private final static QName _ShipmentInformationGlobalAttribute3_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "GlobalAttribute3");
    private final static QName _ShipmentInformationGlobalAttribute4_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "GlobalAttribute4");
    private final static QName _ShipmentInformationGlobalAttribute5_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "GlobalAttribute5");
    private final static QName _ShipmentInformationGlobalAttribute6_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "GlobalAttribute6");
    private final static QName _ShipmentInformationGlobalAttribute7_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "GlobalAttribute7");
    private final static QName _ShipmentInformationGlobalAttribute8_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "GlobalAttribute8");
    private final static QName _ShipmentInformationGlobalAttributeDate2_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "GlobalAttributeDate2");
    private final static QName _ShipmentInformationGlobalAttributeDate1_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "GlobalAttributeDate1");
    private final static QName _ShipmentInformationGlobalAttributeDate4_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "GlobalAttributeDate4");
    private final static QName _ShipmentInformationGlobalAttributeDate3_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "GlobalAttributeDate3");
    private final static QName _ShipmentInformationGlobalAttributeDate5_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "GlobalAttributeDate5");
    private final static QName _ShipmentInformationShipFromOrganization_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "ShipFromOrganization");
    private final static QName _ShipmentInformationShipToPartySiteNumber_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "ShipToPartySiteNumber");
    private final static QName _ShipmentInformationTransportationReason_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "TransportationReason");
    private final static QName _ShipmentInformationFOBSiteNumber_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "FOBSiteNumber");
    private final static QName _ShipmentInformationEquipment_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "Equipment");
    private final static QName _ShipmentInformationRoutingInstructions_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "RoutingInstructions");
    private final static QName _ShipmentInformationVolumeUOM_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "VolumeUOM");
    private final static QName _ShipmentInformationShippingMarks_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "ShippingMarks");
    private final static QName _ShipmentInformationInitialShipDate_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "InitialShipDate");
    private final static QName _ShipmentInformationWaybill_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "Waybill");
    private final static QName _ShipmentInformationProblemContactReference_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "ProblemContactReference");
    private final static QName _ShipmentInformationGlobalAttribute12_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "GlobalAttribute12");
    private final static QName _ShipmentInformationGlobalAttribute13_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "GlobalAttribute13");
    private final static QName _ShipmentInformationServiceLevel_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "ServiceLevel");
    private final static QName _ShipmentInformationGlobalAttribute10_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "GlobalAttribute10");
    private final static QName _ShipmentInformationGlobalAttribute11_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "GlobalAttribute11");
    private final static QName _ShipmentInformationSealNumber_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "SealNumber");
    private final static QName _ShipmentInformationGlobalAttribute16_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "GlobalAttribute16");
    private final static QName _ShipmentInformationGlobalAttribute17_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "GlobalAttribute17");
    private final static QName _ShipmentInformationGlobalAttribute14_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "GlobalAttribute14");
    private final static QName _ShipmentInformationGlobalAttribute15_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "GlobalAttribute15");
    private final static QName _ShipmentInformationEnableAutoship_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "EnableAutoship");
    private final static QName _ShipmentInformationConfirmedDate_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "ConfirmedDate");
    private final static QName _ShipmentInformationGlobalAttributeNumber3_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "GlobalAttributeNumber3");
    private final static QName _ShipmentInformationGlobalAttributeNumber2_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "GlobalAttributeNumber2");
    private final static QName _ShipmentInformationGlobalAttributeNumber5_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "GlobalAttributeNumber5");
    private final static QName _ShipmentInformationGlobalAttributeNumber4_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "GlobalAttributeNumber4");
    private final static QName _ShipmentInformationPlannedDeliveryDate_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "PlannedDeliveryDate");
    private final static QName _ShipmentInformationShipment_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "Shipment");
    private final static QName _ShipmentInformationGlobalAttributeNumber1_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "GlobalAttributeNumber1");
    private final static QName _ShipmentInformationFOB_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "FOB");
    private final static QName _ShipmentInformationCustomerNumber_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "CustomerNumber");
    private final static QName _ShipmentInformationEquipmentType_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "EquipmentType");
    private final static QName _ShipmentInformationCarrierPartyNumber_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "CarrierPartyNumber");
    private final static QName _ShipmentInformationLoadingSequenceRule_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "LoadingSequenceRule");
    private final static QName _InvLotInterfaceSecondaryTransactionQuantity_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "SecondaryTransactionQuantity");
    private final static QName _ShipmentDocumentResponseLineMessageText_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "MessageText");
    private final static QName _ShipmentDocumentResponseLineDocumentName_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "DocumentName");
    private final static QName _ShipmentDocumentResponseLineJobProcessId_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "JobProcessId");
    private final static QName _ShipmentDocumentResponseLineDocumentSubmissionTime_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "DocumentSubmissionTime");
    private final static QName _ShipmentDocumentResponseLineDocumentCompletionTime_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "DocumentCompletionTime");
    private final static QName _ShipmentDocumentResponseLineStatus_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "Status");
    private final static QName _ShipmentDocumentResponseLineDocumentContent_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "DocumentContent");
    private final static QName _ShipmentDocumentResponseLineDocumentContentType_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "DocumentContentType");
    private final static QName _ShipmentDocumentResponseLineDocumentDescription_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "DocumentDescription");
    private final static QName _ShipmentDocumentResponseLineDocumentSubmittedBy_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "DocumentSubmittedBy");
    private final static QName _ShipmentResponseMessageCount_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "MessageCount");
    private final static QName _ShipmentResponseReturnStatus_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "ReturnStatus");
    private final static QName _ContainerInterfaceContainerVolumeUOMName_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "ContainerVolumeUOMName");
    private final static QName _ContainerInterfaceTareWeight_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "TareWeight");
    private final static QName _ContainerInterfaceTrackingNumber_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "TrackingNumber");
    private final static QName _ContainerInterfaceMasterSerialNumber_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "MasterSerialNumber");
    private final static QName _ContainerInterfaceLicensePlateNumber_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "LicensePlateNumber");
    private final static QName _ContainerInterfaceFOBCode_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "FOBCode");
    private final static QName _ContainerInterfaceTareWeightUOMName_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "TareWeightUOMName");
    private final static QName _ContainerInterfaceContainerVolume_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "ContainerVolume");
    private final static QName _ContainerInterfaceGrossWeightUOMName_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "GrossWeightUOMName");
    private final static QName _ContainerInterfaceContainerItemName_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "ContainerItemName");
    private final static QName _ShipmentDocumentResponseHeaderTransportationShipment_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "TransportationShipment");
    private final static QName _ShipmentDocumentResponseHeaderShipmentStatus_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "ShipmentStatus");
    private final static QName _ShipmentInterfaceFOBAddress2_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "FOBAddress2");
    private final static QName _ShipmentInterfaceFOBAddress1_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "FOBAddress1");
    private final static QName _ShipmentInterfaceFOBState_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "FOBState");
    private final static QName _ShipmentInterfaceDockCode_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "DockCode");
    private final static QName _ShipmentInterfaceFOBAddress4_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "FOBAddress4");
    private final static QName _ShipmentInterfaceFOBAddress3_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "FOBAddress3");
    private final static QName _ShipmentInterfaceFOBCounty_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "FOBCounty");
    private final static QName _ShipmentInterfaceActualShipDate_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "ActualShipDate");
    private final static QName _ShipmentInterfaceOrganizationCode_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "OrganizationCode");
    private final static QName _ShipmentInterfaceCODCurrencyCode_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "CODCurrencyCode");
    private final static QName _ShipmentInterfaceFOBCountry_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "FOBCountry");
    private final static QName _ShipmentInterfaceCarrierName_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "CarrierName");
    private final static QName _ShipmentInterfaceFOBCity_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "FOBCity");
    private final static QName _ShipmentInterfacePackingSlipNumber_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "PackingSlipNumber");
    private final static QName _ShipmentInterfaceConfirmedBy_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "ConfirmedBy");
    private final static QName _ShipmentInterfaceFreightTermsCode_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "FreightTermsCode");
    private final static QName _ShipmentInterfaceShipmentName_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "ShipmentName");
    private final static QName _ShipmentInterfaceInitialPickupDate_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "InitialPickupDate");
    private final static QName _ShipmentInterfaceFOBRegion_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "FOBRegion");
    private final static QName _ShipmentInterfaceBillOfLadingNumber_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "BillOfLadingNumber");
    private final static QName _ShipmentInterfaceUltimateDropoffDate_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "UltimateDropoffDate");
    private final static QName _ShipmentInterfaceFOBPostalCode_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "FOBPostalCode");
    private final static QName _ShipmentInterfaceASNDateSent_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "ASNDateSent");
    private final static QName _ShipmentNotesNoteTypeCode_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "NoteTypeCode");
    private final static QName _ShipmentNotesActionType_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "ActionType");
    private final static QName _ShipmentNotesNoteText_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "NoteText");
    private final static QName _ShipmentNotesOldNoteText_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "OldNoteText");
    private final static QName _ShippingCostShippingCostId_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "ShippingCostId");
    private final static QName _ShippingCostConversionRateType_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "ConversionRateType");
    private final static QName _ShippingCostCost_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "Cost");
    private final static QName _ShippingCostAction_QNAME = new QName("http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", "Action");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.services.wsimport.integration1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link FaultType }
     * 
     */
    public FaultType createFaultType() {
        return new FaultType();
    }

    /**
     * Create an instance of {@link Detail }
     * 
     */
    public Detail createDetail() {
        return new Detail();
    }

    /**
     * Create an instance of {@link ProcessShipmentActionAsync }
     * 
     */
    public ProcessShipmentActionAsync createProcessShipmentActionAsync() {
        return new ProcessShipmentActionAsync();
    }

    /**
     * Create an instance of {@link GetServiceLastUpdateTimeResponse }
     * 
     */
    public GetServiceLastUpdateTimeResponse createGetServiceLastUpdateTimeResponse() {
        return new GetServiceLastUpdateTimeResponse();
    }

    /**
     * Create an instance of {@link GetDfltObjAttrHintsAsync }
     * 
     */
    public GetDfltObjAttrHintsAsync createGetDfltObjAttrHintsAsync() {
        return new GetDfltObjAttrHintsAsync();
    }

    /**
     * Create an instance of {@link GetEntityListAsync }
     * 
     */
    public GetEntityListAsync createGetEntityListAsync() {
        return new GetEntityListAsync();
    }

    /**
     * Create an instance of {@link ProcessShippingCostResponse }
     * 
     */
    public ProcessShippingCostResponse createProcessShippingCostResponse() {
        return new ProcessShippingCostResponse();
    }

    /**
     * Create an instance of {@link ShipmentResponse }
     * 
     */
    public ShipmentResponse createShipmentResponse() {
        return new ShipmentResponse();
    }

    /**
     * Create an instance of {@link ProcessCreateUpdateShipmentResponse }
     * 
     */
    public ProcessCreateUpdateShipmentResponse createProcessCreateUpdateShipmentResponse() {
        return new ProcessCreateUpdateShipmentResponse();
    }

    /**
     * Create an instance of {@link ProcessShippingCost }
     * 
     */
    public ProcessShippingCost createProcessShippingCost() {
        return new ProcessShippingCost();
    }

    /**
     * Create an instance of {@link ShippingCost }
     * 
     */
    public ShippingCost createShippingCost() {
        return new ShippingCost();
    }

    /**
     * Create an instance of {@link GetServiceLastUpdateTimeAsync }
     * 
     */
    public GetServiceLastUpdateTimeAsync createGetServiceLastUpdateTimeAsync() {
        return new GetServiceLastUpdateTimeAsync();
    }

    /**
     * Create an instance of {@link GetEntityList }
     * 
     */
    public GetEntityList createGetEntityList() {
        return new GetEntityList();
    }

    /**
     * Create an instance of {@link GetServiceLastUpdateTime }
     * 
     */
    public GetServiceLastUpdateTime createGetServiceLastUpdateTime() {
        return new GetServiceLastUpdateTime();
    }

    /**
     * Create an instance of {@link ProcessCreateUpdateShipmentAsyncResponse }
     * 
     */
    public ProcessCreateUpdateShipmentAsyncResponse createProcessCreateUpdateShipmentAsyncResponse() {
        return new ProcessCreateUpdateShipmentAsyncResponse();
    }

    /**
     * Create an instance of {@link ProcessShippingCostAsync }
     * 
     */
    public ProcessShippingCostAsync createProcessShippingCostAsync() {
        return new ProcessShippingCostAsync();
    }

    /**
     * Create an instance of {@link GetDfltObjAttrHintsResponse }
     * 
     */
    public GetDfltObjAttrHintsResponse createGetDfltObjAttrHintsResponse() {
        return new GetDfltObjAttrHintsResponse();
    }

    /**
     * Create an instance of {@link ObjAttrHints }
     * 
     */
    public ObjAttrHints createObjAttrHints() {
        return new ObjAttrHints();
    }

    /**
     * Create an instance of {@link ProcessCreateUpdateShipmentAsync }
     * 
     */
    public ProcessCreateUpdateShipmentAsync createProcessCreateUpdateShipmentAsync() {
        return new ProcessCreateUpdateShipmentAsync();
    }

    /**
     * Create an instance of {@link ShipmentInformation }
     * 
     */
    public ShipmentInformation createShipmentInformation() {
        return new ShipmentInformation();
    }

    /**
     * Create an instance of {@link FetchShipmentDocumentsAsync }
     * 
     */
    public FetchShipmentDocumentsAsync createFetchShipmentDocumentsAsync() {
        return new FetchShipmentDocumentsAsync();
    }

    /**
     * Create an instance of {@link ProcessShipmentAction }
     * 
     */
    public ProcessShipmentAction createProcessShipmentAction() {
        return new ProcessShipmentAction();
    }

    /**
     * Create an instance of {@link CreateAndConfirmAsync }
     * 
     */
    public CreateAndConfirmAsync createCreateAndConfirmAsync() {
        return new CreateAndConfirmAsync();
    }

    /**
     * Create an instance of {@link ShipmentInterface }
     * 
     */
    public ShipmentInterface createShipmentInterface() {
        return new ShipmentInterface();
    }

    /**
     * Create an instance of {@link ProcessShipmentActionResponse }
     * 
     */
    public ProcessShipmentActionResponse createProcessShipmentActionResponse() {
        return new ProcessShipmentActionResponse();
    }

    /**
     * Create an instance of {@link ProcessShipmentActionAsyncResponse }
     * 
     */
    public ProcessShipmentActionAsyncResponse createProcessShipmentActionAsyncResponse() {
        return new ProcessShipmentActionAsyncResponse();
    }

    /**
     * Create an instance of {@link GetDfltObjAttrHints }
     * 
     */
    public GetDfltObjAttrHints createGetDfltObjAttrHints() {
        return new GetDfltObjAttrHints();
    }

    /**
     * Create an instance of {@link GetDfltObjAttrHintsAsyncResponse }
     * 
     */
    public GetDfltObjAttrHintsAsyncResponse createGetDfltObjAttrHintsAsyncResponse() {
        return new GetDfltObjAttrHintsAsyncResponse();
    }

    /**
     * Create an instance of {@link FetchShipmentDocumentsResponse }
     * 
     */
    public FetchShipmentDocumentsResponse createFetchShipmentDocumentsResponse() {
        return new FetchShipmentDocumentsResponse();
    }

    /**
     * Create an instance of {@link ShipmentDocumentResponseHeader }
     * 
     */
    public ShipmentDocumentResponseHeader createShipmentDocumentResponseHeader() {
        return new ShipmentDocumentResponseHeader();
    }

    /**
     * Create an instance of {@link CreateAndConfirm }
     * 
     */
    public CreateAndConfirm createCreateAndConfirm() {
        return new CreateAndConfirm();
    }

    /**
     * Create an instance of {@link GetEntityListResponse }
     * 
     */
    public GetEntityListResponse createGetEntityListResponse() {
        return new GetEntityListResponse();
    }

    /**
     * Create an instance of {@link ServiceViewInfo }
     * 
     */
    public ServiceViewInfo createServiceViewInfo() {
        return new ServiceViewInfo();
    }

    /**
     * Create an instance of {@link FetchShipmentDocuments }
     * 
     */
    public FetchShipmentDocuments createFetchShipmentDocuments() {
        return new FetchShipmentDocuments();
    }

    /**
     * Create an instance of {@link FetchShipmentDocumentsAsyncResponse }
     * 
     */
    public FetchShipmentDocumentsAsyncResponse createFetchShipmentDocumentsAsyncResponse() {
        return new FetchShipmentDocumentsAsyncResponse();
    }

    /**
     * Create an instance of {@link GetEntityListAsyncResponse }
     * 
     */
    public GetEntityListAsyncResponse createGetEntityListAsyncResponse() {
        return new GetEntityListAsyncResponse();
    }

    /**
     * Create an instance of {@link CreateAndConfirmAsyncResponse }
     * 
     */
    public CreateAndConfirmAsyncResponse createCreateAndConfirmAsyncResponse() {
        return new CreateAndConfirmAsyncResponse();
    }

    /**
     * Create an instance of {@link ShipmentInterfaceResult }
     * 
     */
    public ShipmentInterfaceResult createShipmentInterfaceResult() {
        return new ShipmentInterfaceResult();
    }

    /**
     * Create an instance of {@link ProcessShippingCostAsyncResponse }
     * 
     */
    public ProcessShippingCostAsyncResponse createProcessShippingCostAsyncResponse() {
        return new ProcessShippingCostAsyncResponse();
    }

    /**
     * Create an instance of {@link CreateAndConfirmResponse }
     * 
     */
    public CreateAndConfirmResponse createCreateAndConfirmResponse() {
        return new CreateAndConfirmResponse();
    }

    /**
     * Create an instance of {@link GetServiceLastUpdateTimeAsyncResponse }
     * 
     */
    public GetServiceLastUpdateTimeAsyncResponse createGetServiceLastUpdateTimeAsyncResponse() {
        return new GetServiceLastUpdateTimeAsyncResponse();
    }

    /**
     * Create an instance of {@link ProcessCreateUpdateShipment }
     * 
     */
    public ProcessCreateUpdateShipment createProcessCreateUpdateShipment() {
        return new ProcessCreateUpdateShipment();
    }

    /**
     * Create an instance of {@link ProcessControl }
     * 
     */
    public ProcessControl createProcessControl() {
        return new ProcessControl();
    }

    /**
     * Create an instance of {@link FindCriteria }
     * 
     */
    public FindCriteria createFindCriteria() {
        return new FindCriteria();
    }

    /**
     * Create an instance of {@link FindControl }
     * 
     */
    public FindControl createFindControl() {
        return new FindControl();
    }

    /**
     * Create an instance of {@link ChildFindCriteria }
     * 
     */
    public ChildFindCriteria createChildFindCriteria() {
        return new ChildFindCriteria();
    }

    /**
     * Create an instance of {@link BigIntegerResult }
     * 
     */
    public BigIntegerResult createBigIntegerResult() {
        return new BigIntegerResult();
    }

    /**
     * Create an instance of {@link MeasureType }
     * 
     */
    public MeasureType createMeasureType() {
        return new MeasureType();
    }

    /**
     * Create an instance of {@link DateResult }
     * 
     */
    public DateResult createDateResult() {
        return new DateResult();
    }

    /**
     * Create an instance of {@link DoubleResult }
     * 
     */
    public DoubleResult createDoubleResult() {
        return new DoubleResult();
    }

    /**
     * Create an instance of {@link DataObjectResult }
     * 
     */
    public DataObjectResult createDataObjectResult() {
        return new DataObjectResult();
    }

    /**
     * Create an instance of {@link ViewCriteriaItem }
     * 
     */
    public ViewCriteriaItem createViewCriteriaItem() {
        return new ViewCriteriaItem();
    }

    /**
     * Create an instance of {@link AttrCtrlHints }
     * 
     */
    public AttrCtrlHints createAttrCtrlHints() {
        return new AttrCtrlHints();
    }

    /**
     * Create an instance of {@link LongResult }
     * 
     */
    public LongResult createLongResult() {
        return new LongResult();
    }

    /**
     * Create an instance of {@link ViewCriteriaRow }
     * 
     */
    public ViewCriteriaRow createViewCriteriaRow() {
        return new ViewCriteriaRow();
    }

    /**
     * Create an instance of {@link BigDecimalResult }
     * 
     */
    public BigDecimalResult createBigDecimalResult() {
        return new BigDecimalResult();
    }

    /**
     * Create an instance of {@link BooleanResult }
     * 
     */
    public BooleanResult createBooleanResult() {
        return new BooleanResult();
    }

    /**
     * Create an instance of {@link SortAttribute }
     * 
     */
    public SortAttribute createSortAttribute() {
        return new SortAttribute();
    }

    /**
     * Create an instance of {@link TimestampResult }
     * 
     */
    public TimestampResult createTimestampResult() {
        return new TimestampResult();
    }

    /**
     * Create an instance of {@link SortOrder }
     * 
     */
    public SortOrder createSortOrder() {
        return new SortOrder();
    }

    /**
     * Create an instance of {@link ShortResult }
     * 
     */
    public ShortResult createShortResult() {
        return new ShortResult();
    }

    /**
     * Create an instance of {@link BytesResult }
     * 
     */
    public BytesResult createBytesResult() {
        return new BytesResult();
    }

    /**
     * Create an instance of {@link CtrlHint }
     * 
     */
    public CtrlHint createCtrlHint() {
        return new CtrlHint();
    }

    /**
     * Create an instance of {@link FloatResult }
     * 
     */
    public FloatResult createFloatResult() {
        return new FloatResult();
    }

    /**
     * Create an instance of {@link AmountType }
     * 
     */
    public AmountType createAmountType() {
        return new AmountType();
    }

    /**
     * Create an instance of {@link TimeResult }
     * 
     */
    public TimeResult createTimeResult() {
        return new TimeResult();
    }

    /**
     * Create an instance of {@link StringResult }
     * 
     */
    public StringResult createStringResult() {
        return new StringResult();
    }

    /**
     * Create an instance of {@link DataHandlerResult }
     * 
     */
    public DataHandlerResult createDataHandlerResult() {
        return new DataHandlerResult();
    }

    /**
     * Create an instance of {@link ViewCriteria }
     * 
     */
    public ViewCriteria createViewCriteria() {
        return new ViewCriteria();
    }

    /**
     * Create an instance of {@link ByteResult }
     * 
     */
    public ByteResult createByteResult() {
        return new ByteResult();
    }

    /**
     * Create an instance of {@link MethodResult }
     * 
     */
    public MethodResult createMethodResult() {
        return new MethodResult();
    }

    /**
     * Create an instance of {@link IntegerResult }
     * 
     */
    public IntegerResult createIntegerResult() {
        return new IntegerResult();
    }

    /**
     * Create an instance of {@link ServiceErrorMessage }
     * 
     */
    public ServiceErrorMessage createServiceErrorMessage() {
        return new ServiceErrorMessage();
    }

    /**
     * Create an instance of {@link ServiceMessage }
     * 
     */
    public ServiceMessage createServiceMessage() {
        return new ServiceMessage();
    }

    /**
     * Create an instance of {@link ServiceDMLErrorMessage }
     * 
     */
    public ServiceDMLErrorMessage createServiceDMLErrorMessage() {
        return new ServiceDMLErrorMessage();
    }

    /**
     * Create an instance of {@link ServiceAttrValErrorMessage }
     * 
     */
    public ServiceAttrValErrorMessage createServiceAttrValErrorMessage() {
        return new ServiceAttrValErrorMessage();
    }

    /**
     * Create an instance of {@link ServiceRowValErrorMessage }
     * 
     */
    public ServiceRowValErrorMessage createServiceRowValErrorMessage() {
        return new ServiceRowValErrorMessage();
    }

    /**
     * Create an instance of {@link JavaInfo }
     * 
     */
    public JavaInfo createJavaInfo() {
        return new JavaInfo();
    }

    /**
     * Create an instance of {@link Types }
     * 
     */
    public Types createTypes() {
        return new Types();
    }

    /**
     * Create an instance of {@link DataGraphType }
     * 
     */
    public DataGraphType createDataGraphType() {
        return new DataGraphType();
    }

    /**
     * Create an instance of {@link Type }
     * 
     */
    public Type createType() {
        return new Type();
    }

    /**
     * Create an instance of {@link ChangeSummaryType }
     * 
     */
    public ChangeSummaryType createChangeSummaryType() {
        return new ChangeSummaryType();
    }

    /**
     * Create an instance of {@link Property }
     * 
     */
    public Property createProperty() {
        return new Property();
    }

    /**
     * Create an instance of {@link XSDType }
     * 
     */
    public XSDType createXSDType() {
        return new XSDType();
    }

    /**
     * Create an instance of {@link ModelsType }
     * 
     */
    public ModelsType createModelsType() {
        return new ModelsType();
    }

    /**
     * Create an instance of {@link XMLInfo }
     * 
     */
    public XMLInfo createXMLInfo() {
        return new XMLInfo();
    }

    /**
     * Create an instance of {@link InvSerialsInterface }
     * 
     */
    public InvSerialsInterface createInvSerialsInterface() {
        return new InvSerialsInterface();
    }

    /**
     * Create an instance of {@link InvLotInterfaceResult }
     * 
     */
    public InvLotInterfaceResult createInvLotInterfaceResult() {
        return new InvLotInterfaceResult();
    }

    /**
     * Create an instance of {@link ContainerInterfaceResult }
     * 
     */
    public ContainerInterfaceResult createContainerInterfaceResult() {
        return new ContainerInterfaceResult();
    }

    /**
     * Create an instance of {@link ContainerInterface }
     * 
     */
    public ContainerInterface createContainerInterface() {
        return new ContainerInterface();
    }

    /**
     * Create an instance of {@link InvSerialsInterfaceResult }
     * 
     */
    public InvSerialsInterfaceResult createInvSerialsInterfaceResult() {
        return new InvSerialsInterfaceResult();
    }

    /**
     * Create an instance of {@link ShipmentLineInterfaceResult }
     * 
     */
    public ShipmentLineInterfaceResult createShipmentLineInterfaceResult() {
        return new ShipmentLineInterfaceResult();
    }

    /**
     * Create an instance of {@link ShipmentDocumentResponseLine }
     * 
     */
    public ShipmentDocumentResponseLine createShipmentDocumentResponseLine() {
        return new ShipmentDocumentResponseLine();
    }

    /**
     * Create an instance of {@link ErrorMessage }
     * 
     */
    public ErrorMessage createErrorMessage() {
        return new ErrorMessage();
    }

    /**
     * Create an instance of {@link ShipmentNotes }
     * 
     */
    public ShipmentNotes createShipmentNotes() {
        return new ShipmentNotes();
    }

    /**
     * Create an instance of {@link FreightCostInterfaceResult }
     * 
     */
    public FreightCostInterfaceResult createFreightCostInterfaceResult() {
        return new FreightCostInterfaceResult();
    }

    /**
     * Create an instance of {@link FreightCostInterface }
     * 
     */
    public FreightCostInterface createFreightCostInterface() {
        return new FreightCostInterface();
    }

    /**
     * Create an instance of {@link ShipmentLineInterface }
     * 
     */
    public ShipmentLineInterface createShipmentLineInterface() {
        return new ShipmentLineInterface();
    }

    /**
     * Create an instance of {@link InvLotInterface }
     * 
     */
    public InvLotInterface createInvLotInterface() {
        return new InvLotInterface();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ProcessControl }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/adf/svc/types/", name = "processControl")
    public JAXBElement<ProcessControl> createProcessControl(ProcessControl value) {
        return new JAXBElement<ProcessControl>(_ProcessControl_QNAME, ProcessControl.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link InvLotInterface }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "invLotInterface")
    public JAXBElement<InvLotInterface> createInvLotInterface(InvLotInterface value) {
        return new JAXBElement<InvLotInterface>(_InvLotInterface_QNAME, InvLotInterface.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ShipmentInterfaceResult }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "shipmentInterfaceResult")
    public JAXBElement<ShipmentInterfaceResult> createShipmentInterfaceResult(ShipmentInterfaceResult value) {
        return new JAXBElement<ShipmentInterfaceResult>(_ShipmentInterfaceResult_QNAME, ShipmentInterfaceResult.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ShipmentInformation }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "shipmentInformation")
    public JAXBElement<ShipmentInformation> createShipmentInformation(ShipmentInformation value) {
        return new JAXBElement<ShipmentInformation>(_ShipmentInformation_QNAME, ShipmentInformation.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FaultType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/oracleas/schema/oracle-fault-11_0", name = "Fault")
    public JAXBElement<FaultType> createFault(FaultType value) {
        return new JAXBElement<FaultType>(_Fault_QNAME, FaultType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Types }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "commonj.sdo", name = "types")
    public JAXBElement<Types> createTypes(Types value) {
        return new JAXBElement<Types>(_Types_QNAME, Types.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataGraphType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "commonj.sdo", name = "datagraph")
    public JAXBElement<DataGraphType> createDatagraph(DataGraphType value) {
        return new JAXBElement<DataGraphType>(_Datagraph_QNAME, DataGraphType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FreightCostInterfaceResult }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "freightCostInterfaceResult")
    public JAXBElement<FreightCostInterfaceResult> createFreightCostInterfaceResult(FreightCostInterfaceResult value) {
        return new JAXBElement<FreightCostInterfaceResult>(_FreightCostInterfaceResult_QNAME, FreightCostInterfaceResult.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ErrorMessage }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "errorMessage")
    public JAXBElement<ErrorMessage> createErrorMessage(ErrorMessage value) {
        return new JAXBElement<ErrorMessage>(_ErrorMessage_QNAME, ErrorMessage.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ShippingCost }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "shippingCost")
    public JAXBElement<ShippingCost> createShippingCost(ShippingCost value) {
        return new JAXBElement<ShippingCost>(_ShippingCost_QNAME, ShippingCost.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link InvSerialsInterface }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "invSerialsInterface")
    public JAXBElement<InvSerialsInterface> createInvSerialsInterface(InvSerialsInterface value) {
        return new JAXBElement<InvSerialsInterface>(_InvSerialsInterface_QNAME, InvSerialsInterface.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link InvLotInterfaceResult }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "invLotInterfaceResult")
    public JAXBElement<InvLotInterfaceResult> createInvLotInterfaceResult(InvLotInterfaceResult value) {
        return new JAXBElement<InvLotInterfaceResult>(_InvLotInterfaceResult_QNAME, InvLotInterfaceResult.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link InvSerialsInterfaceResult }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "invSerialsInterfaceResult")
    public JAXBElement<InvSerialsInterfaceResult> createInvSerialsInterfaceResult(InvSerialsInterfaceResult value) {
        return new JAXBElement<InvSerialsInterfaceResult>(_InvSerialsInterfaceResult_QNAME, InvSerialsInterfaceResult.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ShipmentLineInterfaceResult }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "shipmentLineInterfaceResult")
    public JAXBElement<ShipmentLineInterfaceResult> createShipmentLineInterfaceResult(ShipmentLineInterfaceResult value) {
        return new JAXBElement<ShipmentLineInterfaceResult>(_ShipmentLineInterfaceResult_QNAME, ShipmentLineInterfaceResult.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ShipmentDocumentResponseLine }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "shipmentDocumentResponseLine")
    public JAXBElement<ShipmentDocumentResponseLine> createShipmentDocumentResponseLine(ShipmentDocumentResponseLine value) {
        return new JAXBElement<ShipmentDocumentResponseLine>(_ShipmentDocumentResponseLine_QNAME, ShipmentDocumentResponseLine.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ContainerInterface }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "containerInterface")
    public JAXBElement<ContainerInterface> createContainerInterface(ContainerInterface value) {
        return new JAXBElement<ContainerInterface>(_ContainerInterface_QNAME, ContainerInterface.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindControl }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/adf/svc/types/", name = "findControl")
    public JAXBElement<FindControl> createFindControl(FindControl value) {
        return new JAXBElement<FindControl>(_FindControl_QNAME, FindControl.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Type }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "commonj.sdo", name = "type")
    public JAXBElement<Type> createType(Type value) {
        return new JAXBElement<Type>(_Type_QNAME, Type.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindCriteria }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/adf/svc/types/", name = "findCriteria")
    public JAXBElement<FindCriteria> createFindCriteria(FindCriteria value) {
        return new JAXBElement<FindCriteria>(_FindCriteria_QNAME, FindCriteria.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ShipmentLineInterface }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "shipmentLineInterface")
    public JAXBElement<ShipmentLineInterface> createShipmentLineInterface(ShipmentLineInterface value) {
        return new JAXBElement<ShipmentLineInterface>(_ShipmentLineInterface_QNAME, ShipmentLineInterface.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ShipmentInterface }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "shipmentInterface")
    public JAXBElement<ShipmentInterface> createShipmentInterface(ShipmentInterface value) {
        return new JAXBElement<ShipmentInterface>(_ShipmentInterface_QNAME, ShipmentInterface.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Object }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "commonj.sdo", name = "dataObject")
    public JAXBElement<Object> createDataObject(Object value) {
        return new JAXBElement<Object>(_DataObject_QNAME, Object.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ShipmentDocumentResponseHeader }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "shipmentDocumentResponseHeader")
    public JAXBElement<ShipmentDocumentResponseHeader> createShipmentDocumentResponseHeader(ShipmentDocumentResponseHeader value) {
        return new JAXBElement<ShipmentDocumentResponseHeader>(_ShipmentDocumentResponseHeader_QNAME, ShipmentDocumentResponseHeader.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ShipmentNotes }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "shipmentNotes")
    public JAXBElement<ShipmentNotes> createShipmentNotes(ShipmentNotes value) {
        return new JAXBElement<ShipmentNotes>(_ShipmentNotes_QNAME, ShipmentNotes.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ShipmentResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "shipmentResponse")
    public JAXBElement<ShipmentResponse> createShipmentResponse(ShipmentResponse value) {
        return new JAXBElement<ShipmentResponse>(_ShipmentResponse_QNAME, ShipmentResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FreightCostInterface }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "freightCostInterface")
    public JAXBElement<FreightCostInterface> createFreightCostInterface(FreightCostInterface value) {
        return new JAXBElement<FreightCostInterface>(_FreightCostInterface_QNAME, FreightCostInterface.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ContainerInterfaceResult }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "containerInterfaceResult")
    public JAXBElement<ContainerInterfaceResult> createContainerInterfaceResult(ContainerInterfaceResult value) {
        return new JAXBElement<ContainerInterfaceResult>(_ContainerInterfaceResult_QNAME, ContainerInterfaceResult.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ServiceErrorMessage }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/adf/svc/errors/", name = "ServiceErrorMessage")
    public JAXBElement<ServiceErrorMessage> createServiceErrorMessage(ServiceErrorMessage value) {
        return new JAXBElement<ServiceErrorMessage>(_ServiceErrorMessage_QNAME, ServiceErrorMessage.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/types/", name = "DeferInventoryUpdates", scope = ProcessShipmentAction.class)
    public JAXBElement<String> createProcessShipmentActionDeferInventoryUpdates(String value) {
        return new JAXBElement<String>(_ProcessShipmentActionDeferInventoryUpdates_QNAME, String.class, ProcessShipmentAction.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/types/", name = "InitMsgList", scope = ProcessShipmentAction.class)
    public JAXBElement<String> createProcessShipmentActionInitMsgList(String value) {
        return new JAXBElement<String>(_ProcessShipmentActionInitMsgList_QNAME, String.class, ProcessShipmentAction.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/types/", name = "ActionType", scope = ProcessShipmentAction.class)
    public JAXBElement<String> createProcessShipmentActionActionType(String value) {
        return new JAXBElement<String>(_ProcessShipmentActionActionType_QNAME, String.class, ProcessShipmentAction.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/types/", name = "ShipConfirmRule", scope = ProcessShipmentAction.class)
    public JAXBElement<String> createProcessShipmentActionShipConfirmRule(String value) {
        return new JAXBElement<String>(_ProcessShipmentActionShipConfirmRule_QNAME, String.class, ProcessShipmentAction.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/types/", name = "CloseShipment", scope = ProcessShipmentAction.class)
    public JAXBElement<String> createProcessShipmentActionCloseShipment(String value) {
        return new JAXBElement<String>(_ProcessShipmentActionCloseShipment_QNAME, String.class, ProcessShipmentAction.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/types/", name = "CreateBillOfLading", scope = ProcessShipmentAction.class)
    public JAXBElement<String> createProcessShipmentActionCreateBillOfLading(String value) {
        return new JAXBElement<String>(_ProcessShipmentActionCreateBillOfLading_QNAME, String.class, ProcessShipmentAction.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/types/", name = "DocumentJobSet", scope = ProcessShipmentAction.class)
    public JAXBElement<String> createProcessShipmentActionDocumentJobSet(String value) {
        return new JAXBElement<String>(_ProcessShipmentActionDocumentJobSet_QNAME, String.class, ProcessShipmentAction.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/types/", name = "CreateShipmentForStagedQuantity", scope = ProcessShipmentAction.class)
    public JAXBElement<String> createProcessShipmentActionCreateShipmentForStagedQuantity(String value) {
        return new JAXBElement<String>(_ProcessShipmentActionCreateShipmentForStagedQuantity_QNAME, String.class, ProcessShipmentAction.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/types/", name = "PackingSlipStatus", scope = ProcessShipmentAction.class)
    public JAXBElement<String> createProcessShipmentActionPackingSlipStatus(String value) {
        return new JAXBElement<String>(_ProcessShipmentActionPackingSlipStatus_QNAME, String.class, ProcessShipmentAction.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/types/", name = "ActualShipDate", scope = ProcessShipmentAction.class)
    public JAXBElement<XMLGregorianCalendar> createProcessShipmentActionActualShipDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_ProcessShipmentActionActualShipDate_QNAME, XMLGregorianCalendar.class, ProcessShipmentAction.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/types/", name = "OverrideWeightVolume", scope = ProcessShipmentAction.class)
    public JAXBElement<String> createProcessShipmentActionOverrideWeightVolume(String value) {
        return new JAXBElement<String>(_ProcessShipmentActionOverrideWeightVolume_QNAME, String.class, ProcessShipmentAction.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/types/", name = "OutboundMessageCode", scope = ProcessShipmentAction.class)
    public JAXBElement<String> createProcessShipmentActionOutboundMessageCode(String value) {
        return new JAXBElement<String>(_ProcessShipmentActionOutboundMessageCode_QNAME, String.class, ProcessShipmentAction.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "CAttribute20", scope = InvSerialsInterface.class)
    public JAXBElement<String> createInvSerialsInterfaceCAttribute20(String value) {
        return new JAXBElement<String>(_InvSerialsInterfaceCAttribute20_QNAME, String.class, InvSerialsInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "AttributeDate1", scope = InvSerialsInterface.class)
    public JAXBElement<XMLGregorianCalendar> createInvSerialsInterfaceAttributeDate1(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_InvSerialsInterfaceAttributeDate1_QNAME, XMLGregorianCalendar.class, InvSerialsInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "AttributeDate5", scope = InvSerialsInterface.class)
    public JAXBElement<XMLGregorianCalendar> createInvSerialsInterfaceAttributeDate5(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_InvSerialsInterfaceAttributeDate5_QNAME, XMLGregorianCalendar.class, InvSerialsInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "AttributeDate4", scope = InvSerialsInterface.class)
    public JAXBElement<XMLGregorianCalendar> createInvSerialsInterfaceAttributeDate4(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_InvSerialsInterfaceAttributeDate4_QNAME, XMLGregorianCalendar.class, InvSerialsInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "AttributeDate3", scope = InvSerialsInterface.class)
    public JAXBElement<XMLGregorianCalendar> createInvSerialsInterfaceAttributeDate3(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_InvSerialsInterfaceAttributeDate3_QNAME, XMLGregorianCalendar.class, InvSerialsInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "AttributeDate2", scope = InvSerialsInterface.class)
    public JAXBElement<XMLGregorianCalendar> createInvSerialsInterfaceAttributeDate2(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_InvSerialsInterfaceAttributeDate2_QNAME, XMLGregorianCalendar.class, InvSerialsInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "AttributeNumber2", scope = InvSerialsInterface.class)
    public JAXBElement<BigDecimal> createInvSerialsInterfaceAttributeNumber2(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_InvSerialsInterfaceAttributeNumber2_QNAME, BigDecimal.class, InvSerialsInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "DAttribute10", scope = InvSerialsInterface.class)
    public JAXBElement<XMLGregorianCalendar> createInvSerialsInterfaceDAttribute10(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_InvSerialsInterfaceDAttribute10_QNAME, XMLGregorianCalendar.class, InvSerialsInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "AttributeNumber1", scope = InvSerialsInterface.class)
    public JAXBElement<BigDecimal> createInvSerialsInterfaceAttributeNumber1(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_InvSerialsInterfaceAttributeNumber1_QNAME, BigDecimal.class, InvSerialsInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "AttributeNumber4", scope = InvSerialsInterface.class)
    public JAXBElement<BigDecimal> createInvSerialsInterfaceAttributeNumber4(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_InvSerialsInterfaceAttributeNumber4_QNAME, BigDecimal.class, InvSerialsInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "AttributeNumber3", scope = InvSerialsInterface.class)
    public JAXBElement<BigDecimal> createInvSerialsInterfaceAttributeNumber3(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_InvSerialsInterfaceAttributeNumber3_QNAME, BigDecimal.class, InvSerialsInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "Attribute10", scope = InvSerialsInterface.class)
    public JAXBElement<String> createInvSerialsInterfaceAttribute10(String value) {
        return new JAXBElement<String>(_InvSerialsInterfaceAttribute10_QNAME, String.class, InvSerialsInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "AttributeNumber6", scope = InvSerialsInterface.class)
    public JAXBElement<BigDecimal> createInvSerialsInterfaceAttributeNumber6(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_InvSerialsInterfaceAttributeNumber6_QNAME, BigDecimal.class, InvSerialsInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "AttributeNumber5", scope = InvSerialsInterface.class)
    public JAXBElement<BigDecimal> createInvSerialsInterfaceAttributeNumber5(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_InvSerialsInterfaceAttributeNumber5_QNAME, BigDecimal.class, InvSerialsInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "AttributeNumber8", scope = InvSerialsInterface.class)
    public JAXBElement<BigDecimal> createInvSerialsInterfaceAttributeNumber8(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_InvSerialsInterfaceAttributeNumber8_QNAME, BigDecimal.class, InvSerialsInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "AttributeNumber7", scope = InvSerialsInterface.class)
    public JAXBElement<BigDecimal> createInvSerialsInterfaceAttributeNumber7(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_InvSerialsInterfaceAttributeNumber7_QNAME, BigDecimal.class, InvSerialsInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "Attribute14", scope = InvSerialsInterface.class)
    public JAXBElement<String> createInvSerialsInterfaceAttribute14(String value) {
        return new JAXBElement<String>(_InvSerialsInterfaceAttribute14_QNAME, String.class, InvSerialsInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "CAttribute19", scope = InvSerialsInterface.class)
    public JAXBElement<String> createInvSerialsInterfaceCAttribute19(String value) {
        return new JAXBElement<String>(_InvSerialsInterfaceCAttribute19_QNAME, String.class, InvSerialsInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "Attribute13", scope = InvSerialsInterface.class)
    public JAXBElement<String> createInvSerialsInterfaceAttribute13(String value) {
        return new JAXBElement<String>(_InvSerialsInterfaceAttribute13_QNAME, String.class, InvSerialsInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "AttributeNumber9", scope = InvSerialsInterface.class)
    public JAXBElement<BigDecimal> createInvSerialsInterfaceAttributeNumber9(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_InvSerialsInterfaceAttributeNumber9_QNAME, BigDecimal.class, InvSerialsInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "CAttribute18", scope = InvSerialsInterface.class)
    public JAXBElement<String> createInvSerialsInterfaceCAttribute18(String value) {
        return new JAXBElement<String>(_InvSerialsInterfaceCAttribute18_QNAME, String.class, InvSerialsInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "Attribute12", scope = InvSerialsInterface.class)
    public JAXBElement<String> createInvSerialsInterfaceAttribute12(String value) {
        return new JAXBElement<String>(_InvSerialsInterfaceAttribute12_QNAME, String.class, InvSerialsInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "Attribute11", scope = InvSerialsInterface.class)
    public JAXBElement<String> createInvSerialsInterfaceAttribute11(String value) {
        return new JAXBElement<String>(_InvSerialsInterfaceAttribute11_QNAME, String.class, InvSerialsInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "CAttribute1", scope = InvSerialsInterface.class)
    public JAXBElement<String> createInvSerialsInterfaceCAttribute1(String value) {
        return new JAXBElement<String>(_InvSerialsInterfaceCAttribute1_QNAME, String.class, InvSerialsInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "CAttribute2", scope = InvSerialsInterface.class)
    public JAXBElement<String> createInvSerialsInterfaceCAttribute2(String value) {
        return new JAXBElement<String>(_InvSerialsInterfaceCAttribute2_QNAME, String.class, InvSerialsInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "CAttribute3", scope = InvSerialsInterface.class)
    public JAXBElement<String> createInvSerialsInterfaceCAttribute3(String value) {
        return new JAXBElement<String>(_InvSerialsInterfaceCAttribute3_QNAME, String.class, InvSerialsInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "CAttribute4", scope = InvSerialsInterface.class)
    public JAXBElement<String> createInvSerialsInterfaceCAttribute4(String value) {
        return new JAXBElement<String>(_InvSerialsInterfaceCAttribute4_QNAME, String.class, InvSerialsInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "CAttribute5", scope = InvSerialsInterface.class)
    public JAXBElement<String> createInvSerialsInterfaceCAttribute5(String value) {
        return new JAXBElement<String>(_InvSerialsInterfaceCAttribute5_QNAME, String.class, InvSerialsInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "CAttribute6", scope = InvSerialsInterface.class)
    public JAXBElement<String> createInvSerialsInterfaceCAttribute6(String value) {
        return new JAXBElement<String>(_InvSerialsInterfaceCAttribute6_QNAME, String.class, InvSerialsInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "CAttribute7", scope = InvSerialsInterface.class)
    public JAXBElement<String> createInvSerialsInterfaceCAttribute7(String value) {
        return new JAXBElement<String>(_InvSerialsInterfaceCAttribute7_QNAME, String.class, InvSerialsInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "CAttribute8", scope = InvSerialsInterface.class)
    public JAXBElement<String> createInvSerialsInterfaceCAttribute8(String value) {
        return new JAXBElement<String>(_InvSerialsInterfaceCAttribute8_QNAME, String.class, InvSerialsInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "Attribute18", scope = InvSerialsInterface.class)
    public JAXBElement<String> createInvSerialsInterfaceAttribute18(String value) {
        return new JAXBElement<String>(_InvSerialsInterfaceAttribute18_QNAME, String.class, InvSerialsInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "Attribute17", scope = InvSerialsInterface.class)
    public JAXBElement<String> createInvSerialsInterfaceAttribute17(String value) {
        return new JAXBElement<String>(_InvSerialsInterfaceAttribute17_QNAME, String.class, InvSerialsInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "Attribute16", scope = InvSerialsInterface.class)
    public JAXBElement<String> createInvSerialsInterfaceAttribute16(String value) {
        return new JAXBElement<String>(_InvSerialsInterfaceAttribute16_QNAME, String.class, InvSerialsInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "Attribute15", scope = InvSerialsInterface.class)
    public JAXBElement<String> createInvSerialsInterfaceAttribute15(String value) {
        return new JAXBElement<String>(_InvSerialsInterfaceAttribute15_QNAME, String.class, InvSerialsInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "Attribute19", scope = InvSerialsInterface.class)
    public JAXBElement<String> createInvSerialsInterfaceAttribute19(String value) {
        return new JAXBElement<String>(_InvSerialsInterfaceAttribute19_QNAME, String.class, InvSerialsInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "Attribute1", scope = InvSerialsInterface.class)
    public JAXBElement<String> createInvSerialsInterfaceAttribute1(String value) {
        return new JAXBElement<String>(_InvSerialsInterfaceAttribute1_QNAME, String.class, InvSerialsInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "Attribute20", scope = InvSerialsInterface.class)
    public JAXBElement<String> createInvSerialsInterfaceAttribute20(String value) {
        return new JAXBElement<String>(_InvSerialsInterfaceAttribute20_QNAME, String.class, InvSerialsInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "Attribute3", scope = InvSerialsInterface.class)
    public JAXBElement<String> createInvSerialsInterfaceAttribute3(String value) {
        return new JAXBElement<String>(_InvSerialsInterfaceAttribute3_QNAME, String.class, InvSerialsInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "AttributeCategory", scope = InvSerialsInterface.class)
    public JAXBElement<String> createInvSerialsInterfaceAttributeCategory(String value) {
        return new JAXBElement<String>(_InvSerialsInterfaceAttributeCategory_QNAME, String.class, InvSerialsInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "Attribute2", scope = InvSerialsInterface.class)
    public JAXBElement<String> createInvSerialsInterfaceAttribute2(String value) {
        return new JAXBElement<String>(_InvSerialsInterfaceAttribute2_QNAME, String.class, InvSerialsInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "SubinventoryCode", scope = InvSerialsInterface.class)
    public JAXBElement<String> createInvSerialsInterfaceSubinventoryCode(String value) {
        return new JAXBElement<String>(_InvSerialsInterfaceSubinventoryCode_QNAME, String.class, InvSerialsInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "Attribute9", scope = InvSerialsInterface.class)
    public JAXBElement<String> createInvSerialsInterfaceAttribute9(String value) {
        return new JAXBElement<String>(_InvSerialsInterfaceAttribute9_QNAME, String.class, InvSerialsInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "DAttribute5", scope = InvSerialsInterface.class)
    public JAXBElement<XMLGregorianCalendar> createInvSerialsInterfaceDAttribute5(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_InvSerialsInterfaceDAttribute5_QNAME, XMLGregorianCalendar.class, InvSerialsInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "Attribute8", scope = InvSerialsInterface.class)
    public JAXBElement<String> createInvSerialsInterfaceAttribute8(String value) {
        return new JAXBElement<String>(_InvSerialsInterfaceAttribute8_QNAME, String.class, InvSerialsInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "DAttribute4", scope = InvSerialsInterface.class)
    public JAXBElement<XMLGregorianCalendar> createInvSerialsInterfaceDAttribute4(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_InvSerialsInterfaceDAttribute4_QNAME, XMLGregorianCalendar.class, InvSerialsInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "DAttribute7", scope = InvSerialsInterface.class)
    public JAXBElement<XMLGregorianCalendar> createInvSerialsInterfaceDAttribute7(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_InvSerialsInterfaceDAttribute7_QNAME, XMLGregorianCalendar.class, InvSerialsInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "DAttribute6", scope = InvSerialsInterface.class)
    public JAXBElement<XMLGregorianCalendar> createInvSerialsInterfaceDAttribute6(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_InvSerialsInterfaceDAttribute6_QNAME, XMLGregorianCalendar.class, InvSerialsInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "Attribute5", scope = InvSerialsInterface.class)
    public JAXBElement<String> createInvSerialsInterfaceAttribute5(String value) {
        return new JAXBElement<String>(_InvSerialsInterfaceAttribute5_QNAME, String.class, InvSerialsInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "DAttribute1", scope = InvSerialsInterface.class)
    public JAXBElement<XMLGregorianCalendar> createInvSerialsInterfaceDAttribute1(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_InvSerialsInterfaceDAttribute1_QNAME, XMLGregorianCalendar.class, InvSerialsInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "ToSerialNumber", scope = InvSerialsInterface.class)
    public JAXBElement<String> createInvSerialsInterfaceToSerialNumber(String value) {
        return new JAXBElement<String>(_InvSerialsInterfaceToSerialNumber_QNAME, String.class, InvSerialsInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "Attribute4", scope = InvSerialsInterface.class)
    public JAXBElement<String> createInvSerialsInterfaceAttribute4(String value) {
        return new JAXBElement<String>(_InvSerialsInterfaceAttribute4_QNAME, String.class, InvSerialsInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "Attribute7", scope = InvSerialsInterface.class)
    public JAXBElement<String> createInvSerialsInterfaceAttribute7(String value) {
        return new JAXBElement<String>(_InvSerialsInterfaceAttribute7_QNAME, String.class, InvSerialsInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "DAttribute3", scope = InvSerialsInterface.class)
    public JAXBElement<XMLGregorianCalendar> createInvSerialsInterfaceDAttribute3(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_InvSerialsInterfaceDAttribute3_QNAME, XMLGregorianCalendar.class, InvSerialsInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "SerialAttributeCategory", scope = InvSerialsInterface.class)
    public JAXBElement<String> createInvSerialsInterfaceSerialAttributeCategory(String value) {
        return new JAXBElement<String>(_InvSerialsInterfaceSerialAttributeCategory_QNAME, String.class, InvSerialsInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "Attribute6", scope = InvSerialsInterface.class)
    public JAXBElement<String> createInvSerialsInterfaceAttribute6(String value) {
        return new JAXBElement<String>(_InvSerialsInterfaceAttribute6_QNAME, String.class, InvSerialsInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "DAttribute2", scope = InvSerialsInterface.class)
    public JAXBElement<XMLGregorianCalendar> createInvSerialsInterfaceDAttribute2(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_InvSerialsInterfaceDAttribute2_QNAME, XMLGregorianCalendar.class, InvSerialsInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "DAttribute9", scope = InvSerialsInterface.class)
    public JAXBElement<XMLGregorianCalendar> createInvSerialsInterfaceDAttribute9(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_InvSerialsInterfaceDAttribute9_QNAME, XMLGregorianCalendar.class, InvSerialsInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "DAttribute8", scope = InvSerialsInterface.class)
    public JAXBElement<XMLGregorianCalendar> createInvSerialsInterfaceDAttribute8(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_InvSerialsInterfaceDAttribute8_QNAME, XMLGregorianCalendar.class, InvSerialsInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "NAttribute5", scope = InvSerialsInterface.class)
    public JAXBElement<BigDecimal> createInvSerialsInterfaceNAttribute5(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_InvSerialsInterfaceNAttribute5_QNAME, BigDecimal.class, InvSerialsInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "NAttribute4", scope = InvSerialsInterface.class)
    public JAXBElement<BigDecimal> createInvSerialsInterfaceNAttribute4(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_InvSerialsInterfaceNAttribute4_QNAME, BigDecimal.class, InvSerialsInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "NAttribute3", scope = InvSerialsInterface.class)
    public JAXBElement<BigDecimal> createInvSerialsInterfaceNAttribute3(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_InvSerialsInterfaceNAttribute3_QNAME, BigDecimal.class, InvSerialsInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "NAttribute2", scope = InvSerialsInterface.class)
    public JAXBElement<BigDecimal> createInvSerialsInterfaceNAttribute2(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_InvSerialsInterfaceNAttribute2_QNAME, BigDecimal.class, InvSerialsInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "NAttribute1", scope = InvSerialsInterface.class)
    public JAXBElement<BigDecimal> createInvSerialsInterfaceNAttribute1(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_InvSerialsInterfaceNAttribute1_QNAME, BigDecimal.class, InvSerialsInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "NAttribute9", scope = InvSerialsInterface.class)
    public JAXBElement<BigDecimal> createInvSerialsInterfaceNAttribute9(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_InvSerialsInterfaceNAttribute9_QNAME, BigDecimal.class, InvSerialsInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "NAttribute8", scope = InvSerialsInterface.class)
    public JAXBElement<BigDecimal> createInvSerialsInterfaceNAttribute8(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_InvSerialsInterfaceNAttribute8_QNAME, BigDecimal.class, InvSerialsInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "NAttribute7", scope = InvSerialsInterface.class)
    public JAXBElement<BigDecimal> createInvSerialsInterfaceNAttribute7(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_InvSerialsInterfaceNAttribute7_QNAME, BigDecimal.class, InvSerialsInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "NAttribute6", scope = InvSerialsInterface.class)
    public JAXBElement<BigDecimal> createInvSerialsInterfaceNAttribute6(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_InvSerialsInterfaceNAttribute6_QNAME, BigDecimal.class, InvSerialsInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "CAttribute9", scope = InvSerialsInterface.class)
    public JAXBElement<String> createInvSerialsInterfaceCAttribute9(String value) {
        return new JAXBElement<String>(_InvSerialsInterfaceCAttribute9_QNAME, String.class, InvSerialsInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "AttributeNumber10", scope = InvSerialsInterface.class)
    public JAXBElement<BigDecimal> createInvSerialsInterfaceAttributeNumber10(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_InvSerialsInterfaceAttributeNumber10_QNAME, BigDecimal.class, InvSerialsInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "NAttribute10", scope = InvSerialsInterface.class)
    public JAXBElement<BigDecimal> createInvSerialsInterfaceNAttribute10(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_InvSerialsInterfaceNAttribute10_QNAME, BigDecimal.class, InvSerialsInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "Locator", scope = InvSerialsInterface.class)
    public JAXBElement<String> createInvSerialsInterfaceLocator(String value) {
        return new JAXBElement<String>(_InvSerialsInterfaceLocator_QNAME, String.class, InvSerialsInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "CAttribute15", scope = InvSerialsInterface.class)
    public JAXBElement<String> createInvSerialsInterfaceCAttribute15(String value) {
        return new JAXBElement<String>(_InvSerialsInterfaceCAttribute15_QNAME, String.class, InvSerialsInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "CAttribute14", scope = InvSerialsInterface.class)
    public JAXBElement<String> createInvSerialsInterfaceCAttribute14(String value) {
        return new JAXBElement<String>(_InvSerialsInterfaceCAttribute14_QNAME, String.class, InvSerialsInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "CAttribute17", scope = InvSerialsInterface.class)
    public JAXBElement<String> createInvSerialsInterfaceCAttribute17(String value) {
        return new JAXBElement<String>(_InvSerialsInterfaceCAttribute17_QNAME, String.class, InvSerialsInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "CAttribute16", scope = InvSerialsInterface.class)
    public JAXBElement<String> createInvSerialsInterfaceCAttribute16(String value) {
        return new JAXBElement<String>(_InvSerialsInterfaceCAttribute16_QNAME, String.class, InvSerialsInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "CAttribute11", scope = InvSerialsInterface.class)
    public JAXBElement<String> createInvSerialsInterfaceCAttribute11(String value) {
        return new JAXBElement<String>(_InvSerialsInterfaceCAttribute11_QNAME, String.class, InvSerialsInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "CAttribute10", scope = InvSerialsInterface.class)
    public JAXBElement<String> createInvSerialsInterfaceCAttribute10(String value) {
        return new JAXBElement<String>(_InvSerialsInterfaceCAttribute10_QNAME, String.class, InvSerialsInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "CAttribute13", scope = InvSerialsInterface.class)
    public JAXBElement<String> createInvSerialsInterfaceCAttribute13(String value) {
        return new JAXBElement<String>(_InvSerialsInterfaceCAttribute13_QNAME, String.class, InvSerialsInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "CAttribute12", scope = InvSerialsInterface.class)
    public JAXBElement<String> createInvSerialsInterfaceCAttribute12(String value) {
        return new JAXBElement<String>(_InvSerialsInterfaceCAttribute12_QNAME, String.class, InvSerialsInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "AttributeTimestamp1", scope = InvSerialsInterface.class)
    public JAXBElement<XMLGregorianCalendar> createInvSerialsInterfaceAttributeTimestamp1(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_InvSerialsInterfaceAttributeTimestamp1_QNAME, XMLGregorianCalendar.class, InvSerialsInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "AttributeTimestamp5", scope = InvSerialsInterface.class)
    public JAXBElement<XMLGregorianCalendar> createInvSerialsInterfaceAttributeTimestamp5(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_InvSerialsInterfaceAttributeTimestamp5_QNAME, XMLGregorianCalendar.class, InvSerialsInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "AttributeTimestamp4", scope = InvSerialsInterface.class)
    public JAXBElement<XMLGregorianCalendar> createInvSerialsInterfaceAttributeTimestamp4(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_InvSerialsInterfaceAttributeTimestamp4_QNAME, XMLGregorianCalendar.class, InvSerialsInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "TAttribute1", scope = InvSerialsInterface.class)
    public JAXBElement<XMLGregorianCalendar> createInvSerialsInterfaceTAttribute1(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_InvSerialsInterfaceTAttribute1_QNAME, XMLGregorianCalendar.class, InvSerialsInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "AttributeTimestamp3", scope = InvSerialsInterface.class)
    public JAXBElement<XMLGregorianCalendar> createInvSerialsInterfaceAttributeTimestamp3(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_InvSerialsInterfaceAttributeTimestamp3_QNAME, XMLGregorianCalendar.class, InvSerialsInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "TAttribute2", scope = InvSerialsInterface.class)
    public JAXBElement<XMLGregorianCalendar> createInvSerialsInterfaceTAttribute2(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_InvSerialsInterfaceTAttribute2_QNAME, XMLGregorianCalendar.class, InvSerialsInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "AttributeTimestamp2", scope = InvSerialsInterface.class)
    public JAXBElement<XMLGregorianCalendar> createInvSerialsInterfaceAttributeTimestamp2(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_InvSerialsInterfaceAttributeTimestamp2_QNAME, XMLGregorianCalendar.class, InvSerialsInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "TAttribute3", scope = InvSerialsInterface.class)
    public JAXBElement<XMLGregorianCalendar> createInvSerialsInterfaceTAttribute3(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_InvSerialsInterfaceTAttribute3_QNAME, XMLGregorianCalendar.class, InvSerialsInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "TAttribute4", scope = InvSerialsInterface.class)
    public JAXBElement<XMLGregorianCalendar> createInvSerialsInterfaceTAttribute4(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_InvSerialsInterfaceTAttribute4_QNAME, XMLGregorianCalendar.class, InvSerialsInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "TAttribute5", scope = InvSerialsInterface.class)
    public JAXBElement<XMLGregorianCalendar> createInvSerialsInterfaceTAttribute5(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_InvSerialsInterfaceTAttribute5_QNAME, XMLGregorianCalendar.class, InvSerialsInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "AttributeDate1", scope = FreightCostInterface.class)
    public JAXBElement<XMLGregorianCalendar> createFreightCostInterfaceAttributeDate1(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_InvSerialsInterfaceAttributeDate1_QNAME, XMLGregorianCalendar.class, FreightCostInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "ConversionDate", scope = FreightCostInterface.class)
    public JAXBElement<XMLGregorianCalendar> createFreightCostInterfaceConversionDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_FreightCostInterfaceConversionDate_QNAME, XMLGregorianCalendar.class, FreightCostInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "AttributeDate5", scope = FreightCostInterface.class)
    public JAXBElement<XMLGregorianCalendar> createFreightCostInterfaceAttributeDate5(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_InvSerialsInterfaceAttributeDate5_QNAME, XMLGregorianCalendar.class, FreightCostInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "AttributeDate4", scope = FreightCostInterface.class)
    public JAXBElement<XMLGregorianCalendar> createFreightCostInterfaceAttributeDate4(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_InvSerialsInterfaceAttributeDate4_QNAME, XMLGregorianCalendar.class, FreightCostInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "AttributeDate3", scope = FreightCostInterface.class)
    public JAXBElement<XMLGregorianCalendar> createFreightCostInterfaceAttributeDate3(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_InvSerialsInterfaceAttributeDate3_QNAME, XMLGregorianCalendar.class, FreightCostInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "AttributeDate2", scope = FreightCostInterface.class)
    public JAXBElement<XMLGregorianCalendar> createFreightCostInterfaceAttributeDate2(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_InvSerialsInterfaceAttributeDate2_QNAME, XMLGregorianCalendar.class, FreightCostInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "AttributeNumber2", scope = FreightCostInterface.class)
    public JAXBElement<BigDecimal> createFreightCostInterfaceAttributeNumber2(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_InvSerialsInterfaceAttributeNumber2_QNAME, BigDecimal.class, FreightCostInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "AttributeNumber1", scope = FreightCostInterface.class)
    public JAXBElement<BigDecimal> createFreightCostInterfaceAttributeNumber1(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_InvSerialsInterfaceAttributeNumber1_QNAME, BigDecimal.class, FreightCostInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "ConversionRate", scope = FreightCostInterface.class)
    public JAXBElement<BigDecimal> createFreightCostInterfaceConversionRate(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_FreightCostInterfaceConversionRate_QNAME, BigDecimal.class, FreightCostInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "AttributeNumber4", scope = FreightCostInterface.class)
    public JAXBElement<BigDecimal> createFreightCostInterfaceAttributeNumber4(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_InvSerialsInterfaceAttributeNumber4_QNAME, BigDecimal.class, FreightCostInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "AttributeNumber3", scope = FreightCostInterface.class)
    public JAXBElement<BigDecimal> createFreightCostInterfaceAttributeNumber3(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_InvSerialsInterfaceAttributeNumber3_QNAME, BigDecimal.class, FreightCostInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "Attribute10", scope = FreightCostInterface.class)
    public JAXBElement<String> createFreightCostInterfaceAttribute10(String value) {
        return new JAXBElement<String>(_InvSerialsInterfaceAttribute10_QNAME, String.class, FreightCostInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "AttributeNumber6", scope = FreightCostInterface.class)
    public JAXBElement<BigDecimal> createFreightCostInterfaceAttributeNumber6(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_InvSerialsInterfaceAttributeNumber6_QNAME, BigDecimal.class, FreightCostInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "AttributeNumber5", scope = FreightCostInterface.class)
    public JAXBElement<BigDecimal> createFreightCostInterfaceAttributeNumber5(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_InvSerialsInterfaceAttributeNumber5_QNAME, BigDecimal.class, FreightCostInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "AttributeNumber8", scope = FreightCostInterface.class)
    public JAXBElement<BigDecimal> createFreightCostInterfaceAttributeNumber8(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_InvSerialsInterfaceAttributeNumber8_QNAME, BigDecimal.class, FreightCostInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "AttributeNumber7", scope = FreightCostInterface.class)
    public JAXBElement<BigDecimal> createFreightCostInterfaceAttributeNumber7(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_InvSerialsInterfaceAttributeNumber7_QNAME, BigDecimal.class, FreightCostInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "Attribute14", scope = FreightCostInterface.class)
    public JAXBElement<String> createFreightCostInterfaceAttribute14(String value) {
        return new JAXBElement<String>(_InvSerialsInterfaceAttribute14_QNAME, String.class, FreightCostInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "Attribute13", scope = FreightCostInterface.class)
    public JAXBElement<String> createFreightCostInterfaceAttribute13(String value) {
        return new JAXBElement<String>(_InvSerialsInterfaceAttribute13_QNAME, String.class, FreightCostInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "AttributeNumber9", scope = FreightCostInterface.class)
    public JAXBElement<BigDecimal> createFreightCostInterfaceAttributeNumber9(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_InvSerialsInterfaceAttributeNumber9_QNAME, BigDecimal.class, FreightCostInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "Attribute12", scope = FreightCostInterface.class)
    public JAXBElement<String> createFreightCostInterfaceAttribute12(String value) {
        return new JAXBElement<String>(_InvSerialsInterfaceAttribute12_QNAME, String.class, FreightCostInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "Attribute11", scope = FreightCostInterface.class)
    public JAXBElement<String> createFreightCostInterfaceAttribute11(String value) {
        return new JAXBElement<String>(_InvSerialsInterfaceAttribute11_QNAME, String.class, FreightCostInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "AttributeNumber10", scope = FreightCostInterface.class)
    public JAXBElement<BigDecimal> createFreightCostInterfaceAttributeNumber10(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_InvSerialsInterfaceAttributeNumber10_QNAME, BigDecimal.class, FreightCostInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "Attribute18", scope = FreightCostInterface.class)
    public JAXBElement<String> createFreightCostInterfaceAttribute18(String value) {
        return new JAXBElement<String>(_InvSerialsInterfaceAttribute18_QNAME, String.class, FreightCostInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "Attribute17", scope = FreightCostInterface.class)
    public JAXBElement<String> createFreightCostInterfaceAttribute17(String value) {
        return new JAXBElement<String>(_InvSerialsInterfaceAttribute17_QNAME, String.class, FreightCostInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "Attribute16", scope = FreightCostInterface.class)
    public JAXBElement<String> createFreightCostInterfaceAttribute16(String value) {
        return new JAXBElement<String>(_InvSerialsInterfaceAttribute16_QNAME, String.class, FreightCostInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "Attribute15", scope = FreightCostInterface.class)
    public JAXBElement<String> createFreightCostInterfaceAttribute15(String value) {
        return new JAXBElement<String>(_InvSerialsInterfaceAttribute15_QNAME, String.class, FreightCostInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "Attribute19", scope = FreightCostInterface.class)
    public JAXBElement<String> createFreightCostInterfaceAttribute19(String value) {
        return new JAXBElement<String>(_InvSerialsInterfaceAttribute19_QNAME, String.class, FreightCostInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "Attribute1", scope = FreightCostInterface.class)
    public JAXBElement<String> createFreightCostInterfaceAttribute1(String value) {
        return new JAXBElement<String>(_InvSerialsInterfaceAttribute1_QNAME, String.class, FreightCostInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "Attribute20", scope = FreightCostInterface.class)
    public JAXBElement<String> createFreightCostInterfaceAttribute20(String value) {
        return new JAXBElement<String>(_InvSerialsInterfaceAttribute20_QNAME, String.class, FreightCostInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "AttributeCategory", scope = FreightCostInterface.class)
    public JAXBElement<String> createFreightCostInterfaceAttributeCategory(String value) {
        return new JAXBElement<String>(_InvSerialsInterfaceAttributeCategory_QNAME, String.class, FreightCostInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "Attribute3", scope = FreightCostInterface.class)
    public JAXBElement<String> createFreightCostInterfaceAttribute3(String value) {
        return new JAXBElement<String>(_InvSerialsInterfaceAttribute3_QNAME, String.class, FreightCostInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "Attribute2", scope = FreightCostInterface.class)
    public JAXBElement<String> createFreightCostInterfaceAttribute2(String value) {
        return new JAXBElement<String>(_InvSerialsInterfaceAttribute2_QNAME, String.class, FreightCostInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "AttributeTimestamp1", scope = FreightCostInterface.class)
    public JAXBElement<XMLGregorianCalendar> createFreightCostInterfaceAttributeTimestamp1(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_InvSerialsInterfaceAttributeTimestamp1_QNAME, XMLGregorianCalendar.class, FreightCostInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "FreightCostName", scope = FreightCostInterface.class)
    public JAXBElement<String> createFreightCostInterfaceFreightCostName(String value) {
        return new JAXBElement<String>(_FreightCostInterfaceFreightCostName_QNAME, String.class, FreightCostInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "Attribute9", scope = FreightCostInterface.class)
    public JAXBElement<String> createFreightCostInterfaceAttribute9(String value) {
        return new JAXBElement<String>(_InvSerialsInterfaceAttribute9_QNAME, String.class, FreightCostInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "AttributeTimestamp5", scope = FreightCostInterface.class)
    public JAXBElement<XMLGregorianCalendar> createFreightCostInterfaceAttributeTimestamp5(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_InvSerialsInterfaceAttributeTimestamp5_QNAME, XMLGregorianCalendar.class, FreightCostInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "Attribute8", scope = FreightCostInterface.class)
    public JAXBElement<String> createFreightCostInterfaceAttribute8(String value) {
        return new JAXBElement<String>(_InvSerialsInterfaceAttribute8_QNAME, String.class, FreightCostInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "AttributeTimestamp4", scope = FreightCostInterface.class)
    public JAXBElement<XMLGregorianCalendar> createFreightCostInterfaceAttributeTimestamp4(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_InvSerialsInterfaceAttributeTimestamp4_QNAME, XMLGregorianCalendar.class, FreightCostInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "AttributeTimestamp3", scope = FreightCostInterface.class)
    public JAXBElement<XMLGregorianCalendar> createFreightCostInterfaceAttributeTimestamp3(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_InvSerialsInterfaceAttributeTimestamp3_QNAME, XMLGregorianCalendar.class, FreightCostInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "ConversionTypeCode", scope = FreightCostInterface.class)
    public JAXBElement<String> createFreightCostInterfaceConversionTypeCode(String value) {
        return new JAXBElement<String>(_FreightCostInterfaceConversionTypeCode_QNAME, String.class, FreightCostInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "AttributeTimestamp2", scope = FreightCostInterface.class)
    public JAXBElement<XMLGregorianCalendar> createFreightCostInterfaceAttributeTimestamp2(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_InvSerialsInterfaceAttributeTimestamp2_QNAME, XMLGregorianCalendar.class, FreightCostInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "Attribute5", scope = FreightCostInterface.class)
    public JAXBElement<String> createFreightCostInterfaceAttribute5(String value) {
        return new JAXBElement<String>(_InvSerialsInterfaceAttribute5_QNAME, String.class, FreightCostInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "Attribute4", scope = FreightCostInterface.class)
    public JAXBElement<String> createFreightCostInterfaceAttribute4(String value) {
        return new JAXBElement<String>(_InvSerialsInterfaceAttribute4_QNAME, String.class, FreightCostInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "Attribute7", scope = FreightCostInterface.class)
    public JAXBElement<String> createFreightCostInterfaceAttribute7(String value) {
        return new JAXBElement<String>(_InvSerialsInterfaceAttribute7_QNAME, String.class, FreightCostInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "Attribute6", scope = FreightCostInterface.class)
    public JAXBElement<String> createFreightCostInterfaceAttribute6(String value) {
        return new JAXBElement<String>(_InvSerialsInterfaceAttribute6_QNAME, String.class, FreightCostInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/types/", name = "ToActualShipDate", scope = FetchShipmentDocuments.class)
    public JAXBElement<XMLGregorianCalendar> createFetchShipmentDocumentsToActualShipDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_FetchShipmentDocumentsToActualShipDate_QNAME, XMLGregorianCalendar.class, FetchShipmentDocuments.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/types/", name = "ToCreationDate", scope = FetchShipmentDocuments.class)
    public JAXBElement<XMLGregorianCalendar> createFetchShipmentDocumentsToCreationDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_FetchShipmentDocumentsToCreationDate_QNAME, XMLGregorianCalendar.class, FetchShipmentDocuments.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/types/", name = "FromActualShipDate", scope = FetchShipmentDocuments.class)
    public JAXBElement<XMLGregorianCalendar> createFetchShipmentDocumentsFromActualShipDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_FetchShipmentDocumentsFromActualShipDate_QNAME, XMLGregorianCalendar.class, FetchShipmentDocuments.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/types/", name = "Shipment", scope = FetchShipmentDocuments.class)
    public JAXBElement<String> createFetchShipmentDocumentsShipment(String value) {
        return new JAXBElement<String>(_FetchShipmentDocumentsShipment_QNAME, String.class, FetchShipmentDocuments.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/types/", name = "ToConfirmedDate", scope = FetchShipmentDocuments.class)
    public JAXBElement<XMLGregorianCalendar> createFetchShipmentDocumentsToConfirmedDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_FetchShipmentDocumentsToConfirmedDate_QNAME, XMLGregorianCalendar.class, FetchShipmentDocuments.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/types/", name = "FromCreationDate", scope = FetchShipmentDocuments.class)
    public JAXBElement<XMLGregorianCalendar> createFetchShipmentDocumentsFromCreationDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_FetchShipmentDocumentsFromCreationDate_QNAME, XMLGregorianCalendar.class, FetchShipmentDocuments.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/types/", name = "FromConfirmedDate", scope = FetchShipmentDocuments.class)
    public JAXBElement<XMLGregorianCalendar> createFetchShipmentDocumentsFromConfirmedDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_FetchShipmentDocumentsFromConfirmedDate_QNAME, XMLGregorianCalendar.class, FetchShipmentDocuments.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/types/", name = "InitMsgList", scope = ProcessCreateUpdateShipment.class)
    public JAXBElement<String> createProcessCreateUpdateShipmentInitMsgList(String value) {
        return new JAXBElement<String>(_ProcessShipmentActionInitMsgList_QNAME, String.class, ProcessCreateUpdateShipment.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "LoadingSequence", scope = ShipmentLineInterface.class)
    public JAXBElement<BigDecimal> createShipmentLineInterfaceLoadingSequence(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_ShipmentLineInterfaceLoadingSequence_QNAME, BigDecimal.class, ShipmentLineInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "AttributeDate1", scope = ShipmentLineInterface.class)
    public JAXBElement<XMLGregorianCalendar> createShipmentLineInterfaceAttributeDate1(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_InvSerialsInterfaceAttributeDate1_QNAME, XMLGregorianCalendar.class, ShipmentLineInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "AttributeDate5", scope = ShipmentLineInterface.class)
    public JAXBElement<XMLGregorianCalendar> createShipmentLineInterfaceAttributeDate5(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_InvSerialsInterfaceAttributeDate5_QNAME, XMLGregorianCalendar.class, ShipmentLineInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "AttributeDate4", scope = ShipmentLineInterface.class)
    public JAXBElement<XMLGregorianCalendar> createShipmentLineInterfaceAttributeDate4(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_InvSerialsInterfaceAttributeDate4_QNAME, XMLGregorianCalendar.class, ShipmentLineInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "AttributeDate3", scope = ShipmentLineInterface.class)
    public JAXBElement<XMLGregorianCalendar> createShipmentLineInterfaceAttributeDate3(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_InvSerialsInterfaceAttributeDate3_QNAME, XMLGregorianCalendar.class, ShipmentLineInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "AttributeDate2", scope = ShipmentLineInterface.class)
    public JAXBElement<XMLGregorianCalendar> createShipmentLineInterfaceAttributeDate2(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_InvSerialsInterfaceAttributeDate2_QNAME, XMLGregorianCalendar.class, ShipmentLineInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "AttributeNumber2", scope = ShipmentLineInterface.class)
    public JAXBElement<BigDecimal> createShipmentLineInterfaceAttributeNumber2(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_InvSerialsInterfaceAttributeNumber2_QNAME, BigDecimal.class, ShipmentLineInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "Subinventory", scope = ShipmentLineInterface.class)
    public JAXBElement<String> createShipmentLineInterfaceSubinventory(String value) {
        return new JAXBElement<String>(_ShipmentLineInterfaceSubinventory_QNAME, String.class, ShipmentLineInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "AttributeNumber1", scope = ShipmentLineInterface.class)
    public JAXBElement<BigDecimal> createShipmentLineInterfaceAttributeNumber1(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_InvSerialsInterfaceAttributeNumber1_QNAME, BigDecimal.class, ShipmentLineInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "ProductFiscClassification", scope = ShipmentLineInterface.class)
    public JAXBElement<String> createShipmentLineInterfaceProductFiscClassification(String value) {
        return new JAXBElement<String>(_ShipmentLineInterfaceProductFiscClassification_QNAME, String.class, ShipmentLineInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "AttributeNumber4", scope = ShipmentLineInterface.class)
    public JAXBElement<BigDecimal> createShipmentLineInterfaceAttributeNumber4(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_InvSerialsInterfaceAttributeNumber4_QNAME, BigDecimal.class, ShipmentLineInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "AttributeNumber3", scope = ShipmentLineInterface.class)
    public JAXBElement<BigDecimal> createShipmentLineInterfaceAttributeNumber3(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_InvSerialsInterfaceAttributeNumber3_QNAME, BigDecimal.class, ShipmentLineInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "Attribute10", scope = ShipmentLineInterface.class)
    public JAXBElement<String> createShipmentLineInterfaceAttribute10(String value) {
        return new JAXBElement<String>(_InvSerialsInterfaceAttribute10_QNAME, String.class, ShipmentLineInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "AttributeNumber6", scope = ShipmentLineInterface.class)
    public JAXBElement<BigDecimal> createShipmentLineInterfaceAttributeNumber6(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_InvSerialsInterfaceAttributeNumber6_QNAME, BigDecimal.class, ShipmentLineInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "AttributeNumber5", scope = ShipmentLineInterface.class)
    public JAXBElement<BigDecimal> createShipmentLineInterfaceAttributeNumber5(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_InvSerialsInterfaceAttributeNumber5_QNAME, BigDecimal.class, ShipmentLineInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "AttributeNumber8", scope = ShipmentLineInterface.class)
    public JAXBElement<BigDecimal> createShipmentLineInterfaceAttributeNumber8(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_InvSerialsInterfaceAttributeNumber8_QNAME, BigDecimal.class, ShipmentLineInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "WeightUOMName", scope = ShipmentLineInterface.class)
    public JAXBElement<String> createShipmentLineInterfaceWeightUOMName(String value) {
        return new JAXBElement<String>(_ShipmentLineInterfaceWeightUOMName_QNAME, String.class, ShipmentLineInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "AttributeNumber7", scope = ShipmentLineInterface.class)
    public JAXBElement<BigDecimal> createShipmentLineInterfaceAttributeNumber7(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_InvSerialsInterfaceAttributeNumber7_QNAME, BigDecimal.class, ShipmentLineInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "PackingInstructions", scope = ShipmentLineInterface.class)
    public JAXBElement<String> createShipmentLineInterfacePackingInstructions(String value) {
        return new JAXBElement<String>(_ShipmentLineInterfacePackingInstructions_QNAME, String.class, ShipmentLineInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "Attribute14", scope = ShipmentLineInterface.class)
    public JAXBElement<String> createShipmentLineInterfaceAttribute14(String value) {
        return new JAXBElement<String>(_InvSerialsInterfaceAttribute14_QNAME, String.class, ShipmentLineInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "TransactionBusinessCategory", scope = ShipmentLineInterface.class)
    public JAXBElement<String> createShipmentLineInterfaceTransactionBusinessCategory(String value) {
        return new JAXBElement<String>(_ShipmentLineInterfaceTransactionBusinessCategory_QNAME, String.class, ShipmentLineInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "Attribute13", scope = ShipmentLineInterface.class)
    public JAXBElement<String> createShipmentLineInterfaceAttribute13(String value) {
        return new JAXBElement<String>(_InvSerialsInterfaceAttribute13_QNAME, String.class, ShipmentLineInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "AttributeNumber9", scope = ShipmentLineInterface.class)
    public JAXBElement<BigDecimal> createShipmentLineInterfaceAttributeNumber9(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_InvSerialsInterfaceAttributeNumber9_QNAME, BigDecimal.class, ShipmentLineInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "NetWeight", scope = ShipmentLineInterface.class)
    public JAXBElement<BigDecimal> createShipmentLineInterfaceNetWeight(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_ShipmentLineInterfaceNetWeight_QNAME, BigDecimal.class, ShipmentLineInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "ExemptCertificateNumber", scope = ShipmentLineInterface.class)
    public JAXBElement<String> createShipmentLineInterfaceExemptCertificateNumber(String value) {
        return new JAXBElement<String>(_ShipmentLineInterfaceExemptCertificateNumber_QNAME, String.class, ShipmentLineInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "Attribute12", scope = ShipmentLineInterface.class)
    public JAXBElement<String> createShipmentLineInterfaceAttribute12(String value) {
        return new JAXBElement<String>(_InvSerialsInterfaceAttribute12_QNAME, String.class, ShipmentLineInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "Attribute11", scope = ShipmentLineInterface.class)
    public JAXBElement<String> createShipmentLineInterfaceAttribute11(String value) {
        return new JAXBElement<String>(_InvSerialsInterfaceAttribute11_QNAME, String.class, ShipmentLineInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "DeliveryDetailId", scope = ShipmentLineInterface.class)
    public JAXBElement<Long> createShipmentLineInterfaceDeliveryDetailId(Long value) {
        return new JAXBElement<Long>(_ShipmentLineInterfaceDeliveryDetailId_QNAME, Long.class, ShipmentLineInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MeasureType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "ShippedQuantity", scope = ShipmentLineInterface.class)
    public JAXBElement<MeasureType> createShipmentLineInterfaceShippedQuantity(MeasureType value) {
        return new JAXBElement<MeasureType>(_ShipmentLineInterfaceShippedQuantity_QNAME, MeasureType.class, ShipmentLineInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "IntendedUse", scope = ShipmentLineInterface.class)
    public JAXBElement<String> createShipmentLineInterfaceIntendedUse(String value) {
        return new JAXBElement<String>(_ShipmentLineInterfaceIntendedUse_QNAME, String.class, ShipmentLineInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "Attribute18", scope = ShipmentLineInterface.class)
    public JAXBElement<String> createShipmentLineInterfaceAttribute18(String value) {
        return new JAXBElement<String>(_InvSerialsInterfaceAttribute18_QNAME, String.class, ShipmentLineInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "ItemNumber", scope = ShipmentLineInterface.class)
    public JAXBElement<String> createShipmentLineInterfaceItemNumber(String value) {
        return new JAXBElement<String>(_ShipmentLineInterfaceItemNumber_QNAME, String.class, ShipmentLineInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "Attribute17", scope = ShipmentLineInterface.class)
    public JAXBElement<String> createShipmentLineInterfaceAttribute17(String value) {
        return new JAXBElement<String>(_InvSerialsInterfaceAttribute17_QNAME, String.class, ShipmentLineInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "Attribute16", scope = ShipmentLineInterface.class)
    public JAXBElement<String> createShipmentLineInterfaceAttribute16(String value) {
        return new JAXBElement<String>(_InvSerialsInterfaceAttribute16_QNAME, String.class, ShipmentLineInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "Attribute15", scope = ShipmentLineInterface.class)
    public JAXBElement<String> createShipmentLineInterfaceAttribute15(String value) {
        return new JAXBElement<String>(_InvSerialsInterfaceAttribute15_QNAME, String.class, ShipmentLineInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "Attribute19", scope = ShipmentLineInterface.class)
    public JAXBElement<String> createShipmentLineInterfaceAttribute19(String value) {
        return new JAXBElement<String>(_InvSerialsInterfaceAttribute19_QNAME, String.class, ShipmentLineInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "Attribute1", scope = ShipmentLineInterface.class)
    public JAXBElement<String> createShipmentLineInterfaceAttribute1(String value) {
        return new JAXBElement<String>(_InvSerialsInterfaceAttribute1_QNAME, String.class, ShipmentLineInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "Attribute20", scope = ShipmentLineInterface.class)
    public JAXBElement<String> createShipmentLineInterfaceAttribute20(String value) {
        return new JAXBElement<String>(_InvSerialsInterfaceAttribute20_QNAME, String.class, ShipmentLineInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "AttributeCategory", scope = ShipmentLineInterface.class)
    public JAXBElement<String> createShipmentLineInterfaceAttributeCategory(String value) {
        return new JAXBElement<String>(_InvSerialsInterfaceAttributeCategory_QNAME, String.class, ShipmentLineInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "Attribute3", scope = ShipmentLineInterface.class)
    public JAXBElement<String> createShipmentLineInterfaceAttribute3(String value) {
        return new JAXBElement<String>(_InvSerialsInterfaceAttribute3_QNAME, String.class, ShipmentLineInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "Attribute2", scope = ShipmentLineInterface.class)
    public JAXBElement<String> createShipmentLineInterfaceAttribute2(String value) {
        return new JAXBElement<String>(_InvSerialsInterfaceAttribute2_QNAME, String.class, ShipmentLineInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "TaxInvoiceDate", scope = ShipmentLineInterface.class)
    public JAXBElement<XMLGregorianCalendar> createShipmentLineInterfaceTaxInvoiceDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_ShipmentLineInterfaceTaxInvoiceDate_QNAME, XMLGregorianCalendar.class, ShipmentLineInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "Attribute9", scope = ShipmentLineInterface.class)
    public JAXBElement<String> createShipmentLineInterfaceAttribute9(String value) {
        return new JAXBElement<String>(_InvSerialsInterfaceAttribute9_QNAME, String.class, ShipmentLineInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "Attribute8", scope = ShipmentLineInterface.class)
    public JAXBElement<String> createShipmentLineInterfaceAttribute8(String value) {
        return new JAXBElement<String>(_InvSerialsInterfaceAttribute8_QNAME, String.class, ShipmentLineInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MeasureType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "SecondaryShippedQuantity", scope = ShipmentLineInterface.class)
    public JAXBElement<MeasureType> createShipmentLineInterfaceSecondaryShippedQuantity(MeasureType value) {
        return new JAXBElement<MeasureType>(_ShipmentLineInterfaceSecondaryShippedQuantity_QNAME, MeasureType.class, ShipmentLineInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "Attribute5", scope = ShipmentLineInterface.class)
    public JAXBElement<String> createShipmentLineInterfaceAttribute5(String value) {
        return new JAXBElement<String>(_InvSerialsInterfaceAttribute5_QNAME, String.class, ShipmentLineInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "Attribute4", scope = ShipmentLineInterface.class)
    public JAXBElement<String> createShipmentLineInterfaceAttribute4(String value) {
        return new JAXBElement<String>(_InvSerialsInterfaceAttribute4_QNAME, String.class, ShipmentLineInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "Attribute7", scope = ShipmentLineInterface.class)
    public JAXBElement<String> createShipmentLineInterfaceAttribute7(String value) {
        return new JAXBElement<String>(_InvSerialsInterfaceAttribute7_QNAME, String.class, ShipmentLineInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "Attribute6", scope = ShipmentLineInterface.class)
    public JAXBElement<String> createShipmentLineInterfaceAttribute6(String value) {
        return new JAXBElement<String>(_InvSerialsInterfaceAttribute6_QNAME, String.class, ShipmentLineInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "AssessableValue", scope = ShipmentLineInterface.class)
    public JAXBElement<BigDecimal> createShipmentLineInterfaceAssessableValue(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_ShipmentLineInterfaceAssessableValue_QNAME, BigDecimal.class, ShipmentLineInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "ProductCategory", scope = ShipmentLineInterface.class)
    public JAXBElement<String> createShipmentLineInterfaceProductCategory(String value) {
        return new JAXBElement<String>(_ShipmentLineInterfaceProductCategory_QNAME, String.class, ShipmentLineInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "TaxClassificationCode", scope = ShipmentLineInterface.class)
    public JAXBElement<String> createShipmentLineInterfaceTaxClassificationCode(String value) {
        return new JAXBElement<String>(_ShipmentLineInterfaceTaxClassificationCode_QNAME, String.class, ShipmentLineInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "DefaultTaxationCountry", scope = ShipmentLineInterface.class)
    public JAXBElement<String> createShipmentLineInterfaceDefaultTaxationCountry(String value) {
        return new JAXBElement<String>(_ShipmentLineInterfaceDefaultTaxationCountry_QNAME, String.class, ShipmentLineInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "UserDefinedFiscalClassification", scope = ShipmentLineInterface.class)
    public JAXBElement<String> createShipmentLineInterfaceUserDefinedFiscalClassification(String value) {
        return new JAXBElement<String>(_ShipmentLineInterfaceUserDefinedFiscalClassification_QNAME, String.class, ShipmentLineInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "Revision", scope = ShipmentLineInterface.class)
    public JAXBElement<String> createShipmentLineInterfaceRevision(String value) {
        return new JAXBElement<String>(_ShipmentLineInterfaceRevision_QNAME, String.class, ShipmentLineInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "Volume", scope = ShipmentLineInterface.class)
    public JAXBElement<BigDecimal> createShipmentLineInterfaceVolume(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_ShipmentLineInterfaceVolume_QNAME, BigDecimal.class, ShipmentLineInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "ProductType", scope = ShipmentLineInterface.class)
    public JAXBElement<String> createShipmentLineInterfaceProductType(String value) {
        return new JAXBElement<String>(_ShipmentLineInterfaceProductType_QNAME, String.class, ShipmentLineInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "FirstPartyTaxRegistrationNumber", scope = ShipmentLineInterface.class)
    public JAXBElement<String> createShipmentLineInterfaceFirstPartyTaxRegistrationNumber(String value) {
        return new JAXBElement<String>(_ShipmentLineInterfaceFirstPartyTaxRegistrationNumber_QNAME, String.class, ShipmentLineInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "TaxInvoiceNumber", scope = ShipmentLineInterface.class)
    public JAXBElement<String> createShipmentLineInterfaceTaxInvoiceNumber(String value) {
        return new JAXBElement<String>(_ShipmentLineInterfaceTaxInvoiceNumber_QNAME, String.class, ShipmentLineInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "LocationOfFinalDischarge", scope = ShipmentLineInterface.class)
    public JAXBElement<String> createShipmentLineInterfaceLocationOfFinalDischarge(String value) {
        return new JAXBElement<String>(_ShipmentLineInterfaceLocationOfFinalDischarge_QNAME, String.class, ShipmentLineInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "DocumentSubType", scope = ShipmentLineInterface.class)
    public JAXBElement<String> createShipmentLineInterfaceDocumentSubType(String value) {
        return new JAXBElement<String>(_ShipmentLineInterfaceDocumentSubType_QNAME, String.class, ShipmentLineInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "GrossWeight", scope = ShipmentLineInterface.class)
    public JAXBElement<BigDecimal> createShipmentLineInterfaceGrossWeight(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_ShipmentLineInterfaceGrossWeight_QNAME, BigDecimal.class, ShipmentLineInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "ExemptReasonCode", scope = ShipmentLineInterface.class)
    public JAXBElement<String> createShipmentLineInterfaceExemptReasonCode(String value) {
        return new JAXBElement<String>(_ShipmentLineInterfaceExemptReasonCode_QNAME, String.class, ShipmentLineInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "AttributeNumber10", scope = ShipmentLineInterface.class)
    public JAXBElement<BigDecimal> createShipmentLineInterfaceAttributeNumber10(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_InvSerialsInterfaceAttributeNumber10_QNAME, BigDecimal.class, ShipmentLineInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "Locator", scope = ShipmentLineInterface.class)
    public JAXBElement<String> createShipmentLineInterfaceLocator(String value) {
        return new JAXBElement<String>(_InvSerialsInterfaceLocator_QNAME, String.class, ShipmentLineInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "ThirdPartyTaxRegistrationNumber", scope = ShipmentLineInterface.class)
    public JAXBElement<String> createShipmentLineInterfaceThirdPartyTaxRegistrationNumber(String value) {
        return new JAXBElement<String>(_ShipmentLineInterfaceThirdPartyTaxRegistrationNumber_QNAME, String.class, ShipmentLineInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "VolumeUOMName", scope = ShipmentLineInterface.class)
    public JAXBElement<String> createShipmentLineInterfaceVolumeUOMName(String value) {
        return new JAXBElement<String>(_ShipmentLineInterfaceVolumeUOMName_QNAME, String.class, ShipmentLineInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "ShippingInstructions", scope = ShipmentLineInterface.class)
    public JAXBElement<String> createShipmentLineInterfaceShippingInstructions(String value) {
        return new JAXBElement<String>(_ShipmentLineInterfaceShippingInstructions_QNAME, String.class, ShipmentLineInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "AttributeTimestamp1", scope = ShipmentLineInterface.class)
    public JAXBElement<XMLGregorianCalendar> createShipmentLineInterfaceAttributeTimestamp1(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_InvSerialsInterfaceAttributeTimestamp1_QNAME, XMLGregorianCalendar.class, ShipmentLineInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "AttributeTimestamp5", scope = ShipmentLineInterface.class)
    public JAXBElement<XMLGregorianCalendar> createShipmentLineInterfaceAttributeTimestamp5(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_InvSerialsInterfaceAttributeTimestamp5_QNAME, XMLGregorianCalendar.class, ShipmentLineInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "AttributeTimestamp4", scope = ShipmentLineInterface.class)
    public JAXBElement<XMLGregorianCalendar> createShipmentLineInterfaceAttributeTimestamp4(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_InvSerialsInterfaceAttributeTimestamp4_QNAME, XMLGregorianCalendar.class, ShipmentLineInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "AttributeTimestamp3", scope = ShipmentLineInterface.class)
    public JAXBElement<XMLGregorianCalendar> createShipmentLineInterfaceAttributeTimestamp3(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_InvSerialsInterfaceAttributeTimestamp3_QNAME, XMLGregorianCalendar.class, ShipmentLineInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "AttributeTimestamp2", scope = ShipmentLineInterface.class)
    public JAXBElement<XMLGregorianCalendar> createShipmentLineInterfaceAttributeTimestamp2(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_InvSerialsInterfaceAttributeTimestamp2_QNAME, XMLGregorianCalendar.class, ShipmentLineInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/types/", name = "ToActualShipDate", scope = FetchShipmentDocumentsAsync.class)
    public JAXBElement<XMLGregorianCalendar> createFetchShipmentDocumentsAsyncToActualShipDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_FetchShipmentDocumentsToActualShipDate_QNAME, XMLGregorianCalendar.class, FetchShipmentDocumentsAsync.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/types/", name = "ToCreationDate", scope = FetchShipmentDocumentsAsync.class)
    public JAXBElement<XMLGregorianCalendar> createFetchShipmentDocumentsAsyncToCreationDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_FetchShipmentDocumentsToCreationDate_QNAME, XMLGregorianCalendar.class, FetchShipmentDocumentsAsync.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/types/", name = "FromActualShipDate", scope = FetchShipmentDocumentsAsync.class)
    public JAXBElement<XMLGregorianCalendar> createFetchShipmentDocumentsAsyncFromActualShipDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_FetchShipmentDocumentsFromActualShipDate_QNAME, XMLGregorianCalendar.class, FetchShipmentDocumentsAsync.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/types/", name = "Shipment", scope = FetchShipmentDocumentsAsync.class)
    public JAXBElement<String> createFetchShipmentDocumentsAsyncShipment(String value) {
        return new JAXBElement<String>(_FetchShipmentDocumentsShipment_QNAME, String.class, FetchShipmentDocumentsAsync.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/types/", name = "ToConfirmedDate", scope = FetchShipmentDocumentsAsync.class)
    public JAXBElement<XMLGregorianCalendar> createFetchShipmentDocumentsAsyncToConfirmedDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_FetchShipmentDocumentsToConfirmedDate_QNAME, XMLGregorianCalendar.class, FetchShipmentDocumentsAsync.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/types/", name = "FromCreationDate", scope = FetchShipmentDocumentsAsync.class)
    public JAXBElement<XMLGregorianCalendar> createFetchShipmentDocumentsAsyncFromCreationDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_FetchShipmentDocumentsFromCreationDate_QNAME, XMLGregorianCalendar.class, FetchShipmentDocumentsAsync.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/types/", name = "FromConfirmedDate", scope = FetchShipmentDocumentsAsync.class)
    public JAXBElement<XMLGregorianCalendar> createFetchShipmentDocumentsAsyncFromConfirmedDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_FetchShipmentDocumentsFromConfirmedDate_QNAME, XMLGregorianCalendar.class, FetchShipmentDocumentsAsync.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "FreightTerms", scope = ShipmentInformation.class)
    public JAXBElement<String> createShipmentInformationFreightTerms(String value) {
        return new JAXBElement<String>(_ShipmentInformationFreightTerms_QNAME, String.class, ShipmentInformation.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AmountType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "CODAmount", scope = ShipmentInformation.class)
    public JAXBElement<AmountType> createShipmentInformationCODAmount(AmountType value) {
        return new JAXBElement<AmountType>(_ShipmentInformationCODAmount_QNAME, AmountType.class, ShipmentInformation.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "AutomaticallyPack", scope = ShipmentInformation.class)
    public JAXBElement<String> createShipmentInformationAutomaticallyPack(String value) {
        return new JAXBElement<String>(_ShipmentInformationAutomaticallyPack_QNAME, String.class, ShipmentInformation.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "Dock", scope = ShipmentInformation.class)
    public JAXBElement<String> createShipmentInformationDock(String value) {
        return new JAXBElement<String>(_ShipmentInformationDock_QNAME, String.class, ShipmentInformation.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "Attribute10", scope = ShipmentInformation.class)
    public JAXBElement<String> createShipmentInformationAttribute10(String value) {
        return new JAXBElement<String>(_InvSerialsInterfaceAttribute10_QNAME, String.class, ShipmentInformation.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "GlobalAttributeCategory", scope = ShipmentInformation.class)
    public JAXBElement<String> createShipmentInformationGlobalAttributeCategory(String value) {
        return new JAXBElement<String>(_ShipmentInformationGlobalAttributeCategory_QNAME, String.class, ShipmentInformation.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "Attribute14", scope = ShipmentInformation.class)
    public JAXBElement<String> createShipmentInformationAttribute14(String value) {
        return new JAXBElement<String>(_InvSerialsInterfaceAttribute14_QNAME, String.class, ShipmentInformation.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "Attribute13", scope = ShipmentInformation.class)
    public JAXBElement<String> createShipmentInformationAttribute13(String value) {
        return new JAXBElement<String>(_InvSerialsInterfaceAttribute13_QNAME, String.class, ShipmentInformation.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "Planned", scope = ShipmentInformation.class)
    public JAXBElement<String> createShipmentInformationPlanned(String value) {
        return new JAXBElement<String>(_ShipmentInformationPlanned_QNAME, String.class, ShipmentInformation.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "NetWeight", scope = ShipmentInformation.class)
    public JAXBElement<BigDecimal> createShipmentInformationNetWeight(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_ShipmentLineInterfaceNetWeight_QNAME, BigDecimal.class, ShipmentInformation.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "Attribute12", scope = ShipmentInformation.class)
    public JAXBElement<String> createShipmentInformationAttribute12(String value) {
        return new JAXBElement<String>(_InvSerialsInterfaceAttribute12_QNAME, String.class, ShipmentInformation.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "Attribute11", scope = ShipmentInformation.class)
    public JAXBElement<String> createShipmentInformationAttribute11(String value) {
        return new JAXBElement<String>(_InvSerialsInterfaceAttribute11_QNAME, String.class, ShipmentInformation.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "WeightUOM", scope = ShipmentInformation.class)
    public JAXBElement<String> createShipmentInformationWeightUOM(String value) {
        return new JAXBElement<String>(_ShipmentInformationWeightUOM_QNAME, String.class, ShipmentInformation.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "GlobalAttribute9", scope = ShipmentInformation.class)
    public JAXBElement<String> createShipmentInformationGlobalAttribute9(String value) {
        return new JAXBElement<String>(_ShipmentInformationGlobalAttribute9_QNAME, String.class, ShipmentInformation.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "CODRemitTo", scope = ShipmentInformation.class)
    public JAXBElement<String> createShipmentInformationCODRemitTo(String value) {
        return new JAXBElement<String>(_ShipmentInformationCODRemitTo_QNAME, String.class, ShipmentInformation.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "Attribute18", scope = ShipmentInformation.class)
    public JAXBElement<String> createShipmentInformationAttribute18(String value) {
        return new JAXBElement<String>(_InvSerialsInterfaceAttribute18_QNAME, String.class, ShipmentInformation.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "Attribute17", scope = ShipmentInformation.class)
    public JAXBElement<String> createShipmentInformationAttribute17(String value) {
        return new JAXBElement<String>(_InvSerialsInterfaceAttribute17_QNAME, String.class, ShipmentInformation.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "Attribute16", scope = ShipmentInformation.class)
    public JAXBElement<String> createShipmentInformationAttribute16(String value) {
        return new JAXBElement<String>(_InvSerialsInterfaceAttribute16_QNAME, String.class, ShipmentInformation.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "CODCurrency", scope = ShipmentInformation.class)
    public JAXBElement<String> createShipmentInformationCODCurrency(String value) {
        return new JAXBElement<String>(_ShipmentInformationCODCurrency_QNAME, String.class, ShipmentInformation.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "Attribute15", scope = ShipmentInformation.class)
    public JAXBElement<String> createShipmentInformationAttribute15(String value) {
        return new JAXBElement<String>(_InvSerialsInterfaceAttribute15_QNAME, String.class, ShipmentInformation.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "Attribute19", scope = ShipmentInformation.class)
    public JAXBElement<String> createShipmentInformationAttribute19(String value) {
        return new JAXBElement<String>(_InvSerialsInterfaceAttribute19_QNAME, String.class, ShipmentInformation.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "Attribute20", scope = ShipmentInformation.class)
    public JAXBElement<String> createShipmentInformationAttribute20(String value) {
        return new JAXBElement<String>(_InvSerialsInterfaceAttribute20_QNAME, String.class, ShipmentInformation.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "AttributeCategory", scope = ShipmentInformation.class)
    public JAXBElement<String> createShipmentInformationAttributeCategory(String value) {
        return new JAXBElement<String>(_InvSerialsInterfaceAttributeCategory_QNAME, String.class, ShipmentInformation.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "Description", scope = ShipmentInformation.class)
    public JAXBElement<String> createShipmentInformationDescription(String value) {
        return new JAXBElement<String>(_ShipmentInformationDescription_QNAME, String.class, ShipmentInformation.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "GlobalAttribute18", scope = ShipmentInformation.class)
    public JAXBElement<String> createShipmentInformationGlobalAttribute18(String value) {
        return new JAXBElement<String>(_ShipmentInformationGlobalAttribute18_QNAME, String.class, ShipmentInformation.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "GlobalAttribute19", scope = ShipmentInformation.class)
    public JAXBElement<String> createShipmentInformationGlobalAttribute19(String value) {
        return new JAXBElement<String>(_ShipmentInformationGlobalAttribute19_QNAME, String.class, ShipmentInformation.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "ModeOfTransport", scope = ShipmentInformation.class)
    public JAXBElement<String> createShipmentInformationModeOfTransport(String value) {
        return new JAXBElement<String>(_ShipmentInformationModeOfTransport_QNAME, String.class, ShipmentInformation.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "Volume", scope = ShipmentInformation.class)
    public JAXBElement<BigDecimal> createShipmentInformationVolume(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_ShipmentLineInterfaceVolume_QNAME, BigDecimal.class, ShipmentInformation.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "ProrateWeight", scope = ShipmentInformation.class)
    public JAXBElement<String> createShipmentInformationProrateWeight(String value) {
        return new JAXBElement<String>(_ShipmentInformationProrateWeight_QNAME, String.class, ShipmentInformation.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "GlobalAttribute20", scope = ShipmentInformation.class)
    public JAXBElement<String> createShipmentInformationGlobalAttribute20(String value) {
        return new JAXBElement<String>(_ShipmentInformationGlobalAttribute20_QNAME, String.class, ShipmentInformation.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "CODPaidBy", scope = ShipmentInformation.class)
    public JAXBElement<String> createShipmentInformationCODPaidBy(String value) {
        return new JAXBElement<String>(_ShipmentInformationCODPaidBy_QNAME, String.class, ShipmentInformation.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "GrossWeight", scope = ShipmentInformation.class)
    public JAXBElement<BigDecimal> createShipmentInformationGrossWeight(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_ShipmentLineInterfaceGrossWeight_QNAME, BigDecimal.class, ShipmentInformation.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "GlobalAttribute1", scope = ShipmentInformation.class)
    public JAXBElement<String> createShipmentInformationGlobalAttribute1(String value) {
        return new JAXBElement<String>(_ShipmentInformationGlobalAttribute1_QNAME, String.class, ShipmentInformation.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "GlobalAttribute2", scope = ShipmentInformation.class)
    public JAXBElement<String> createShipmentInformationGlobalAttribute2(String value) {
        return new JAXBElement<String>(_ShipmentInformationGlobalAttribute2_QNAME, String.class, ShipmentInformation.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "GlobalAttribute3", scope = ShipmentInformation.class)
    public JAXBElement<String> createShipmentInformationGlobalAttribute3(String value) {
        return new JAXBElement<String>(_ShipmentInformationGlobalAttribute3_QNAME, String.class, ShipmentInformation.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "GlobalAttribute4", scope = ShipmentInformation.class)
    public JAXBElement<String> createShipmentInformationGlobalAttribute4(String value) {
        return new JAXBElement<String>(_ShipmentInformationGlobalAttribute4_QNAME, String.class, ShipmentInformation.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "GlobalAttribute5", scope = ShipmentInformation.class)
    public JAXBElement<String> createShipmentInformationGlobalAttribute5(String value) {
        return new JAXBElement<String>(_ShipmentInformationGlobalAttribute5_QNAME, String.class, ShipmentInformation.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "GlobalAttribute6", scope = ShipmentInformation.class)
    public JAXBElement<String> createShipmentInformationGlobalAttribute6(String value) {
        return new JAXBElement<String>(_ShipmentInformationGlobalAttribute6_QNAME, String.class, ShipmentInformation.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "GlobalAttribute7", scope = ShipmentInformation.class)
    public JAXBElement<String> createShipmentInformationGlobalAttribute7(String value) {
        return new JAXBElement<String>(_ShipmentInformationGlobalAttribute7_QNAME, String.class, ShipmentInformation.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "GlobalAttribute8", scope = ShipmentInformation.class)
    public JAXBElement<String> createShipmentInformationGlobalAttribute8(String value) {
        return new JAXBElement<String>(_ShipmentInformationGlobalAttribute8_QNAME, String.class, ShipmentInformation.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "GlobalAttributeDate2", scope = ShipmentInformation.class)
    public JAXBElement<XMLGregorianCalendar> createShipmentInformationGlobalAttributeDate2(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_ShipmentInformationGlobalAttributeDate2_QNAME, XMLGregorianCalendar.class, ShipmentInformation.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "GlobalAttributeDate1", scope = ShipmentInformation.class)
    public JAXBElement<XMLGregorianCalendar> createShipmentInformationGlobalAttributeDate1(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_ShipmentInformationGlobalAttributeDate1_QNAME, XMLGregorianCalendar.class, ShipmentInformation.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "GlobalAttributeDate4", scope = ShipmentInformation.class)
    public JAXBElement<XMLGregorianCalendar> createShipmentInformationGlobalAttributeDate4(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_ShipmentInformationGlobalAttributeDate4_QNAME, XMLGregorianCalendar.class, ShipmentInformation.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "GlobalAttributeDate3", scope = ShipmentInformation.class)
    public JAXBElement<XMLGregorianCalendar> createShipmentInformationGlobalAttributeDate3(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_ShipmentInformationGlobalAttributeDate3_QNAME, XMLGregorianCalendar.class, ShipmentInformation.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "GlobalAttributeDate5", scope = ShipmentInformation.class)
    public JAXBElement<XMLGregorianCalendar> createShipmentInformationGlobalAttributeDate5(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_ShipmentInformationGlobalAttributeDate5_QNAME, XMLGregorianCalendar.class, ShipmentInformation.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "ShipFromOrganization", scope = ShipmentInformation.class)
    public JAXBElement<String> createShipmentInformationShipFromOrganization(String value) {
        return new JAXBElement<String>(_ShipmentInformationShipFromOrganization_QNAME, String.class, ShipmentInformation.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "AttributeTimestamp1", scope = ShipmentInformation.class)
    public JAXBElement<XMLGregorianCalendar> createShipmentInformationAttributeTimestamp1(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_InvSerialsInterfaceAttributeTimestamp1_QNAME, XMLGregorianCalendar.class, ShipmentInformation.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "AttributeTimestamp5", scope = ShipmentInformation.class)
    public JAXBElement<XMLGregorianCalendar> createShipmentInformationAttributeTimestamp5(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_InvSerialsInterfaceAttributeTimestamp5_QNAME, XMLGregorianCalendar.class, ShipmentInformation.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "AttributeTimestamp4", scope = ShipmentInformation.class)
    public JAXBElement<XMLGregorianCalendar> createShipmentInformationAttributeTimestamp4(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_InvSerialsInterfaceAttributeTimestamp4_QNAME, XMLGregorianCalendar.class, ShipmentInformation.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "AttributeTimestamp3", scope = ShipmentInformation.class)
    public JAXBElement<XMLGregorianCalendar> createShipmentInformationAttributeTimestamp3(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_InvSerialsInterfaceAttributeTimestamp3_QNAME, XMLGregorianCalendar.class, ShipmentInformation.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "AttributeTimestamp2", scope = ShipmentInformation.class)
    public JAXBElement<XMLGregorianCalendar> createShipmentInformationAttributeTimestamp2(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_InvSerialsInterfaceAttributeTimestamp2_QNAME, XMLGregorianCalendar.class, ShipmentInformation.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "AttributeDate1", scope = ShipmentInformation.class)
    public JAXBElement<XMLGregorianCalendar> createShipmentInformationAttributeDate1(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_InvSerialsInterfaceAttributeDate1_QNAME, XMLGregorianCalendar.class, ShipmentInformation.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "AttributeDate5", scope = ShipmentInformation.class)
    public JAXBElement<XMLGregorianCalendar> createShipmentInformationAttributeDate5(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_InvSerialsInterfaceAttributeDate5_QNAME, XMLGregorianCalendar.class, ShipmentInformation.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "AttributeDate4", scope = ShipmentInformation.class)
    public JAXBElement<XMLGregorianCalendar> createShipmentInformationAttributeDate4(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_InvSerialsInterfaceAttributeDate4_QNAME, XMLGregorianCalendar.class, ShipmentInformation.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "ShipToPartySiteNumber", scope = ShipmentInformation.class)
    public JAXBElement<String> createShipmentInformationShipToPartySiteNumber(String value) {
        return new JAXBElement<String>(_ShipmentInformationShipToPartySiteNumber_QNAME, String.class, ShipmentInformation.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "AttributeDate3", scope = ShipmentInformation.class)
    public JAXBElement<XMLGregorianCalendar> createShipmentInformationAttributeDate3(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_InvSerialsInterfaceAttributeDate3_QNAME, XMLGregorianCalendar.class, ShipmentInformation.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "TransportationReason", scope = ShipmentInformation.class)
    public JAXBElement<String> createShipmentInformationTransportationReason(String value) {
        return new JAXBElement<String>(_ShipmentInformationTransportationReason_QNAME, String.class, ShipmentInformation.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "FOBSiteNumber", scope = ShipmentInformation.class)
    public JAXBElement<String> createShipmentInformationFOBSiteNumber(String value) {
        return new JAXBElement<String>(_ShipmentInformationFOBSiteNumber_QNAME, String.class, ShipmentInformation.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "AttributeDate2", scope = ShipmentInformation.class)
    public JAXBElement<XMLGregorianCalendar> createShipmentInformationAttributeDate2(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_InvSerialsInterfaceAttributeDate2_QNAME, XMLGregorianCalendar.class, ShipmentInformation.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "AttributeNumber2", scope = ShipmentInformation.class)
    public JAXBElement<BigDecimal> createShipmentInformationAttributeNumber2(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_InvSerialsInterfaceAttributeNumber2_QNAME, BigDecimal.class, ShipmentInformation.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "AttributeNumber1", scope = ShipmentInformation.class)
    public JAXBElement<BigDecimal> createShipmentInformationAttributeNumber1(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_InvSerialsInterfaceAttributeNumber1_QNAME, BigDecimal.class, ShipmentInformation.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "Equipment", scope = ShipmentInformation.class)
    public JAXBElement<String> createShipmentInformationEquipment(String value) {
        return new JAXBElement<String>(_ShipmentInformationEquipment_QNAME, String.class, ShipmentInformation.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "AttributeNumber4", scope = ShipmentInformation.class)
    public JAXBElement<BigDecimal> createShipmentInformationAttributeNumber4(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_InvSerialsInterfaceAttributeNumber4_QNAME, BigDecimal.class, ShipmentInformation.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "AttributeNumber3", scope = ShipmentInformation.class)
    public JAXBElement<BigDecimal> createShipmentInformationAttributeNumber3(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_InvSerialsInterfaceAttributeNumber3_QNAME, BigDecimal.class, ShipmentInformation.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "AttributeNumber6", scope = ShipmentInformation.class)
    public JAXBElement<BigDecimal> createShipmentInformationAttributeNumber6(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_InvSerialsInterfaceAttributeNumber6_QNAME, BigDecimal.class, ShipmentInformation.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "AttributeNumber5", scope = ShipmentInformation.class)
    public JAXBElement<BigDecimal> createShipmentInformationAttributeNumber5(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_InvSerialsInterfaceAttributeNumber5_QNAME, BigDecimal.class, ShipmentInformation.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "AttributeNumber8", scope = ShipmentInformation.class)
    public JAXBElement<BigDecimal> createShipmentInformationAttributeNumber8(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_InvSerialsInterfaceAttributeNumber8_QNAME, BigDecimal.class, ShipmentInformation.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "AttributeNumber7", scope = ShipmentInformation.class)
    public JAXBElement<BigDecimal> createShipmentInformationAttributeNumber7(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_InvSerialsInterfaceAttributeNumber7_QNAME, BigDecimal.class, ShipmentInformation.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "RoutingInstructions", scope = ShipmentInformation.class)
    public JAXBElement<String> createShipmentInformationRoutingInstructions(String value) {
        return new JAXBElement<String>(_ShipmentInformationRoutingInstructions_QNAME, String.class, ShipmentInformation.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "VolumeUOM", scope = ShipmentInformation.class)
    public JAXBElement<String> createShipmentInformationVolumeUOM(String value) {
        return new JAXBElement<String>(_ShipmentInformationVolumeUOM_QNAME, String.class, ShipmentInformation.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "AttributeNumber9", scope = ShipmentInformation.class)
    public JAXBElement<BigDecimal> createShipmentInformationAttributeNumber9(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_InvSerialsInterfaceAttributeNumber9_QNAME, BigDecimal.class, ShipmentInformation.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "ShippingMarks", scope = ShipmentInformation.class)
    public JAXBElement<String> createShipmentInformationShippingMarks(String value) {
        return new JAXBElement<String>(_ShipmentInformationShippingMarks_QNAME, String.class, ShipmentInformation.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "InitialShipDate", scope = ShipmentInformation.class)
    public JAXBElement<XMLGregorianCalendar> createShipmentInformationInitialShipDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_ShipmentInformationInitialShipDate_QNAME, XMLGregorianCalendar.class, ShipmentInformation.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "Attribute1", scope = ShipmentInformation.class)
    public JAXBElement<String> createShipmentInformationAttribute1(String value) {
        return new JAXBElement<String>(_InvSerialsInterfaceAttribute1_QNAME, String.class, ShipmentInformation.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "Attribute3", scope = ShipmentInformation.class)
    public JAXBElement<String> createShipmentInformationAttribute3(String value) {
        return new JAXBElement<String>(_InvSerialsInterfaceAttribute3_QNAME, String.class, ShipmentInformation.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "Attribute2", scope = ShipmentInformation.class)
    public JAXBElement<String> createShipmentInformationAttribute2(String value) {
        return new JAXBElement<String>(_InvSerialsInterfaceAttribute2_QNAME, String.class, ShipmentInformation.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "Waybill", scope = ShipmentInformation.class)
    public JAXBElement<String> createShipmentInformationWaybill(String value) {
        return new JAXBElement<String>(_ShipmentInformationWaybill_QNAME, String.class, ShipmentInformation.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "ProblemContactReference", scope = ShipmentInformation.class)
    public JAXBElement<String> createShipmentInformationProblemContactReference(String value) {
        return new JAXBElement<String>(_ShipmentInformationProblemContactReference_QNAME, String.class, ShipmentInformation.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "Attribute9", scope = ShipmentInformation.class)
    public JAXBElement<String> createShipmentInformationAttribute9(String value) {
        return new JAXBElement<String>(_InvSerialsInterfaceAttribute9_QNAME, String.class, ShipmentInformation.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "GlobalAttribute12", scope = ShipmentInformation.class)
    public JAXBElement<String> createShipmentInformationGlobalAttribute12(String value) {
        return new JAXBElement<String>(_ShipmentInformationGlobalAttribute12_QNAME, String.class, ShipmentInformation.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "Attribute8", scope = ShipmentInformation.class)
    public JAXBElement<String> createShipmentInformationAttribute8(String value) {
        return new JAXBElement<String>(_InvSerialsInterfaceAttribute8_QNAME, String.class, ShipmentInformation.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "GlobalAttribute13", scope = ShipmentInformation.class)
    public JAXBElement<String> createShipmentInformationGlobalAttribute13(String value) {
        return new JAXBElement<String>(_ShipmentInformationGlobalAttribute13_QNAME, String.class, ShipmentInformation.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "ServiceLevel", scope = ShipmentInformation.class)
    public JAXBElement<String> createShipmentInformationServiceLevel(String value) {
        return new JAXBElement<String>(_ShipmentInformationServiceLevel_QNAME, String.class, ShipmentInformation.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "GlobalAttribute10", scope = ShipmentInformation.class)
    public JAXBElement<String> createShipmentInformationGlobalAttribute10(String value) {
        return new JAXBElement<String>(_ShipmentInformationGlobalAttribute10_QNAME, String.class, ShipmentInformation.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "GlobalAttribute11", scope = ShipmentInformation.class)
    public JAXBElement<String> createShipmentInformationGlobalAttribute11(String value) {
        return new JAXBElement<String>(_ShipmentInformationGlobalAttribute11_QNAME, String.class, ShipmentInformation.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "SealNumber", scope = ShipmentInformation.class)
    public JAXBElement<String> createShipmentInformationSealNumber(String value) {
        return new JAXBElement<String>(_ShipmentInformationSealNumber_QNAME, String.class, ShipmentInformation.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "Attribute5", scope = ShipmentInformation.class)
    public JAXBElement<String> createShipmentInformationAttribute5(String value) {
        return new JAXBElement<String>(_InvSerialsInterfaceAttribute5_QNAME, String.class, ShipmentInformation.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "GlobalAttribute16", scope = ShipmentInformation.class)
    public JAXBElement<String> createShipmentInformationGlobalAttribute16(String value) {
        return new JAXBElement<String>(_ShipmentInformationGlobalAttribute16_QNAME, String.class, ShipmentInformation.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "Attribute4", scope = ShipmentInformation.class)
    public JAXBElement<String> createShipmentInformationAttribute4(String value) {
        return new JAXBElement<String>(_InvSerialsInterfaceAttribute4_QNAME, String.class, ShipmentInformation.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "GlobalAttribute17", scope = ShipmentInformation.class)
    public JAXBElement<String> createShipmentInformationGlobalAttribute17(String value) {
        return new JAXBElement<String>(_ShipmentInformationGlobalAttribute17_QNAME, String.class, ShipmentInformation.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "Attribute7", scope = ShipmentInformation.class)
    public JAXBElement<String> createShipmentInformationAttribute7(String value) {
        return new JAXBElement<String>(_InvSerialsInterfaceAttribute7_QNAME, String.class, ShipmentInformation.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "GlobalAttribute14", scope = ShipmentInformation.class)
    public JAXBElement<String> createShipmentInformationGlobalAttribute14(String value) {
        return new JAXBElement<String>(_ShipmentInformationGlobalAttribute14_QNAME, String.class, ShipmentInformation.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "Attribute6", scope = ShipmentInformation.class)
    public JAXBElement<String> createShipmentInformationAttribute6(String value) {
        return new JAXBElement<String>(_InvSerialsInterfaceAttribute6_QNAME, String.class, ShipmentInformation.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "GlobalAttribute15", scope = ShipmentInformation.class)
    public JAXBElement<String> createShipmentInformationGlobalAttribute15(String value) {
        return new JAXBElement<String>(_ShipmentInformationGlobalAttribute15_QNAME, String.class, ShipmentInformation.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "EnableAutoship", scope = ShipmentInformation.class)
    public JAXBElement<String> createShipmentInformationEnableAutoship(String value) {
        return new JAXBElement<String>(_ShipmentInformationEnableAutoship_QNAME, String.class, ShipmentInformation.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "ConfirmedDate", scope = ShipmentInformation.class)
    public JAXBElement<XMLGregorianCalendar> createShipmentInformationConfirmedDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_ShipmentInformationConfirmedDate_QNAME, XMLGregorianCalendar.class, ShipmentInformation.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "GlobalAttributeNumber3", scope = ShipmentInformation.class)
    public JAXBElement<BigDecimal> createShipmentInformationGlobalAttributeNumber3(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_ShipmentInformationGlobalAttributeNumber3_QNAME, BigDecimal.class, ShipmentInformation.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "GlobalAttributeNumber2", scope = ShipmentInformation.class)
    public JAXBElement<BigDecimal> createShipmentInformationGlobalAttributeNumber2(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_ShipmentInformationGlobalAttributeNumber2_QNAME, BigDecimal.class, ShipmentInformation.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "GlobalAttributeNumber5", scope = ShipmentInformation.class)
    public JAXBElement<BigDecimal> createShipmentInformationGlobalAttributeNumber5(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_ShipmentInformationGlobalAttributeNumber5_QNAME, BigDecimal.class, ShipmentInformation.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "GlobalAttributeNumber4", scope = ShipmentInformation.class)
    public JAXBElement<BigDecimal> createShipmentInformationGlobalAttributeNumber4(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_ShipmentInformationGlobalAttributeNumber4_QNAME, BigDecimal.class, ShipmentInformation.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "PlannedDeliveryDate", scope = ShipmentInformation.class)
    public JAXBElement<XMLGregorianCalendar> createShipmentInformationPlannedDeliveryDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_ShipmentInformationPlannedDeliveryDate_QNAME, XMLGregorianCalendar.class, ShipmentInformation.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "Shipment", scope = ShipmentInformation.class)
    public JAXBElement<String> createShipmentInformationShipment(String value) {
        return new JAXBElement<String>(_ShipmentInformationShipment_QNAME, String.class, ShipmentInformation.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "GlobalAttributeNumber1", scope = ShipmentInformation.class)
    public JAXBElement<BigDecimal> createShipmentInformationGlobalAttributeNumber1(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_ShipmentInformationGlobalAttributeNumber1_QNAME, BigDecimal.class, ShipmentInformation.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "AttributeNumber10", scope = ShipmentInformation.class)
    public JAXBElement<BigDecimal> createShipmentInformationAttributeNumber10(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_InvSerialsInterfaceAttributeNumber10_QNAME, BigDecimal.class, ShipmentInformation.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "FOB", scope = ShipmentInformation.class)
    public JAXBElement<String> createShipmentInformationFOB(String value) {
        return new JAXBElement<String>(_ShipmentInformationFOB_QNAME, String.class, ShipmentInformation.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "CustomerNumber", scope = ShipmentInformation.class)
    public JAXBElement<String> createShipmentInformationCustomerNumber(String value) {
        return new JAXBElement<String>(_ShipmentInformationCustomerNumber_QNAME, String.class, ShipmentInformation.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "EquipmentType", scope = ShipmentInformation.class)
    public JAXBElement<String> createShipmentInformationEquipmentType(String value) {
        return new JAXBElement<String>(_ShipmentInformationEquipmentType_QNAME, String.class, ShipmentInformation.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "CarrierPartyNumber", scope = ShipmentInformation.class)
    public JAXBElement<String> createShipmentInformationCarrierPartyNumber(String value) {
        return new JAXBElement<String>(_ShipmentInformationCarrierPartyNumber_QNAME, String.class, ShipmentInformation.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "LoadingSequenceRule", scope = ShipmentInformation.class)
    public JAXBElement<String> createShipmentInformationLoadingSequenceRule(String value) {
        return new JAXBElement<String>(_ShipmentInformationLoadingSequenceRule_QNAME, String.class, ShipmentInformation.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MeasureType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "SecondaryTransactionQuantity", scope = InvLotInterface.class)
    public JAXBElement<MeasureType> createInvLotInterfaceSecondaryTransactionQuantity(MeasureType value) {
        return new JAXBElement<MeasureType>(_InvLotInterfaceSecondaryTransactionQuantity_QNAME, MeasureType.class, InvLotInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "SubinventoryCode", scope = InvLotInterface.class)
    public JAXBElement<String> createInvLotInterfaceSubinventoryCode(String value) {
        return new JAXBElement<String>(_InvSerialsInterfaceSubinventoryCode_QNAME, String.class, InvLotInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "Locator", scope = InvLotInterface.class)
    public JAXBElement<String> createInvLotInterfaceLocator(String value) {
        return new JAXBElement<String>(_InvSerialsInterfaceLocator_QNAME, String.class, InvLotInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "MessageText", scope = ShipmentDocumentResponseLine.class)
    public JAXBElement<String> createShipmentDocumentResponseLineMessageText(String value) {
        return new JAXBElement<String>(_ShipmentDocumentResponseLineMessageText_QNAME, String.class, ShipmentDocumentResponseLine.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "Shipment", scope = ShipmentDocumentResponseLine.class)
    public JAXBElement<String> createShipmentDocumentResponseLineShipment(String value) {
        return new JAXBElement<String>(_ShipmentInformationShipment_QNAME, String.class, ShipmentDocumentResponseLine.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "DocumentName", scope = ShipmentDocumentResponseLine.class)
    public JAXBElement<String> createShipmentDocumentResponseLineDocumentName(String value) {
        return new JAXBElement<String>(_ShipmentDocumentResponseLineDocumentName_QNAME, String.class, ShipmentDocumentResponseLine.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "JobProcessId", scope = ShipmentDocumentResponseLine.class)
    public JAXBElement<Long> createShipmentDocumentResponseLineJobProcessId(Long value) {
        return new JAXBElement<Long>(_ShipmentDocumentResponseLineJobProcessId_QNAME, Long.class, ShipmentDocumentResponseLine.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "DocumentSubmissionTime", scope = ShipmentDocumentResponseLine.class)
    public JAXBElement<XMLGregorianCalendar> createShipmentDocumentResponseLineDocumentSubmissionTime(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_ShipmentDocumentResponseLineDocumentSubmissionTime_QNAME, XMLGregorianCalendar.class, ShipmentDocumentResponseLine.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "DocumentCompletionTime", scope = ShipmentDocumentResponseLine.class)
    public JAXBElement<XMLGregorianCalendar> createShipmentDocumentResponseLineDocumentCompletionTime(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_ShipmentDocumentResponseLineDocumentCompletionTime_QNAME, XMLGregorianCalendar.class, ShipmentDocumentResponseLine.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "Status", scope = ShipmentDocumentResponseLine.class)
    public JAXBElement<String> createShipmentDocumentResponseLineStatus(String value) {
        return new JAXBElement<String>(_ShipmentDocumentResponseLineStatus_QNAME, String.class, ShipmentDocumentResponseLine.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link byte[]}{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "DocumentContent", scope = ShipmentDocumentResponseLine.class)
    public JAXBElement<byte[]> createShipmentDocumentResponseLineDocumentContent(byte[] value) {
        return new JAXBElement<byte[]>(_ShipmentDocumentResponseLineDocumentContent_QNAME, byte[].class, ShipmentDocumentResponseLine.class, ((byte[]) value));
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "DocumentContentType", scope = ShipmentDocumentResponseLine.class)
    public JAXBElement<String> createShipmentDocumentResponseLineDocumentContentType(String value) {
        return new JAXBElement<String>(_ShipmentDocumentResponseLineDocumentContentType_QNAME, String.class, ShipmentDocumentResponseLine.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "DocumentDescription", scope = ShipmentDocumentResponseLine.class)
    public JAXBElement<String> createShipmentDocumentResponseLineDocumentDescription(String value) {
        return new JAXBElement<String>(_ShipmentDocumentResponseLineDocumentDescription_QNAME, String.class, ShipmentDocumentResponseLine.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "DocumentSubmittedBy", scope = ShipmentDocumentResponseLine.class)
    public JAXBElement<String> createShipmentDocumentResponseLineDocumentSubmittedBy(String value) {
        return new JAXBElement<String>(_ShipmentDocumentResponseLineDocumentSubmittedBy_QNAME, String.class, ShipmentDocumentResponseLine.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/types/", name = "InitMsgList", scope = ProcessCreateUpdateShipmentAsync.class)
    public JAXBElement<String> createProcessCreateUpdateShipmentAsyncInitMsgList(String value) {
        return new JAXBElement<String>(_ProcessShipmentActionInitMsgList_QNAME, String.class, ProcessCreateUpdateShipmentAsync.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "MessageText", scope = ErrorMessage.class)
    public JAXBElement<String> createErrorMessageMessageText(String value) {
        return new JAXBElement<String>(_ShipmentDocumentResponseLineMessageText_QNAME, String.class, ErrorMessage.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "Shipment", scope = ShipmentResponse.class)
    public JAXBElement<String> createShipmentResponseShipment(String value) {
        return new JAXBElement<String>(_ShipmentInformationShipment_QNAME, String.class, ShipmentResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "MessageCount", scope = ShipmentResponse.class)
    public JAXBElement<Long> createShipmentResponseMessageCount(Long value) {
        return new JAXBElement<Long>(_ShipmentResponseMessageCount_QNAME, Long.class, ShipmentResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "ReturnStatus", scope = ShipmentResponse.class)
    public JAXBElement<String> createShipmentResponseReturnStatus(String value) {
        return new JAXBElement<String>(_ShipmentResponseReturnStatus_QNAME, String.class, ShipmentResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "AttributeDate1", scope = ContainerInterface.class)
    public JAXBElement<XMLGregorianCalendar> createContainerInterfaceAttributeDate1(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_InvSerialsInterfaceAttributeDate1_QNAME, XMLGregorianCalendar.class, ContainerInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "AttributeDate5", scope = ContainerInterface.class)
    public JAXBElement<XMLGregorianCalendar> createContainerInterfaceAttributeDate5(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_InvSerialsInterfaceAttributeDate5_QNAME, XMLGregorianCalendar.class, ContainerInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "AttributeDate4", scope = ContainerInterface.class)
    public JAXBElement<XMLGregorianCalendar> createContainerInterfaceAttributeDate4(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_InvSerialsInterfaceAttributeDate4_QNAME, XMLGregorianCalendar.class, ContainerInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "AttributeDate3", scope = ContainerInterface.class)
    public JAXBElement<XMLGregorianCalendar> createContainerInterfaceAttributeDate3(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_InvSerialsInterfaceAttributeDate3_QNAME, XMLGregorianCalendar.class, ContainerInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "AttributeDate2", scope = ContainerInterface.class)
    public JAXBElement<XMLGregorianCalendar> createContainerInterfaceAttributeDate2(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_InvSerialsInterfaceAttributeDate2_QNAME, XMLGregorianCalendar.class, ContainerInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "AttributeNumber2", scope = ContainerInterface.class)
    public JAXBElement<BigDecimal> createContainerInterfaceAttributeNumber2(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_InvSerialsInterfaceAttributeNumber2_QNAME, BigDecimal.class, ContainerInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "AttributeNumber1", scope = ContainerInterface.class)
    public JAXBElement<BigDecimal> createContainerInterfaceAttributeNumber1(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_InvSerialsInterfaceAttributeNumber1_QNAME, BigDecimal.class, ContainerInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "AttributeNumber4", scope = ContainerInterface.class)
    public JAXBElement<BigDecimal> createContainerInterfaceAttributeNumber4(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_InvSerialsInterfaceAttributeNumber4_QNAME, BigDecimal.class, ContainerInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "AttributeNumber3", scope = ContainerInterface.class)
    public JAXBElement<BigDecimal> createContainerInterfaceAttributeNumber3(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_InvSerialsInterfaceAttributeNumber3_QNAME, BigDecimal.class, ContainerInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "Attribute10", scope = ContainerInterface.class)
    public JAXBElement<String> createContainerInterfaceAttribute10(String value) {
        return new JAXBElement<String>(_InvSerialsInterfaceAttribute10_QNAME, String.class, ContainerInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "AttributeNumber6", scope = ContainerInterface.class)
    public JAXBElement<BigDecimal> createContainerInterfaceAttributeNumber6(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_InvSerialsInterfaceAttributeNumber6_QNAME, BigDecimal.class, ContainerInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "AttributeNumber5", scope = ContainerInterface.class)
    public JAXBElement<BigDecimal> createContainerInterfaceAttributeNumber5(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_InvSerialsInterfaceAttributeNumber5_QNAME, BigDecimal.class, ContainerInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "AttributeNumber8", scope = ContainerInterface.class)
    public JAXBElement<BigDecimal> createContainerInterfaceAttributeNumber8(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_InvSerialsInterfaceAttributeNumber8_QNAME, BigDecimal.class, ContainerInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "AttributeNumber7", scope = ContainerInterface.class)
    public JAXBElement<BigDecimal> createContainerInterfaceAttributeNumber7(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_InvSerialsInterfaceAttributeNumber7_QNAME, BigDecimal.class, ContainerInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "PackingInstructions", scope = ContainerInterface.class)
    public JAXBElement<String> createContainerInterfacePackingInstructions(String value) {
        return new JAXBElement<String>(_ShipmentLineInterfacePackingInstructions_QNAME, String.class, ContainerInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "Attribute14", scope = ContainerInterface.class)
    public JAXBElement<String> createContainerInterfaceAttribute14(String value) {
        return new JAXBElement<String>(_InvSerialsInterfaceAttribute14_QNAME, String.class, ContainerInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "Attribute13", scope = ContainerInterface.class)
    public JAXBElement<String> createContainerInterfaceAttribute13(String value) {
        return new JAXBElement<String>(_InvSerialsInterfaceAttribute13_QNAME, String.class, ContainerInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "AttributeNumber9", scope = ContainerInterface.class)
    public JAXBElement<BigDecimal> createContainerInterfaceAttributeNumber9(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_InvSerialsInterfaceAttributeNumber9_QNAME, BigDecimal.class, ContainerInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "Attribute12", scope = ContainerInterface.class)
    public JAXBElement<String> createContainerInterfaceAttribute12(String value) {
        return new JAXBElement<String>(_InvSerialsInterfaceAttribute12_QNAME, String.class, ContainerInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "Attribute11", scope = ContainerInterface.class)
    public JAXBElement<String> createContainerInterfaceAttribute11(String value) {
        return new JAXBElement<String>(_InvSerialsInterfaceAttribute11_QNAME, String.class, ContainerInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "ContainerVolumeUOMName", scope = ContainerInterface.class)
    public JAXBElement<String> createContainerInterfaceContainerVolumeUOMName(String value) {
        return new JAXBElement<String>(_ContainerInterfaceContainerVolumeUOMName_QNAME, String.class, ContainerInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "Attribute18", scope = ContainerInterface.class)
    public JAXBElement<String> createContainerInterfaceAttribute18(String value) {
        return new JAXBElement<String>(_InvSerialsInterfaceAttribute18_QNAME, String.class, ContainerInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "Attribute17", scope = ContainerInterface.class)
    public JAXBElement<String> createContainerInterfaceAttribute17(String value) {
        return new JAXBElement<String>(_InvSerialsInterfaceAttribute17_QNAME, String.class, ContainerInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "Attribute16", scope = ContainerInterface.class)
    public JAXBElement<String> createContainerInterfaceAttribute16(String value) {
        return new JAXBElement<String>(_InvSerialsInterfaceAttribute16_QNAME, String.class, ContainerInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "Attribute15", scope = ContainerInterface.class)
    public JAXBElement<String> createContainerInterfaceAttribute15(String value) {
        return new JAXBElement<String>(_InvSerialsInterfaceAttribute15_QNAME, String.class, ContainerInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "Attribute19", scope = ContainerInterface.class)
    public JAXBElement<String> createContainerInterfaceAttribute19(String value) {
        return new JAXBElement<String>(_InvSerialsInterfaceAttribute19_QNAME, String.class, ContainerInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "Attribute1", scope = ContainerInterface.class)
    public JAXBElement<String> createContainerInterfaceAttribute1(String value) {
        return new JAXBElement<String>(_InvSerialsInterfaceAttribute1_QNAME, String.class, ContainerInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "Attribute20", scope = ContainerInterface.class)
    public JAXBElement<String> createContainerInterfaceAttribute20(String value) {
        return new JAXBElement<String>(_InvSerialsInterfaceAttribute20_QNAME, String.class, ContainerInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "AttributeCategory", scope = ContainerInterface.class)
    public JAXBElement<String> createContainerInterfaceAttributeCategory(String value) {
        return new JAXBElement<String>(_InvSerialsInterfaceAttributeCategory_QNAME, String.class, ContainerInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "Attribute3", scope = ContainerInterface.class)
    public JAXBElement<String> createContainerInterfaceAttribute3(String value) {
        return new JAXBElement<String>(_InvSerialsInterfaceAttribute3_QNAME, String.class, ContainerInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "Attribute2", scope = ContainerInterface.class)
    public JAXBElement<String> createContainerInterfaceAttribute2(String value) {
        return new JAXBElement<String>(_InvSerialsInterfaceAttribute2_QNAME, String.class, ContainerInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "Attribute9", scope = ContainerInterface.class)
    public JAXBElement<String> createContainerInterfaceAttribute9(String value) {
        return new JAXBElement<String>(_InvSerialsInterfaceAttribute9_QNAME, String.class, ContainerInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "Attribute8", scope = ContainerInterface.class)
    public JAXBElement<String> createContainerInterfaceAttribute8(String value) {
        return new JAXBElement<String>(_InvSerialsInterfaceAttribute8_QNAME, String.class, ContainerInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "TareWeight", scope = ContainerInterface.class)
    public JAXBElement<BigDecimal> createContainerInterfaceTareWeight(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_ContainerInterfaceTareWeight_QNAME, BigDecimal.class, ContainerInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "SealNumber", scope = ContainerInterface.class)
    public JAXBElement<String> createContainerInterfaceSealNumber(String value) {
        return new JAXBElement<String>(_ShipmentInformationSealNumber_QNAME, String.class, ContainerInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "Attribute5", scope = ContainerInterface.class)
    public JAXBElement<String> createContainerInterfaceAttribute5(String value) {
        return new JAXBElement<String>(_InvSerialsInterfaceAttribute5_QNAME, String.class, ContainerInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "Attribute4", scope = ContainerInterface.class)
    public JAXBElement<String> createContainerInterfaceAttribute4(String value) {
        return new JAXBElement<String>(_InvSerialsInterfaceAttribute4_QNAME, String.class, ContainerInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "TrackingNumber", scope = ContainerInterface.class)
    public JAXBElement<String> createContainerInterfaceTrackingNumber(String value) {
        return new JAXBElement<String>(_ContainerInterfaceTrackingNumber_QNAME, String.class, ContainerInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "Attribute7", scope = ContainerInterface.class)
    public JAXBElement<String> createContainerInterfaceAttribute7(String value) {
        return new JAXBElement<String>(_InvSerialsInterfaceAttribute7_QNAME, String.class, ContainerInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "Attribute6", scope = ContainerInterface.class)
    public JAXBElement<String> createContainerInterfaceAttribute6(String value) {
        return new JAXBElement<String>(_InvSerialsInterfaceAttribute6_QNAME, String.class, ContainerInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "MasterSerialNumber", scope = ContainerInterface.class)
    public JAXBElement<String> createContainerInterfaceMasterSerialNumber(String value) {
        return new JAXBElement<String>(_ContainerInterfaceMasterSerialNumber_QNAME, String.class, ContainerInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "LicensePlateNumber", scope = ContainerInterface.class)
    public JAXBElement<String> createContainerInterfaceLicensePlateNumber(String value) {
        return new JAXBElement<String>(_ContainerInterfaceLicensePlateNumber_QNAME, String.class, ContainerInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "FOBCode", scope = ContainerInterface.class)
    public JAXBElement<String> createContainerInterfaceFOBCode(String value) {
        return new JAXBElement<String>(_ContainerInterfaceFOBCode_QNAME, String.class, ContainerInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "TareWeightUOMName", scope = ContainerInterface.class)
    public JAXBElement<String> createContainerInterfaceTareWeightUOMName(String value) {
        return new JAXBElement<String>(_ContainerInterfaceTareWeightUOMName_QNAME, String.class, ContainerInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "GrossWeight", scope = ContainerInterface.class)
    public JAXBElement<BigDecimal> createContainerInterfaceGrossWeight(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_ShipmentLineInterfaceGrossWeight_QNAME, BigDecimal.class, ContainerInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "ContainerVolume", scope = ContainerInterface.class)
    public JAXBElement<BigDecimal> createContainerInterfaceContainerVolume(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_ContainerInterfaceContainerVolume_QNAME, BigDecimal.class, ContainerInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "AttributeNumber10", scope = ContainerInterface.class)
    public JAXBElement<BigDecimal> createContainerInterfaceAttributeNumber10(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_InvSerialsInterfaceAttributeNumber10_QNAME, BigDecimal.class, ContainerInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "GrossWeightUOMName", scope = ContainerInterface.class)
    public JAXBElement<String> createContainerInterfaceGrossWeightUOMName(String value) {
        return new JAXBElement<String>(_ContainerInterfaceGrossWeightUOMName_QNAME, String.class, ContainerInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "ShippingInstructions", scope = ContainerInterface.class)
    public JAXBElement<String> createContainerInterfaceShippingInstructions(String value) {
        return new JAXBElement<String>(_ShipmentLineInterfaceShippingInstructions_QNAME, String.class, ContainerInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "AttributeTimestamp1", scope = ContainerInterface.class)
    public JAXBElement<XMLGregorianCalendar> createContainerInterfaceAttributeTimestamp1(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_InvSerialsInterfaceAttributeTimestamp1_QNAME, XMLGregorianCalendar.class, ContainerInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "AttributeTimestamp5", scope = ContainerInterface.class)
    public JAXBElement<XMLGregorianCalendar> createContainerInterfaceAttributeTimestamp5(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_InvSerialsInterfaceAttributeTimestamp5_QNAME, XMLGregorianCalendar.class, ContainerInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "ContainerItemName", scope = ContainerInterface.class)
    public JAXBElement<String> createContainerInterfaceContainerItemName(String value) {
        return new JAXBElement<String>(_ContainerInterfaceContainerItemName_QNAME, String.class, ContainerInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "AttributeTimestamp4", scope = ContainerInterface.class)
    public JAXBElement<XMLGregorianCalendar> createContainerInterfaceAttributeTimestamp4(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_InvSerialsInterfaceAttributeTimestamp4_QNAME, XMLGregorianCalendar.class, ContainerInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "AttributeTimestamp3", scope = ContainerInterface.class)
    public JAXBElement<XMLGregorianCalendar> createContainerInterfaceAttributeTimestamp3(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_InvSerialsInterfaceAttributeTimestamp3_QNAME, XMLGregorianCalendar.class, ContainerInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "AttributeTimestamp2", scope = ContainerInterface.class)
    public JAXBElement<XMLGregorianCalendar> createContainerInterfaceAttributeTimestamp2(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_InvSerialsInterfaceAttributeTimestamp2_QNAME, XMLGregorianCalendar.class, ContainerInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "MessageText", scope = ShipmentDocumentResponseHeader.class)
    public JAXBElement<String> createShipmentDocumentResponseHeaderMessageText(String value) {
        return new JAXBElement<String>(_ShipmentDocumentResponseLineMessageText_QNAME, String.class, ShipmentDocumentResponseHeader.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "Shipment", scope = ShipmentDocumentResponseHeader.class)
    public JAXBElement<String> createShipmentDocumentResponseHeaderShipment(String value) {
        return new JAXBElement<String>(_ShipmentInformationShipment_QNAME, String.class, ShipmentDocumentResponseHeader.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "TransportationShipment", scope = ShipmentDocumentResponseHeader.class)
    public JAXBElement<String> createShipmentDocumentResponseHeaderTransportationShipment(String value) {
        return new JAXBElement<String>(_ShipmentDocumentResponseHeaderTransportationShipment_QNAME, String.class, ShipmentDocumentResponseHeader.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "Status", scope = ShipmentDocumentResponseHeader.class)
    public JAXBElement<String> createShipmentDocumentResponseHeaderStatus(String value) {
        return new JAXBElement<String>(_ShipmentDocumentResponseLineStatus_QNAME, String.class, ShipmentDocumentResponseHeader.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "ShipmentStatus", scope = ShipmentDocumentResponseHeader.class)
    public JAXBElement<String> createShipmentDocumentResponseHeaderShipmentStatus(String value) {
        return new JAXBElement<String>(_ShipmentDocumentResponseHeaderShipmentStatus_QNAME, String.class, ShipmentDocumentResponseHeader.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "FOBAddress2", scope = ShipmentInterface.class)
    public JAXBElement<String> createShipmentInterfaceFOBAddress2(String value) {
        return new JAXBElement<String>(_ShipmentInterfaceFOBAddress2_QNAME, String.class, ShipmentInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "FOBAddress1", scope = ShipmentInterface.class)
    public JAXBElement<String> createShipmentInterfaceFOBAddress1(String value) {
        return new JAXBElement<String>(_ShipmentInterfaceFOBAddress1_QNAME, String.class, ShipmentInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "FOBState", scope = ShipmentInterface.class)
    public JAXBElement<String> createShipmentInterfaceFOBState(String value) {
        return new JAXBElement<String>(_ShipmentInterfaceFOBState_QNAME, String.class, ShipmentInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AmountType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "CODAmount", scope = ShipmentInterface.class)
    public JAXBElement<AmountType> createShipmentInterfaceCODAmount(AmountType value) {
        return new JAXBElement<AmountType>(_ShipmentInformationCODAmount_QNAME, AmountType.class, ShipmentInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "DockCode", scope = ShipmentInterface.class)
    public JAXBElement<String> createShipmentInterfaceDockCode(String value) {
        return new JAXBElement<String>(_ShipmentInterfaceDockCode_QNAME, String.class, ShipmentInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "FOBAddress4", scope = ShipmentInterface.class)
    public JAXBElement<String> createShipmentInterfaceFOBAddress4(String value) {
        return new JAXBElement<String>(_ShipmentInterfaceFOBAddress4_QNAME, String.class, ShipmentInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "FOBAddress3", scope = ShipmentInterface.class)
    public JAXBElement<String> createShipmentInterfaceFOBAddress3(String value) {
        return new JAXBElement<String>(_ShipmentInterfaceFOBAddress3_QNAME, String.class, ShipmentInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "FOBCounty", scope = ShipmentInterface.class)
    public JAXBElement<String> createShipmentInterfaceFOBCounty(String value) {
        return new JAXBElement<String>(_ShipmentInterfaceFOBCounty_QNAME, String.class, ShipmentInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "Attribute10", scope = ShipmentInterface.class)
    public JAXBElement<String> createShipmentInterfaceAttribute10(String value) {
        return new JAXBElement<String>(_InvSerialsInterfaceAttribute10_QNAME, String.class, ShipmentInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "GlobalAttributeCategory", scope = ShipmentInterface.class)
    public JAXBElement<String> createShipmentInterfaceGlobalAttributeCategory(String value) {
        return new JAXBElement<String>(_ShipmentInformationGlobalAttributeCategory_QNAME, String.class, ShipmentInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "Attribute14", scope = ShipmentInterface.class)
    public JAXBElement<String> createShipmentInterfaceAttribute14(String value) {
        return new JAXBElement<String>(_InvSerialsInterfaceAttribute14_QNAME, String.class, ShipmentInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "Attribute13", scope = ShipmentInterface.class)
    public JAXBElement<String> createShipmentInterfaceAttribute13(String value) {
        return new JAXBElement<String>(_InvSerialsInterfaceAttribute13_QNAME, String.class, ShipmentInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "NetWeight", scope = ShipmentInterface.class)
    public JAXBElement<BigDecimal> createShipmentInterfaceNetWeight(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_ShipmentLineInterfaceNetWeight_QNAME, BigDecimal.class, ShipmentInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "ActualShipDate", scope = ShipmentInterface.class)
    public JAXBElement<XMLGregorianCalendar> createShipmentInterfaceActualShipDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_ShipmentInterfaceActualShipDate_QNAME, XMLGregorianCalendar.class, ShipmentInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "Attribute12", scope = ShipmentInterface.class)
    public JAXBElement<String> createShipmentInterfaceAttribute12(String value) {
        return new JAXBElement<String>(_InvSerialsInterfaceAttribute12_QNAME, String.class, ShipmentInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "Attribute11", scope = ShipmentInterface.class)
    public JAXBElement<String> createShipmentInterfaceAttribute11(String value) {
        return new JAXBElement<String>(_InvSerialsInterfaceAttribute11_QNAME, String.class, ShipmentInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "GlobalAttribute9", scope = ShipmentInterface.class)
    public JAXBElement<String> createShipmentInterfaceGlobalAttribute9(String value) {
        return new JAXBElement<String>(_ShipmentInformationGlobalAttribute9_QNAME, String.class, ShipmentInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "CODRemitTo", scope = ShipmentInterface.class)
    public JAXBElement<String> createShipmentInterfaceCODRemitTo(String value) {
        return new JAXBElement<String>(_ShipmentInformationCODRemitTo_QNAME, String.class, ShipmentInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "OrganizationCode", scope = ShipmentInterface.class)
    public JAXBElement<String> createShipmentInterfaceOrganizationCode(String value) {
        return new JAXBElement<String>(_ShipmentInterfaceOrganizationCode_QNAME, String.class, ShipmentInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "Attribute18", scope = ShipmentInterface.class)
    public JAXBElement<String> createShipmentInterfaceAttribute18(String value) {
        return new JAXBElement<String>(_InvSerialsInterfaceAttribute18_QNAME, String.class, ShipmentInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "Attribute17", scope = ShipmentInterface.class)
    public JAXBElement<String> createShipmentInterfaceAttribute17(String value) {
        return new JAXBElement<String>(_InvSerialsInterfaceAttribute17_QNAME, String.class, ShipmentInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "Attribute16", scope = ShipmentInterface.class)
    public JAXBElement<String> createShipmentInterfaceAttribute16(String value) {
        return new JAXBElement<String>(_InvSerialsInterfaceAttribute16_QNAME, String.class, ShipmentInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "Attribute15", scope = ShipmentInterface.class)
    public JAXBElement<String> createShipmentInterfaceAttribute15(String value) {
        return new JAXBElement<String>(_InvSerialsInterfaceAttribute15_QNAME, String.class, ShipmentInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "Attribute19", scope = ShipmentInterface.class)
    public JAXBElement<String> createShipmentInterfaceAttribute19(String value) {
        return new JAXBElement<String>(_InvSerialsInterfaceAttribute19_QNAME, String.class, ShipmentInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "Attribute20", scope = ShipmentInterface.class)
    public JAXBElement<String> createShipmentInterfaceAttribute20(String value) {
        return new JAXBElement<String>(_InvSerialsInterfaceAttribute20_QNAME, String.class, ShipmentInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "AttributeCategory", scope = ShipmentInterface.class)
    public JAXBElement<String> createShipmentInterfaceAttributeCategory(String value) {
        return new JAXBElement<String>(_InvSerialsInterfaceAttributeCategory_QNAME, String.class, ShipmentInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "CODCurrencyCode", scope = ShipmentInterface.class)
    public JAXBElement<String> createShipmentInterfaceCODCurrencyCode(String value) {
        return new JAXBElement<String>(_ShipmentInterfaceCODCurrencyCode_QNAME, String.class, ShipmentInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "Description", scope = ShipmentInterface.class)
    public JAXBElement<String> createShipmentInterfaceDescription(String value) {
        return new JAXBElement<String>(_ShipmentInformationDescription_QNAME, String.class, ShipmentInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "GlobalAttribute18", scope = ShipmentInterface.class)
    public JAXBElement<String> createShipmentInterfaceGlobalAttribute18(String value) {
        return new JAXBElement<String>(_ShipmentInformationGlobalAttribute18_QNAME, String.class, ShipmentInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "GlobalAttribute19", scope = ShipmentInterface.class)
    public JAXBElement<String> createShipmentInterfaceGlobalAttribute19(String value) {
        return new JAXBElement<String>(_ShipmentInformationGlobalAttribute19_QNAME, String.class, ShipmentInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "ModeOfTransport", scope = ShipmentInterface.class)
    public JAXBElement<String> createShipmentInterfaceModeOfTransport(String value) {
        return new JAXBElement<String>(_ShipmentInformationModeOfTransport_QNAME, String.class, ShipmentInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "Volume", scope = ShipmentInterface.class)
    public JAXBElement<BigDecimal> createShipmentInterfaceVolume(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_ShipmentLineInterfaceVolume_QNAME, BigDecimal.class, ShipmentInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "GlobalAttribute20", scope = ShipmentInterface.class)
    public JAXBElement<String> createShipmentInterfaceGlobalAttribute20(String value) {
        return new JAXBElement<String>(_ShipmentInformationGlobalAttribute20_QNAME, String.class, ShipmentInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "FOBCountry", scope = ShipmentInterface.class)
    public JAXBElement<String> createShipmentInterfaceFOBCountry(String value) {
        return new JAXBElement<String>(_ShipmentInterfaceFOBCountry_QNAME, String.class, ShipmentInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "CarrierName", scope = ShipmentInterface.class)
    public JAXBElement<String> createShipmentInterfaceCarrierName(String value) {
        return new JAXBElement<String>(_ShipmentInterfaceCarrierName_QNAME, String.class, ShipmentInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "CODPaidBy", scope = ShipmentInterface.class)
    public JAXBElement<String> createShipmentInterfaceCODPaidBy(String value) {
        return new JAXBElement<String>(_ShipmentInformationCODPaidBy_QNAME, String.class, ShipmentInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "FOBCity", scope = ShipmentInterface.class)
    public JAXBElement<String> createShipmentInterfaceFOBCity(String value) {
        return new JAXBElement<String>(_ShipmentInterfaceFOBCity_QNAME, String.class, ShipmentInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "GrossWeight", scope = ShipmentInterface.class)
    public JAXBElement<BigDecimal> createShipmentInterfaceGrossWeight(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_ShipmentLineInterfaceGrossWeight_QNAME, BigDecimal.class, ShipmentInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "GlobalAttribute1", scope = ShipmentInterface.class)
    public JAXBElement<String> createShipmentInterfaceGlobalAttribute1(String value) {
        return new JAXBElement<String>(_ShipmentInformationGlobalAttribute1_QNAME, String.class, ShipmentInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "GlobalAttribute2", scope = ShipmentInterface.class)
    public JAXBElement<String> createShipmentInterfaceGlobalAttribute2(String value) {
        return new JAXBElement<String>(_ShipmentInformationGlobalAttribute2_QNAME, String.class, ShipmentInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "GlobalAttribute3", scope = ShipmentInterface.class)
    public JAXBElement<String> createShipmentInterfaceGlobalAttribute3(String value) {
        return new JAXBElement<String>(_ShipmentInformationGlobalAttribute3_QNAME, String.class, ShipmentInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "GlobalAttribute4", scope = ShipmentInterface.class)
    public JAXBElement<String> createShipmentInterfaceGlobalAttribute4(String value) {
        return new JAXBElement<String>(_ShipmentInformationGlobalAttribute4_QNAME, String.class, ShipmentInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "GlobalAttribute5", scope = ShipmentInterface.class)
    public JAXBElement<String> createShipmentInterfaceGlobalAttribute5(String value) {
        return new JAXBElement<String>(_ShipmentInformationGlobalAttribute5_QNAME, String.class, ShipmentInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "GlobalAttribute6", scope = ShipmentInterface.class)
    public JAXBElement<String> createShipmentInterfaceGlobalAttribute6(String value) {
        return new JAXBElement<String>(_ShipmentInformationGlobalAttribute6_QNAME, String.class, ShipmentInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "GlobalAttribute7", scope = ShipmentInterface.class)
    public JAXBElement<String> createShipmentInterfaceGlobalAttribute7(String value) {
        return new JAXBElement<String>(_ShipmentInformationGlobalAttribute7_QNAME, String.class, ShipmentInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "GlobalAttribute8", scope = ShipmentInterface.class)
    public JAXBElement<String> createShipmentInterfaceGlobalAttribute8(String value) {
        return new JAXBElement<String>(_ShipmentInformationGlobalAttribute8_QNAME, String.class, ShipmentInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "PackingSlipNumber", scope = ShipmentInterface.class)
    public JAXBElement<String> createShipmentInterfacePackingSlipNumber(String value) {
        return new JAXBElement<String>(_ShipmentInterfacePackingSlipNumber_QNAME, String.class, ShipmentInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "GlobalAttributeDate2", scope = ShipmentInterface.class)
    public JAXBElement<XMLGregorianCalendar> createShipmentInterfaceGlobalAttributeDate2(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_ShipmentInformationGlobalAttributeDate2_QNAME, XMLGregorianCalendar.class, ShipmentInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "GlobalAttributeDate1", scope = ShipmentInterface.class)
    public JAXBElement<XMLGregorianCalendar> createShipmentInterfaceGlobalAttributeDate1(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_ShipmentInformationGlobalAttributeDate1_QNAME, XMLGregorianCalendar.class, ShipmentInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "GlobalAttributeDate4", scope = ShipmentInterface.class)
    public JAXBElement<XMLGregorianCalendar> createShipmentInterfaceGlobalAttributeDate4(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_ShipmentInformationGlobalAttributeDate4_QNAME, XMLGregorianCalendar.class, ShipmentInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "VolumeUOMName", scope = ShipmentInterface.class)
    public JAXBElement<String> createShipmentInterfaceVolumeUOMName(String value) {
        return new JAXBElement<String>(_ShipmentLineInterfaceVolumeUOMName_QNAME, String.class, ShipmentInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "GlobalAttributeDate3", scope = ShipmentInterface.class)
    public JAXBElement<XMLGregorianCalendar> createShipmentInterfaceGlobalAttributeDate3(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_ShipmentInformationGlobalAttributeDate3_QNAME, XMLGregorianCalendar.class, ShipmentInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "GlobalAttributeDate5", scope = ShipmentInterface.class)
    public JAXBElement<XMLGregorianCalendar> createShipmentInterfaceGlobalAttributeDate5(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_ShipmentInformationGlobalAttributeDate5_QNAME, XMLGregorianCalendar.class, ShipmentInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "ConfirmedBy", scope = ShipmentInterface.class)
    public JAXBElement<String> createShipmentInterfaceConfirmedBy(String value) {
        return new JAXBElement<String>(_ShipmentInterfaceConfirmedBy_QNAME, String.class, ShipmentInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "AttributeTimestamp1", scope = ShipmentInterface.class)
    public JAXBElement<XMLGregorianCalendar> createShipmentInterfaceAttributeTimestamp1(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_InvSerialsInterfaceAttributeTimestamp1_QNAME, XMLGregorianCalendar.class, ShipmentInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "AttributeTimestamp5", scope = ShipmentInterface.class)
    public JAXBElement<XMLGregorianCalendar> createShipmentInterfaceAttributeTimestamp5(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_InvSerialsInterfaceAttributeTimestamp5_QNAME, XMLGregorianCalendar.class, ShipmentInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "AttributeTimestamp4", scope = ShipmentInterface.class)
    public JAXBElement<XMLGregorianCalendar> createShipmentInterfaceAttributeTimestamp4(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_InvSerialsInterfaceAttributeTimestamp4_QNAME, XMLGregorianCalendar.class, ShipmentInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "AttributeTimestamp3", scope = ShipmentInterface.class)
    public JAXBElement<XMLGregorianCalendar> createShipmentInterfaceAttributeTimestamp3(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_InvSerialsInterfaceAttributeTimestamp3_QNAME, XMLGregorianCalendar.class, ShipmentInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "AttributeTimestamp2", scope = ShipmentInterface.class)
    public JAXBElement<XMLGregorianCalendar> createShipmentInterfaceAttributeTimestamp2(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_InvSerialsInterfaceAttributeTimestamp2_QNAME, XMLGregorianCalendar.class, ShipmentInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "AttributeDate1", scope = ShipmentInterface.class)
    public JAXBElement<XMLGregorianCalendar> createShipmentInterfaceAttributeDate1(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_InvSerialsInterfaceAttributeDate1_QNAME, XMLGregorianCalendar.class, ShipmentInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "AttributeDate5", scope = ShipmentInterface.class)
    public JAXBElement<XMLGregorianCalendar> createShipmentInterfaceAttributeDate5(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_InvSerialsInterfaceAttributeDate5_QNAME, XMLGregorianCalendar.class, ShipmentInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "AttributeDate4", scope = ShipmentInterface.class)
    public JAXBElement<XMLGregorianCalendar> createShipmentInterfaceAttributeDate4(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_InvSerialsInterfaceAttributeDate4_QNAME, XMLGregorianCalendar.class, ShipmentInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "AttributeDate3", scope = ShipmentInterface.class)
    public JAXBElement<XMLGregorianCalendar> createShipmentInterfaceAttributeDate3(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_InvSerialsInterfaceAttributeDate3_QNAME, XMLGregorianCalendar.class, ShipmentInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "TransportationReason", scope = ShipmentInterface.class)
    public JAXBElement<String> createShipmentInterfaceTransportationReason(String value) {
        return new JAXBElement<String>(_ShipmentInformationTransportationReason_QNAME, String.class, ShipmentInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "AttributeDate2", scope = ShipmentInterface.class)
    public JAXBElement<XMLGregorianCalendar> createShipmentInterfaceAttributeDate2(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_InvSerialsInterfaceAttributeDate2_QNAME, XMLGregorianCalendar.class, ShipmentInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "AttributeNumber2", scope = ShipmentInterface.class)
    public JAXBElement<BigDecimal> createShipmentInterfaceAttributeNumber2(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_InvSerialsInterfaceAttributeNumber2_QNAME, BigDecimal.class, ShipmentInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "FreightTermsCode", scope = ShipmentInterface.class)
    public JAXBElement<String> createShipmentInterfaceFreightTermsCode(String value) {
        return new JAXBElement<String>(_ShipmentInterfaceFreightTermsCode_QNAME, String.class, ShipmentInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "AttributeNumber1", scope = ShipmentInterface.class)
    public JAXBElement<BigDecimal> createShipmentInterfaceAttributeNumber1(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_InvSerialsInterfaceAttributeNumber1_QNAME, BigDecimal.class, ShipmentInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "Equipment", scope = ShipmentInterface.class)
    public JAXBElement<String> createShipmentInterfaceEquipment(String value) {
        return new JAXBElement<String>(_ShipmentInformationEquipment_QNAME, String.class, ShipmentInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "AttributeNumber4", scope = ShipmentInterface.class)
    public JAXBElement<BigDecimal> createShipmentInterfaceAttributeNumber4(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_InvSerialsInterfaceAttributeNumber4_QNAME, BigDecimal.class, ShipmentInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "AttributeNumber3", scope = ShipmentInterface.class)
    public JAXBElement<BigDecimal> createShipmentInterfaceAttributeNumber3(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_InvSerialsInterfaceAttributeNumber3_QNAME, BigDecimal.class, ShipmentInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "AttributeNumber6", scope = ShipmentInterface.class)
    public JAXBElement<BigDecimal> createShipmentInterfaceAttributeNumber6(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_InvSerialsInterfaceAttributeNumber6_QNAME, BigDecimal.class, ShipmentInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "AttributeNumber5", scope = ShipmentInterface.class)
    public JAXBElement<BigDecimal> createShipmentInterfaceAttributeNumber5(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_InvSerialsInterfaceAttributeNumber5_QNAME, BigDecimal.class, ShipmentInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "AttributeNumber8", scope = ShipmentInterface.class)
    public JAXBElement<BigDecimal> createShipmentInterfaceAttributeNumber8(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_InvSerialsInterfaceAttributeNumber8_QNAME, BigDecimal.class, ShipmentInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "WeightUOMName", scope = ShipmentInterface.class)
    public JAXBElement<String> createShipmentInterfaceWeightUOMName(String value) {
        return new JAXBElement<String>(_ShipmentLineInterfaceWeightUOMName_QNAME, String.class, ShipmentInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "RoutingInstructions", scope = ShipmentInterface.class)
    public JAXBElement<String> createShipmentInterfaceRoutingInstructions(String value) {
        return new JAXBElement<String>(_ShipmentInformationRoutingInstructions_QNAME, String.class, ShipmentInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "AttributeNumber7", scope = ShipmentInterface.class)
    public JAXBElement<BigDecimal> createShipmentInterfaceAttributeNumber7(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_InvSerialsInterfaceAttributeNumber7_QNAME, BigDecimal.class, ShipmentInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "AttributeNumber9", scope = ShipmentInterface.class)
    public JAXBElement<BigDecimal> createShipmentInterfaceAttributeNumber9(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_InvSerialsInterfaceAttributeNumber9_QNAME, BigDecimal.class, ShipmentInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "ShipmentName", scope = ShipmentInterface.class)
    public JAXBElement<String> createShipmentInterfaceShipmentName(String value) {
        return new JAXBElement<String>(_ShipmentInterfaceShipmentName_QNAME, String.class, ShipmentInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "InitialPickupDate", scope = ShipmentInterface.class)
    public JAXBElement<XMLGregorianCalendar> createShipmentInterfaceInitialPickupDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_ShipmentInterfaceInitialPickupDate_QNAME, XMLGregorianCalendar.class, ShipmentInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "ShippingMarks", scope = ShipmentInterface.class)
    public JAXBElement<String> createShipmentInterfaceShippingMarks(String value) {
        return new JAXBElement<String>(_ShipmentInformationShippingMarks_QNAME, String.class, ShipmentInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "FOBRegion", scope = ShipmentInterface.class)
    public JAXBElement<String> createShipmentInterfaceFOBRegion(String value) {
        return new JAXBElement<String>(_ShipmentInterfaceFOBRegion_QNAME, String.class, ShipmentInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "BillOfLadingNumber", scope = ShipmentInterface.class)
    public JAXBElement<String> createShipmentInterfaceBillOfLadingNumber(String value) {
        return new JAXBElement<String>(_ShipmentInterfaceBillOfLadingNumber_QNAME, String.class, ShipmentInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "Attribute1", scope = ShipmentInterface.class)
    public JAXBElement<String> createShipmentInterfaceAttribute1(String value) {
        return new JAXBElement<String>(_InvSerialsInterfaceAttribute1_QNAME, String.class, ShipmentInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "Attribute3", scope = ShipmentInterface.class)
    public JAXBElement<String> createShipmentInterfaceAttribute3(String value) {
        return new JAXBElement<String>(_InvSerialsInterfaceAttribute3_QNAME, String.class, ShipmentInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "Attribute2", scope = ShipmentInterface.class)
    public JAXBElement<String> createShipmentInterfaceAttribute2(String value) {
        return new JAXBElement<String>(_InvSerialsInterfaceAttribute2_QNAME, String.class, ShipmentInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "Waybill", scope = ShipmentInterface.class)
    public JAXBElement<String> createShipmentInterfaceWaybill(String value) {
        return new JAXBElement<String>(_ShipmentInformationWaybill_QNAME, String.class, ShipmentInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "ProblemContactReference", scope = ShipmentInterface.class)
    public JAXBElement<String> createShipmentInterfaceProblemContactReference(String value) {
        return new JAXBElement<String>(_ShipmentInformationProblemContactReference_QNAME, String.class, ShipmentInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "Attribute9", scope = ShipmentInterface.class)
    public JAXBElement<String> createShipmentInterfaceAttribute9(String value) {
        return new JAXBElement<String>(_InvSerialsInterfaceAttribute9_QNAME, String.class, ShipmentInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "GlobalAttribute12", scope = ShipmentInterface.class)
    public JAXBElement<String> createShipmentInterfaceGlobalAttribute12(String value) {
        return new JAXBElement<String>(_ShipmentInformationGlobalAttribute12_QNAME, String.class, ShipmentInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "Attribute8", scope = ShipmentInterface.class)
    public JAXBElement<String> createShipmentInterfaceAttribute8(String value) {
        return new JAXBElement<String>(_InvSerialsInterfaceAttribute8_QNAME, String.class, ShipmentInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "GlobalAttribute13", scope = ShipmentInterface.class)
    public JAXBElement<String> createShipmentInterfaceGlobalAttribute13(String value) {
        return new JAXBElement<String>(_ShipmentInformationGlobalAttribute13_QNAME, String.class, ShipmentInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "ServiceLevel", scope = ShipmentInterface.class)
    public JAXBElement<String> createShipmentInterfaceServiceLevel(String value) {
        return new JAXBElement<String>(_ShipmentInformationServiceLevel_QNAME, String.class, ShipmentInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "GlobalAttribute10", scope = ShipmentInterface.class)
    public JAXBElement<String> createShipmentInterfaceGlobalAttribute10(String value) {
        return new JAXBElement<String>(_ShipmentInformationGlobalAttribute10_QNAME, String.class, ShipmentInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "GlobalAttribute11", scope = ShipmentInterface.class)
    public JAXBElement<String> createShipmentInterfaceGlobalAttribute11(String value) {
        return new JAXBElement<String>(_ShipmentInformationGlobalAttribute11_QNAME, String.class, ShipmentInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "SealNumber", scope = ShipmentInterface.class)
    public JAXBElement<String> createShipmentInterfaceSealNumber(String value) {
        return new JAXBElement<String>(_ShipmentInformationSealNumber_QNAME, String.class, ShipmentInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "Attribute5", scope = ShipmentInterface.class)
    public JAXBElement<String> createShipmentInterfaceAttribute5(String value) {
        return new JAXBElement<String>(_InvSerialsInterfaceAttribute5_QNAME, String.class, ShipmentInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "GlobalAttribute16", scope = ShipmentInterface.class)
    public JAXBElement<String> createShipmentInterfaceGlobalAttribute16(String value) {
        return new JAXBElement<String>(_ShipmentInformationGlobalAttribute16_QNAME, String.class, ShipmentInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "Attribute4", scope = ShipmentInterface.class)
    public JAXBElement<String> createShipmentInterfaceAttribute4(String value) {
        return new JAXBElement<String>(_InvSerialsInterfaceAttribute4_QNAME, String.class, ShipmentInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "GlobalAttribute17", scope = ShipmentInterface.class)
    public JAXBElement<String> createShipmentInterfaceGlobalAttribute17(String value) {
        return new JAXBElement<String>(_ShipmentInformationGlobalAttribute17_QNAME, String.class, ShipmentInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "Attribute7", scope = ShipmentInterface.class)
    public JAXBElement<String> createShipmentInterfaceAttribute7(String value) {
        return new JAXBElement<String>(_InvSerialsInterfaceAttribute7_QNAME, String.class, ShipmentInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "GlobalAttribute14", scope = ShipmentInterface.class)
    public JAXBElement<String> createShipmentInterfaceGlobalAttribute14(String value) {
        return new JAXBElement<String>(_ShipmentInformationGlobalAttribute14_QNAME, String.class, ShipmentInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "Attribute6", scope = ShipmentInterface.class)
    public JAXBElement<String> createShipmentInterfaceAttribute6(String value) {
        return new JAXBElement<String>(_InvSerialsInterfaceAttribute6_QNAME, String.class, ShipmentInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "GlobalAttribute15", scope = ShipmentInterface.class)
    public JAXBElement<String> createShipmentInterfaceGlobalAttribute15(String value) {
        return new JAXBElement<String>(_ShipmentInformationGlobalAttribute15_QNAME, String.class, ShipmentInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "ConfirmedDate", scope = ShipmentInterface.class)
    public JAXBElement<XMLGregorianCalendar> createShipmentInterfaceConfirmedDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_ShipmentInformationConfirmedDate_QNAME, XMLGregorianCalendar.class, ShipmentInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "GlobalAttributeNumber3", scope = ShipmentInterface.class)
    public JAXBElement<BigDecimal> createShipmentInterfaceGlobalAttributeNumber3(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_ShipmentInformationGlobalAttributeNumber3_QNAME, BigDecimal.class, ShipmentInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "GlobalAttributeNumber2", scope = ShipmentInterface.class)
    public JAXBElement<BigDecimal> createShipmentInterfaceGlobalAttributeNumber2(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_ShipmentInformationGlobalAttributeNumber2_QNAME, BigDecimal.class, ShipmentInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "GlobalAttributeNumber5", scope = ShipmentInterface.class)
    public JAXBElement<BigDecimal> createShipmentInterfaceGlobalAttributeNumber5(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_ShipmentInformationGlobalAttributeNumber5_QNAME, BigDecimal.class, ShipmentInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "GlobalAttributeNumber4", scope = ShipmentInterface.class)
    public JAXBElement<BigDecimal> createShipmentInterfaceGlobalAttributeNumber4(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_ShipmentInformationGlobalAttributeNumber4_QNAME, BigDecimal.class, ShipmentInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "FOBCode", scope = ShipmentInterface.class)
    public JAXBElement<String> createShipmentInterfaceFOBCode(String value) {
        return new JAXBElement<String>(_ContainerInterfaceFOBCode_QNAME, String.class, ShipmentInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "GlobalAttributeNumber1", scope = ShipmentInterface.class)
    public JAXBElement<BigDecimal> createShipmentInterfaceGlobalAttributeNumber1(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_ShipmentInformationGlobalAttributeNumber1_QNAME, BigDecimal.class, ShipmentInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "UltimateDropoffDate", scope = ShipmentInterface.class)
    public JAXBElement<XMLGregorianCalendar> createShipmentInterfaceUltimateDropoffDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_ShipmentInterfaceUltimateDropoffDate_QNAME, XMLGregorianCalendar.class, ShipmentInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "AttributeNumber10", scope = ShipmentInterface.class)
    public JAXBElement<BigDecimal> createShipmentInterfaceAttributeNumber10(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_InvSerialsInterfaceAttributeNumber10_QNAME, BigDecimal.class, ShipmentInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "FOBPostalCode", scope = ShipmentInterface.class)
    public JAXBElement<String> createShipmentInterfaceFOBPostalCode(String value) {
        return new JAXBElement<String>(_ShipmentInterfaceFOBPostalCode_QNAME, String.class, ShipmentInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "EquipmentType", scope = ShipmentInterface.class)
    public JAXBElement<String> createShipmentInterfaceEquipmentType(String value) {
        return new JAXBElement<String>(_ShipmentInformationEquipmentType_QNAME, String.class, ShipmentInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "ASNDateSent", scope = ShipmentInterface.class)
    public JAXBElement<XMLGregorianCalendar> createShipmentInterfaceASNDateSent(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_ShipmentInterfaceASNDateSent_QNAME, XMLGregorianCalendar.class, ShipmentInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "LoadingSequenceRule", scope = ShipmentInterface.class)
    public JAXBElement<String> createShipmentInterfaceLoadingSequenceRule(String value) {
        return new JAXBElement<String>(_ShipmentInformationLoadingSequenceRule_QNAME, String.class, ShipmentInterface.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "NoteTypeCode", scope = ShipmentNotes.class)
    public JAXBElement<String> createShipmentNotesNoteTypeCode(String value) {
        return new JAXBElement<String>(_ShipmentNotesNoteTypeCode_QNAME, String.class, ShipmentNotes.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "ActionType", scope = ShipmentNotes.class)
    public JAXBElement<String> createShipmentNotesActionType(String value) {
        return new JAXBElement<String>(_ShipmentNotesActionType_QNAME, String.class, ShipmentNotes.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "NoteText", scope = ShipmentNotes.class)
    public JAXBElement<String> createShipmentNotesNoteText(String value) {
        return new JAXBElement<String>(_ShipmentNotesNoteText_QNAME, String.class, ShipmentNotes.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "OldNoteText", scope = ShipmentNotes.class)
    public JAXBElement<String> createShipmentNotesOldNoteText(String value) {
        return new JAXBElement<String>(_ShipmentNotesOldNoteText_QNAME, String.class, ShipmentNotes.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/types/", name = "DeferInventoryUpdates", scope = ProcessShipmentActionAsync.class)
    public JAXBElement<String> createProcessShipmentActionAsyncDeferInventoryUpdates(String value) {
        return new JAXBElement<String>(_ProcessShipmentActionDeferInventoryUpdates_QNAME, String.class, ProcessShipmentActionAsync.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/types/", name = "InitMsgList", scope = ProcessShipmentActionAsync.class)
    public JAXBElement<String> createProcessShipmentActionAsyncInitMsgList(String value) {
        return new JAXBElement<String>(_ProcessShipmentActionInitMsgList_QNAME, String.class, ProcessShipmentActionAsync.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/types/", name = "ActionType", scope = ProcessShipmentActionAsync.class)
    public JAXBElement<String> createProcessShipmentActionAsyncActionType(String value) {
        return new JAXBElement<String>(_ProcessShipmentActionActionType_QNAME, String.class, ProcessShipmentActionAsync.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/types/", name = "ShipConfirmRule", scope = ProcessShipmentActionAsync.class)
    public JAXBElement<String> createProcessShipmentActionAsyncShipConfirmRule(String value) {
        return new JAXBElement<String>(_ProcessShipmentActionShipConfirmRule_QNAME, String.class, ProcessShipmentActionAsync.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/types/", name = "CloseShipment", scope = ProcessShipmentActionAsync.class)
    public JAXBElement<String> createProcessShipmentActionAsyncCloseShipment(String value) {
        return new JAXBElement<String>(_ProcessShipmentActionCloseShipment_QNAME, String.class, ProcessShipmentActionAsync.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/types/", name = "CreateBillOfLading", scope = ProcessShipmentActionAsync.class)
    public JAXBElement<String> createProcessShipmentActionAsyncCreateBillOfLading(String value) {
        return new JAXBElement<String>(_ProcessShipmentActionCreateBillOfLading_QNAME, String.class, ProcessShipmentActionAsync.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/types/", name = "DocumentJobSet", scope = ProcessShipmentActionAsync.class)
    public JAXBElement<String> createProcessShipmentActionAsyncDocumentJobSet(String value) {
        return new JAXBElement<String>(_ProcessShipmentActionDocumentJobSet_QNAME, String.class, ProcessShipmentActionAsync.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/types/", name = "CreateShipmentForStagedQuantity", scope = ProcessShipmentActionAsync.class)
    public JAXBElement<String> createProcessShipmentActionAsyncCreateShipmentForStagedQuantity(String value) {
        return new JAXBElement<String>(_ProcessShipmentActionCreateShipmentForStagedQuantity_QNAME, String.class, ProcessShipmentActionAsync.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/types/", name = "PackingSlipStatus", scope = ProcessShipmentActionAsync.class)
    public JAXBElement<String> createProcessShipmentActionAsyncPackingSlipStatus(String value) {
        return new JAXBElement<String>(_ProcessShipmentActionPackingSlipStatus_QNAME, String.class, ProcessShipmentActionAsync.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/types/", name = "ActualShipDate", scope = ProcessShipmentActionAsync.class)
    public JAXBElement<XMLGregorianCalendar> createProcessShipmentActionAsyncActualShipDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_ProcessShipmentActionActualShipDate_QNAME, XMLGregorianCalendar.class, ProcessShipmentActionAsync.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/types/", name = "OverrideWeightVolume", scope = ProcessShipmentActionAsync.class)
    public JAXBElement<String> createProcessShipmentActionAsyncOverrideWeightVolume(String value) {
        return new JAXBElement<String>(_ProcessShipmentActionOverrideWeightVolume_QNAME, String.class, ProcessShipmentActionAsync.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/types/", name = "OutboundMessageCode", scope = ProcessShipmentActionAsync.class)
    public JAXBElement<String> createProcessShipmentActionAsyncOutboundMessageCode(String value) {
        return new JAXBElement<String>(_ProcessShipmentActionOutboundMessageCode_QNAME, String.class, ProcessShipmentActionAsync.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "AttributeDate1", scope = ShippingCost.class)
    public JAXBElement<XMLGregorianCalendar> createShippingCostAttributeDate1(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_InvSerialsInterfaceAttributeDate1_QNAME, XMLGregorianCalendar.class, ShippingCost.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "ConversionDate", scope = ShippingCost.class)
    public JAXBElement<XMLGregorianCalendar> createShippingCostConversionDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_FreightCostInterfaceConversionDate_QNAME, XMLGregorianCalendar.class, ShippingCost.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "ShippingCostId", scope = ShippingCost.class)
    public JAXBElement<Long> createShippingCostShippingCostId(Long value) {
        return new JAXBElement<Long>(_ShippingCostShippingCostId_QNAME, Long.class, ShippingCost.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "AttributeDate5", scope = ShippingCost.class)
    public JAXBElement<XMLGregorianCalendar> createShippingCostAttributeDate5(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_InvSerialsInterfaceAttributeDate5_QNAME, XMLGregorianCalendar.class, ShippingCost.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "AttributeDate4", scope = ShippingCost.class)
    public JAXBElement<XMLGregorianCalendar> createShippingCostAttributeDate4(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_InvSerialsInterfaceAttributeDate4_QNAME, XMLGregorianCalendar.class, ShippingCost.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "AttributeDate3", scope = ShippingCost.class)
    public JAXBElement<XMLGregorianCalendar> createShippingCostAttributeDate3(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_InvSerialsInterfaceAttributeDate3_QNAME, XMLGregorianCalendar.class, ShippingCost.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "AttributeDate2", scope = ShippingCost.class)
    public JAXBElement<XMLGregorianCalendar> createShippingCostAttributeDate2(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_InvSerialsInterfaceAttributeDate2_QNAME, XMLGregorianCalendar.class, ShippingCost.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "AttributeNumber2", scope = ShippingCost.class)
    public JAXBElement<BigDecimal> createShippingCostAttributeNumber2(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_InvSerialsInterfaceAttributeNumber2_QNAME, BigDecimal.class, ShippingCost.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "AttributeNumber1", scope = ShippingCost.class)
    public JAXBElement<BigDecimal> createShippingCostAttributeNumber1(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_InvSerialsInterfaceAttributeNumber1_QNAME, BigDecimal.class, ShippingCost.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "ConversionRate", scope = ShippingCost.class)
    public JAXBElement<Long> createShippingCostConversionRate(Long value) {
        return new JAXBElement<Long>(_FreightCostInterfaceConversionRate_QNAME, Long.class, ShippingCost.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "AttributeNumber4", scope = ShippingCost.class)
    public JAXBElement<BigDecimal> createShippingCostAttributeNumber4(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_InvSerialsInterfaceAttributeNumber4_QNAME, BigDecimal.class, ShippingCost.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "AttributeNumber3", scope = ShippingCost.class)
    public JAXBElement<BigDecimal> createShippingCostAttributeNumber3(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_InvSerialsInterfaceAttributeNumber3_QNAME, BigDecimal.class, ShippingCost.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "Attribute10", scope = ShippingCost.class)
    public JAXBElement<String> createShippingCostAttribute10(String value) {
        return new JAXBElement<String>(_InvSerialsInterfaceAttribute10_QNAME, String.class, ShippingCost.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "AttributeNumber6", scope = ShippingCost.class)
    public JAXBElement<BigDecimal> createShippingCostAttributeNumber6(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_InvSerialsInterfaceAttributeNumber6_QNAME, BigDecimal.class, ShippingCost.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "AttributeNumber5", scope = ShippingCost.class)
    public JAXBElement<BigDecimal> createShippingCostAttributeNumber5(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_InvSerialsInterfaceAttributeNumber5_QNAME, BigDecimal.class, ShippingCost.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "AttributeNumber8", scope = ShippingCost.class)
    public JAXBElement<BigDecimal> createShippingCostAttributeNumber8(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_InvSerialsInterfaceAttributeNumber8_QNAME, BigDecimal.class, ShippingCost.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "Shipment", scope = ShippingCost.class)
    public JAXBElement<String> createShippingCostShipment(String value) {
        return new JAXBElement<String>(_ShipmentInformationShipment_QNAME, String.class, ShippingCost.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "AttributeNumber7", scope = ShippingCost.class)
    public JAXBElement<BigDecimal> createShippingCostAttributeNumber7(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_InvSerialsInterfaceAttributeNumber7_QNAME, BigDecimal.class, ShippingCost.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "Attribute14", scope = ShippingCost.class)
    public JAXBElement<String> createShippingCostAttribute14(String value) {
        return new JAXBElement<String>(_InvSerialsInterfaceAttribute14_QNAME, String.class, ShippingCost.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "Attribute13", scope = ShippingCost.class)
    public JAXBElement<String> createShippingCostAttribute13(String value) {
        return new JAXBElement<String>(_InvSerialsInterfaceAttribute13_QNAME, String.class, ShippingCost.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "AttributeNumber9", scope = ShippingCost.class)
    public JAXBElement<BigDecimal> createShippingCostAttributeNumber9(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_InvSerialsInterfaceAttributeNumber9_QNAME, BigDecimal.class, ShippingCost.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "Attribute12", scope = ShippingCost.class)
    public JAXBElement<String> createShippingCostAttribute12(String value) {
        return new JAXBElement<String>(_InvSerialsInterfaceAttribute12_QNAME, String.class, ShippingCost.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "Attribute11", scope = ShippingCost.class)
    public JAXBElement<String> createShippingCostAttribute11(String value) {
        return new JAXBElement<String>(_InvSerialsInterfaceAttribute11_QNAME, String.class, ShippingCost.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "AttributeNumber10", scope = ShippingCost.class)
    public JAXBElement<BigDecimal> createShippingCostAttributeNumber10(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_InvSerialsInterfaceAttributeNumber10_QNAME, BigDecimal.class, ShippingCost.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "Attribute18", scope = ShippingCost.class)
    public JAXBElement<String> createShippingCostAttribute18(String value) {
        return new JAXBElement<String>(_InvSerialsInterfaceAttribute18_QNAME, String.class, ShippingCost.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "Attribute17", scope = ShippingCost.class)
    public JAXBElement<String> createShippingCostAttribute17(String value) {
        return new JAXBElement<String>(_InvSerialsInterfaceAttribute17_QNAME, String.class, ShippingCost.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "Attribute16", scope = ShippingCost.class)
    public JAXBElement<String> createShippingCostAttribute16(String value) {
        return new JAXBElement<String>(_InvSerialsInterfaceAttribute16_QNAME, String.class, ShippingCost.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "Attribute15", scope = ShippingCost.class)
    public JAXBElement<String> createShippingCostAttribute15(String value) {
        return new JAXBElement<String>(_InvSerialsInterfaceAttribute15_QNAME, String.class, ShippingCost.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "Attribute19", scope = ShippingCost.class)
    public JAXBElement<String> createShippingCostAttribute19(String value) {
        return new JAXBElement<String>(_InvSerialsInterfaceAttribute19_QNAME, String.class, ShippingCost.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "Attribute1", scope = ShippingCost.class)
    public JAXBElement<String> createShippingCostAttribute1(String value) {
        return new JAXBElement<String>(_InvSerialsInterfaceAttribute1_QNAME, String.class, ShippingCost.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "Attribute20", scope = ShippingCost.class)
    public JAXBElement<String> createShippingCostAttribute20(String value) {
        return new JAXBElement<String>(_InvSerialsInterfaceAttribute20_QNAME, String.class, ShippingCost.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "AttributeCategory", scope = ShippingCost.class)
    public JAXBElement<String> createShippingCostAttributeCategory(String value) {
        return new JAXBElement<String>(_InvSerialsInterfaceAttributeCategory_QNAME, String.class, ShippingCost.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "Attribute3", scope = ShippingCost.class)
    public JAXBElement<String> createShippingCostAttribute3(String value) {
        return new JAXBElement<String>(_InvSerialsInterfaceAttribute3_QNAME, String.class, ShippingCost.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "Attribute2", scope = ShippingCost.class)
    public JAXBElement<String> createShippingCostAttribute2(String value) {
        return new JAXBElement<String>(_InvSerialsInterfaceAttribute2_QNAME, String.class, ShippingCost.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "AttributeTimestamp1", scope = ShippingCost.class)
    public JAXBElement<XMLGregorianCalendar> createShippingCostAttributeTimestamp1(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_InvSerialsInterfaceAttributeTimestamp1_QNAME, XMLGregorianCalendar.class, ShippingCost.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "ConversionRateType", scope = ShippingCost.class)
    public JAXBElement<String> createShippingCostConversionRateType(String value) {
        return new JAXBElement<String>(_ShippingCostConversionRateType_QNAME, String.class, ShippingCost.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "Cost", scope = ShippingCost.class)
    public JAXBElement<String> createShippingCostCost(String value) {
        return new JAXBElement<String>(_ShippingCostCost_QNAME, String.class, ShippingCost.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "Action", scope = ShippingCost.class)
    public JAXBElement<String> createShippingCostAction(String value) {
        return new JAXBElement<String>(_ShippingCostAction_QNAME, String.class, ShippingCost.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "Attribute9", scope = ShippingCost.class)
    public JAXBElement<String> createShippingCostAttribute9(String value) {
        return new JAXBElement<String>(_InvSerialsInterfaceAttribute9_QNAME, String.class, ShippingCost.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "AttributeTimestamp5", scope = ShippingCost.class)
    public JAXBElement<XMLGregorianCalendar> createShippingCostAttributeTimestamp5(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_InvSerialsInterfaceAttributeTimestamp5_QNAME, XMLGregorianCalendar.class, ShippingCost.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "Attribute8", scope = ShippingCost.class)
    public JAXBElement<String> createShippingCostAttribute8(String value) {
        return new JAXBElement<String>(_InvSerialsInterfaceAttribute8_QNAME, String.class, ShippingCost.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "AttributeTimestamp4", scope = ShippingCost.class)
    public JAXBElement<XMLGregorianCalendar> createShippingCostAttributeTimestamp4(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_InvSerialsInterfaceAttributeTimestamp4_QNAME, XMLGregorianCalendar.class, ShippingCost.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "AttributeTimestamp3", scope = ShippingCost.class)
    public JAXBElement<XMLGregorianCalendar> createShippingCostAttributeTimestamp3(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_InvSerialsInterfaceAttributeTimestamp3_QNAME, XMLGregorianCalendar.class, ShippingCost.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "AttributeTimestamp2", scope = ShippingCost.class)
    public JAXBElement<XMLGregorianCalendar> createShippingCostAttributeTimestamp2(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_InvSerialsInterfaceAttributeTimestamp2_QNAME, XMLGregorianCalendar.class, ShippingCost.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "Attribute5", scope = ShippingCost.class)
    public JAXBElement<String> createShippingCostAttribute5(String value) {
        return new JAXBElement<String>(_InvSerialsInterfaceAttribute5_QNAME, String.class, ShippingCost.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "Attribute4", scope = ShippingCost.class)
    public JAXBElement<String> createShippingCostAttribute4(String value) {
        return new JAXBElement<String>(_InvSerialsInterfaceAttribute4_QNAME, String.class, ShippingCost.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "Attribute7", scope = ShippingCost.class)
    public JAXBElement<String> createShippingCostAttribute7(String value) {
        return new JAXBElement<String>(_InvSerialsInterfaceAttribute7_QNAME, String.class, ShippingCost.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", name = "Attribute6", scope = ShippingCost.class)
    public JAXBElement<String> createShippingCostAttribute6(String value) {
        return new JAXBElement<String>(_InvSerialsInterfaceAttribute6_QNAME, String.class, ShippingCost.class, value);
    }

}

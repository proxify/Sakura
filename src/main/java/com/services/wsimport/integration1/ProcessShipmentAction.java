
package com.services.wsimport.integration1;

import java.math.BigDecimal;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="apiVersionNumber" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="InitMsgList" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ActionCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Shipment" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ActionType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CreateBillOfLading" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CreateShipmentForStagedQuantity" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ActualShipDate" type="{http://xmlns.oracle.com/adf/svc/types/}dateTime-Timestamp" minOccurs="0"/>
 *         &lt;element name="CloseShipment" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DeferInventoryUpdates" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ShipConfirmRule" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="OverrideWeightVolume" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="OutboundMessageCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PackingSlipStatus" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DocumentJobSet" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "apiVersionNumber",
    "initMsgList",
    "actionCode",
    "shipment",
    "actionType",
    "createBillOfLading",
    "createShipmentForStagedQuantity",
    "actualShipDate",
    "closeShipment",
    "deferInventoryUpdates",
    "shipConfirmRule",
    "overrideWeightVolume",
    "outboundMessageCode",
    "packingSlipStatus",
    "documentJobSet"
})
@XmlRootElement(name = "processShipmentAction")
public class ProcessShipmentAction {

    @XmlElement(required = true)
    protected BigDecimal apiVersionNumber;
    @XmlElementRef(name = "InitMsgList", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/types/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> initMsgList;
    @XmlElement(name = "ActionCode", required = true)
    protected String actionCode;
    @XmlElement(name = "Shipment", required = true)
    protected String shipment;
    @XmlElementRef(name = "ActionType", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/types/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> actionType;
    @XmlElementRef(name = "CreateBillOfLading", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/types/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> createBillOfLading;
    @XmlElementRef(name = "CreateShipmentForStagedQuantity", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/types/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> createShipmentForStagedQuantity;
    @XmlElementRef(name = "ActualShipDate", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/types/", type = JAXBElement.class, required = false)
    protected JAXBElement<XMLGregorianCalendar> actualShipDate;
    @XmlElementRef(name = "CloseShipment", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/types/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> closeShipment;
    @XmlElementRef(name = "DeferInventoryUpdates", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/types/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> deferInventoryUpdates;
    @XmlElementRef(name = "ShipConfirmRule", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/types/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> shipConfirmRule;
    @XmlElementRef(name = "OverrideWeightVolume", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/types/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> overrideWeightVolume;
    @XmlElementRef(name = "OutboundMessageCode", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/types/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> outboundMessageCode;
    @XmlElementRef(name = "PackingSlipStatus", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/types/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> packingSlipStatus;
    @XmlElementRef(name = "DocumentJobSet", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/types/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> documentJobSet;

    /**
     * Gets the value of the apiVersionNumber property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getApiVersionNumber() {
        return apiVersionNumber;
    }

    /**
     * Sets the value of the apiVersionNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setApiVersionNumber(BigDecimal value) {
        this.apiVersionNumber = value;
    }

    /**
     * Gets the value of the initMsgList property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getInitMsgList() {
        return initMsgList;
    }

    /**
     * Sets the value of the initMsgList property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setInitMsgList(JAXBElement<String> value) {
        this.initMsgList = value;
    }

    /**
     * Gets the value of the actionCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getActionCode() {
        return actionCode;
    }

    /**
     * Sets the value of the actionCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setActionCode(String value) {
        this.actionCode = value;
    }

    /**
     * Gets the value of the shipment property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShipment() {
        return shipment;
    }

    /**
     * Sets the value of the shipment property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShipment(String value) {
        this.shipment = value;
    }

    /**
     * Gets the value of the actionType property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getActionType() {
        return actionType;
    }

    /**
     * Sets the value of the actionType property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setActionType(JAXBElement<String> value) {
        this.actionType = value;
    }

    /**
     * Gets the value of the createBillOfLading property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCreateBillOfLading() {
        return createBillOfLading;
    }

    /**
     * Sets the value of the createBillOfLading property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCreateBillOfLading(JAXBElement<String> value) {
        this.createBillOfLading = value;
    }

    /**
     * Gets the value of the createShipmentForStagedQuantity property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCreateShipmentForStagedQuantity() {
        return createShipmentForStagedQuantity;
    }

    /**
     * Sets the value of the createShipmentForStagedQuantity property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCreateShipmentForStagedQuantity(JAXBElement<String> value) {
        this.createShipmentForStagedQuantity = value;
    }

    /**
     * Gets the value of the actualShipDate property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public JAXBElement<XMLGregorianCalendar> getActualShipDate() {
        return actualShipDate;
    }

    /**
     * Sets the value of the actualShipDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public void setActualShipDate(JAXBElement<XMLGregorianCalendar> value) {
        this.actualShipDate = value;
    }

    /**
     * Gets the value of the closeShipment property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCloseShipment() {
        return closeShipment;
    }

    /**
     * Sets the value of the closeShipment property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCloseShipment(JAXBElement<String> value) {
        this.closeShipment = value;
    }

    /**
     * Gets the value of the deferInventoryUpdates property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDeferInventoryUpdates() {
        return deferInventoryUpdates;
    }

    /**
     * Sets the value of the deferInventoryUpdates property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDeferInventoryUpdates(JAXBElement<String> value) {
        this.deferInventoryUpdates = value;
    }

    /**
     * Gets the value of the shipConfirmRule property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getShipConfirmRule() {
        return shipConfirmRule;
    }

    /**
     * Sets the value of the shipConfirmRule property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setShipConfirmRule(JAXBElement<String> value) {
        this.shipConfirmRule = value;
    }

    /**
     * Gets the value of the overrideWeightVolume property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getOverrideWeightVolume() {
        return overrideWeightVolume;
    }

    /**
     * Sets the value of the overrideWeightVolume property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setOverrideWeightVolume(JAXBElement<String> value) {
        this.overrideWeightVolume = value;
    }

    /**
     * Gets the value of the outboundMessageCode property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getOutboundMessageCode() {
        return outboundMessageCode;
    }

    /**
     * Sets the value of the outboundMessageCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setOutboundMessageCode(JAXBElement<String> value) {
        this.outboundMessageCode = value;
    }

    /**
     * Gets the value of the packingSlipStatus property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPackingSlipStatus() {
        return packingSlipStatus;
    }

    /**
     * Sets the value of the packingSlipStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPackingSlipStatus(JAXBElement<String> value) {
        this.packingSlipStatus = value;
    }

    /**
     * Gets the value of the documentJobSet property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDocumentJobSet() {
        return documentJobSet;
    }

    /**
     * Sets the value of the documentJobSet property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDocumentJobSet(JAXBElement<String> value) {
        this.documentJobSet = value;
    }

}

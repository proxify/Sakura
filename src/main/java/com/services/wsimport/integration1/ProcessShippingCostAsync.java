
package com.services.wsimport.integration1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="shippingCost" type="{http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/}ShippingCost"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "shippingCost"
})
@XmlRootElement(name = "processShippingCostAsync")
public class ProcessShippingCostAsync {

    @XmlElement(required = true)
    protected ShippingCost shippingCost;

    /**
     * Gets the value of the shippingCost property.
     * 
     * @return
     *     possible object is
     *     {@link ShippingCost }
     *     
     */
    public ShippingCost getShippingCost() {
        return shippingCost;
    }

    /**
     * Sets the value of the shippingCost property.
     * 
     * @param value
     *     allowed object is
     *     {@link ShippingCost }
     *     
     */
    public void setShippingCost(ShippingCost value) {
        this.shippingCost = value;
    }

}

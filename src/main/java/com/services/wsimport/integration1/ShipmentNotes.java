
package com.services.wsimport.integration1;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ShipmentNotes complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ShipmentNotes">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="NoteTypeCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ActionType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="NoteText" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="OldNoteText" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ShipmentNotes", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", propOrder = {
    "noteTypeCode",
    "actionType",
    "noteText",
    "oldNoteText"
})
public class ShipmentNotes {

    @XmlElementRef(name = "NoteTypeCode", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> noteTypeCode;
    @XmlElementRef(name = "ActionType", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> actionType;
    @XmlElementRef(name = "NoteText", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> noteText;
    @XmlElementRef(name = "OldNoteText", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> oldNoteText;

    /**
     * Gets the value of the noteTypeCode property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getNoteTypeCode() {
        return noteTypeCode;
    }

    /**
     * Sets the value of the noteTypeCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setNoteTypeCode(JAXBElement<String> value) {
        this.noteTypeCode = value;
    }

    /**
     * Gets the value of the actionType property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getActionType() {
        return actionType;
    }

    /**
     * Sets the value of the actionType property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setActionType(JAXBElement<String> value) {
        this.actionType = value;
    }

    /**
     * Gets the value of the noteText property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getNoteText() {
        return noteText;
    }

    /**
     * Sets the value of the noteText property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setNoteText(JAXBElement<String> value) {
        this.noteText = value;
    }

    /**
     * Gets the value of the oldNoteText property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getOldNoteText() {
        return oldNoteText;
    }

    /**
     * Sets the value of the oldNoteText property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setOldNoteText(JAXBElement<String> value) {
        this.oldNoteText = value;
    }

}

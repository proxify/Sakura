
package com.services.wsimport.integration1;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for ContainerInterface complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ContainerInterface">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="LicensePlateNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TrackingNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MasterSerialNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SealNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ContainerItemName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ContainerVolume" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="ContainerVolumeUOMName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FOBCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="GrossWeight" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="GrossWeightUOMName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TareWeight" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="TareWeightUOMName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PackingInstructions" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ShippingInstructions" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AttributeCategory" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Attribute1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Attribute2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Attribute3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Attribute4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Attribute5" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Attribute6" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Attribute7" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Attribute8" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Attribute9" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Attribute10" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Attribute11" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Attribute12" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Attribute13" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Attribute14" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Attribute15" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Attribute16" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Attribute17" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Attribute18" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Attribute19" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Attribute20" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AttributeDate1" type="{http://xmlns.oracle.com/adf/svc/types/}date-Date" minOccurs="0"/>
 *         &lt;element name="AttributeDate2" type="{http://xmlns.oracle.com/adf/svc/types/}date-Date" minOccurs="0"/>
 *         &lt;element name="AttributeDate3" type="{http://xmlns.oracle.com/adf/svc/types/}date-Date" minOccurs="0"/>
 *         &lt;element name="AttributeDate4" type="{http://xmlns.oracle.com/adf/svc/types/}date-Date" minOccurs="0"/>
 *         &lt;element name="AttributeDate5" type="{http://xmlns.oracle.com/adf/svc/types/}date-Date" minOccurs="0"/>
 *         &lt;element name="AttributeNumber1" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="AttributeNumber10" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="AttributeNumber2" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="AttributeNumber3" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="AttributeNumber4" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="AttributeNumber5" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="AttributeNumber6" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="AttributeNumber7" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="AttributeNumber8" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="AttributeNumber9" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="AttributeTimestamp1" type="{http://xmlns.oracle.com/adf/svc/types/}dateTime-Timestamp" minOccurs="0"/>
 *         &lt;element name="AttributeTimestamp2" type="{http://xmlns.oracle.com/adf/svc/types/}dateTime-Timestamp" minOccurs="0"/>
 *         &lt;element name="AttributeTimestamp3" type="{http://xmlns.oracle.com/adf/svc/types/}dateTime-Timestamp" minOccurs="0"/>
 *         &lt;element name="AttributeTimestamp4" type="{http://xmlns.oracle.com/adf/svc/types/}dateTime-Timestamp" minOccurs="0"/>
 *         &lt;element name="AttributeTimestamp5" type="{http://xmlns.oracle.com/adf/svc/types/}dateTime-Timestamp" minOccurs="0"/>
 *         &lt;element name="PackingUnit" type="{http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/}ContainerInterface" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="ShippingCost" type="{http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/}FreightCostInterface" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="ShipmentLine" type="{http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/}ShipmentLineInterface" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ContainerInterface", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", propOrder = {
    "licensePlateNumber",
    "trackingNumber",
    "masterSerialNumber",
    "sealNumber",
    "containerItemName",
    "containerVolume",
    "containerVolumeUOMName",
    "fobCode",
    "grossWeight",
    "grossWeightUOMName",
    "tareWeight",
    "tareWeightUOMName",
    "packingInstructions",
    "shippingInstructions",
    "attributeCategory",
    "attribute1",
    "attribute2",
    "attribute3",
    "attribute4",
    "attribute5",
    "attribute6",
    "attribute7",
    "attribute8",
    "attribute9",
    "attribute10",
    "attribute11",
    "attribute12",
    "attribute13",
    "attribute14",
    "attribute15",
    "attribute16",
    "attribute17",
    "attribute18",
    "attribute19",
    "attribute20",
    "attributeDate1",
    "attributeDate2",
    "attributeDate3",
    "attributeDate4",
    "attributeDate5",
    "attributeNumber1",
    "attributeNumber10",
    "attributeNumber2",
    "attributeNumber3",
    "attributeNumber4",
    "attributeNumber5",
    "attributeNumber6",
    "attributeNumber7",
    "attributeNumber8",
    "attributeNumber9",
    "attributeTimestamp1",
    "attributeTimestamp2",
    "attributeTimestamp3",
    "attributeTimestamp4",
    "attributeTimestamp5",
    "packingUnit",
    "shippingCost",
    "shipmentLine"
})
public class ContainerInterface {

    @XmlElementRef(name = "LicensePlateNumber", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> licensePlateNumber;
    @XmlElementRef(name = "TrackingNumber", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> trackingNumber;
    @XmlElementRef(name = "MasterSerialNumber", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> masterSerialNumber;
    @XmlElementRef(name = "SealNumber", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> sealNumber;
    @XmlElementRef(name = "ContainerItemName", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> containerItemName;
    @XmlElementRef(name = "ContainerVolume", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<BigDecimal> containerVolume;
    @XmlElementRef(name = "ContainerVolumeUOMName", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> containerVolumeUOMName;
    @XmlElementRef(name = "FOBCode", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> fobCode;
    @XmlElementRef(name = "GrossWeight", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<BigDecimal> grossWeight;
    @XmlElementRef(name = "GrossWeightUOMName", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> grossWeightUOMName;
    @XmlElementRef(name = "TareWeight", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<BigDecimal> tareWeight;
    @XmlElementRef(name = "TareWeightUOMName", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> tareWeightUOMName;
    @XmlElementRef(name = "PackingInstructions", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> packingInstructions;
    @XmlElementRef(name = "ShippingInstructions", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> shippingInstructions;
    @XmlElementRef(name = "AttributeCategory", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> attributeCategory;
    @XmlElementRef(name = "Attribute1", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> attribute1;
    @XmlElementRef(name = "Attribute2", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> attribute2;
    @XmlElementRef(name = "Attribute3", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> attribute3;
    @XmlElementRef(name = "Attribute4", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> attribute4;
    @XmlElementRef(name = "Attribute5", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> attribute5;
    @XmlElementRef(name = "Attribute6", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> attribute6;
    @XmlElementRef(name = "Attribute7", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> attribute7;
    @XmlElementRef(name = "Attribute8", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> attribute8;
    @XmlElementRef(name = "Attribute9", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> attribute9;
    @XmlElementRef(name = "Attribute10", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> attribute10;
    @XmlElementRef(name = "Attribute11", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> attribute11;
    @XmlElementRef(name = "Attribute12", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> attribute12;
    @XmlElementRef(name = "Attribute13", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> attribute13;
    @XmlElementRef(name = "Attribute14", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> attribute14;
    @XmlElementRef(name = "Attribute15", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> attribute15;
    @XmlElementRef(name = "Attribute16", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> attribute16;
    @XmlElementRef(name = "Attribute17", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> attribute17;
    @XmlElementRef(name = "Attribute18", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> attribute18;
    @XmlElementRef(name = "Attribute19", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> attribute19;
    @XmlElementRef(name = "Attribute20", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> attribute20;
    @XmlElementRef(name = "AttributeDate1", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<XMLGregorianCalendar> attributeDate1;
    @XmlElementRef(name = "AttributeDate2", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<XMLGregorianCalendar> attributeDate2;
    @XmlElementRef(name = "AttributeDate3", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<XMLGregorianCalendar> attributeDate3;
    @XmlElementRef(name = "AttributeDate4", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<XMLGregorianCalendar> attributeDate4;
    @XmlElementRef(name = "AttributeDate5", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<XMLGregorianCalendar> attributeDate5;
    @XmlElementRef(name = "AttributeNumber1", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<BigDecimal> attributeNumber1;
    @XmlElementRef(name = "AttributeNumber10", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<BigDecimal> attributeNumber10;
    @XmlElementRef(name = "AttributeNumber2", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<BigDecimal> attributeNumber2;
    @XmlElementRef(name = "AttributeNumber3", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<BigDecimal> attributeNumber3;
    @XmlElementRef(name = "AttributeNumber4", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<BigDecimal> attributeNumber4;
    @XmlElementRef(name = "AttributeNumber5", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<BigDecimal> attributeNumber5;
    @XmlElementRef(name = "AttributeNumber6", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<BigDecimal> attributeNumber6;
    @XmlElementRef(name = "AttributeNumber7", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<BigDecimal> attributeNumber7;
    @XmlElementRef(name = "AttributeNumber8", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<BigDecimal> attributeNumber8;
    @XmlElementRef(name = "AttributeNumber9", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<BigDecimal> attributeNumber9;
    @XmlElementRef(name = "AttributeTimestamp1", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<XMLGregorianCalendar> attributeTimestamp1;
    @XmlElementRef(name = "AttributeTimestamp2", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<XMLGregorianCalendar> attributeTimestamp2;
    @XmlElementRef(name = "AttributeTimestamp3", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<XMLGregorianCalendar> attributeTimestamp3;
    @XmlElementRef(name = "AttributeTimestamp4", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<XMLGregorianCalendar> attributeTimestamp4;
    @XmlElementRef(name = "AttributeTimestamp5", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<XMLGregorianCalendar> attributeTimestamp5;
    @XmlElement(name = "PackingUnit")
    protected List<ContainerInterface> packingUnit;
    @XmlElement(name = "ShippingCost")
    protected List<FreightCostInterface> shippingCost;
    @XmlElement(name = "ShipmentLine")
    protected List<ShipmentLineInterface> shipmentLine;

    /**
     * Gets the value of the licensePlateNumber property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getLicensePlateNumber() {
        return licensePlateNumber;
    }

    /**
     * Sets the value of the licensePlateNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setLicensePlateNumber(JAXBElement<String> value) {
        this.licensePlateNumber = value;
    }

    /**
     * Gets the value of the trackingNumber property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getTrackingNumber() {
        return trackingNumber;
    }

    /**
     * Sets the value of the trackingNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setTrackingNumber(JAXBElement<String> value) {
        this.trackingNumber = value;
    }

    /**
     * Gets the value of the masterSerialNumber property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getMasterSerialNumber() {
        return masterSerialNumber;
    }

    /**
     * Sets the value of the masterSerialNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setMasterSerialNumber(JAXBElement<String> value) {
        this.masterSerialNumber = value;
    }

    /**
     * Gets the value of the sealNumber property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getSealNumber() {
        return sealNumber;
    }

    /**
     * Sets the value of the sealNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setSealNumber(JAXBElement<String> value) {
        this.sealNumber = value;
    }

    /**
     * Gets the value of the containerItemName property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getContainerItemName() {
        return containerItemName;
    }

    /**
     * Sets the value of the containerItemName property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setContainerItemName(JAXBElement<String> value) {
        this.containerItemName = value;
    }

    /**
     * Gets the value of the containerVolume property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}
     *     
     */
    public JAXBElement<BigDecimal> getContainerVolume() {
        return containerVolume;
    }

    /**
     * Sets the value of the containerVolume property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}
     *     
     */
    public void setContainerVolume(JAXBElement<BigDecimal> value) {
        this.containerVolume = value;
    }

    /**
     * Gets the value of the containerVolumeUOMName property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getContainerVolumeUOMName() {
        return containerVolumeUOMName;
    }

    /**
     * Sets the value of the containerVolumeUOMName property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setContainerVolumeUOMName(JAXBElement<String> value) {
        this.containerVolumeUOMName = value;
    }

    /**
     * Gets the value of the fobCode property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getFOBCode() {
        return fobCode;
    }

    /**
     * Sets the value of the fobCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setFOBCode(JAXBElement<String> value) {
        this.fobCode = value;
    }

    /**
     * Gets the value of the grossWeight property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}
     *     
     */
    public JAXBElement<BigDecimal> getGrossWeight() {
        return grossWeight;
    }

    /**
     * Sets the value of the grossWeight property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}
     *     
     */
    public void setGrossWeight(JAXBElement<BigDecimal> value) {
        this.grossWeight = value;
    }

    /**
     * Gets the value of the grossWeightUOMName property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getGrossWeightUOMName() {
        return grossWeightUOMName;
    }

    /**
     * Sets the value of the grossWeightUOMName property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setGrossWeightUOMName(JAXBElement<String> value) {
        this.grossWeightUOMName = value;
    }

    /**
     * Gets the value of the tareWeight property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}
     *     
     */
    public JAXBElement<BigDecimal> getTareWeight() {
        return tareWeight;
    }

    /**
     * Sets the value of the tareWeight property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}
     *     
     */
    public void setTareWeight(JAXBElement<BigDecimal> value) {
        this.tareWeight = value;
    }

    /**
     * Gets the value of the tareWeightUOMName property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getTareWeightUOMName() {
        return tareWeightUOMName;
    }

    /**
     * Sets the value of the tareWeightUOMName property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setTareWeightUOMName(JAXBElement<String> value) {
        this.tareWeightUOMName = value;
    }

    /**
     * Gets the value of the packingInstructions property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPackingInstructions() {
        return packingInstructions;
    }

    /**
     * Sets the value of the packingInstructions property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPackingInstructions(JAXBElement<String> value) {
        this.packingInstructions = value;
    }

    /**
     * Gets the value of the shippingInstructions property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getShippingInstructions() {
        return shippingInstructions;
    }

    /**
     * Sets the value of the shippingInstructions property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setShippingInstructions(JAXBElement<String> value) {
        this.shippingInstructions = value;
    }

    /**
     * Gets the value of the attributeCategory property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getAttributeCategory() {
        return attributeCategory;
    }

    /**
     * Sets the value of the attributeCategory property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setAttributeCategory(JAXBElement<String> value) {
        this.attributeCategory = value;
    }

    /**
     * Gets the value of the attribute1 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getAttribute1() {
        return attribute1;
    }

    /**
     * Sets the value of the attribute1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setAttribute1(JAXBElement<String> value) {
        this.attribute1 = value;
    }

    /**
     * Gets the value of the attribute2 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getAttribute2() {
        return attribute2;
    }

    /**
     * Sets the value of the attribute2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setAttribute2(JAXBElement<String> value) {
        this.attribute2 = value;
    }

    /**
     * Gets the value of the attribute3 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getAttribute3() {
        return attribute3;
    }

    /**
     * Sets the value of the attribute3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setAttribute3(JAXBElement<String> value) {
        this.attribute3 = value;
    }

    /**
     * Gets the value of the attribute4 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getAttribute4() {
        return attribute4;
    }

    /**
     * Sets the value of the attribute4 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setAttribute4(JAXBElement<String> value) {
        this.attribute4 = value;
    }

    /**
     * Gets the value of the attribute5 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getAttribute5() {
        return attribute5;
    }

    /**
     * Sets the value of the attribute5 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setAttribute5(JAXBElement<String> value) {
        this.attribute5 = value;
    }

    /**
     * Gets the value of the attribute6 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getAttribute6() {
        return attribute6;
    }

    /**
     * Sets the value of the attribute6 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setAttribute6(JAXBElement<String> value) {
        this.attribute6 = value;
    }

    /**
     * Gets the value of the attribute7 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getAttribute7() {
        return attribute7;
    }

    /**
     * Sets the value of the attribute7 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setAttribute7(JAXBElement<String> value) {
        this.attribute7 = value;
    }

    /**
     * Gets the value of the attribute8 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getAttribute8() {
        return attribute8;
    }

    /**
     * Sets the value of the attribute8 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setAttribute8(JAXBElement<String> value) {
        this.attribute8 = value;
    }

    /**
     * Gets the value of the attribute9 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getAttribute9() {
        return attribute9;
    }

    /**
     * Sets the value of the attribute9 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setAttribute9(JAXBElement<String> value) {
        this.attribute9 = value;
    }

    /**
     * Gets the value of the attribute10 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getAttribute10() {
        return attribute10;
    }

    /**
     * Sets the value of the attribute10 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setAttribute10(JAXBElement<String> value) {
        this.attribute10 = value;
    }

    /**
     * Gets the value of the attribute11 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getAttribute11() {
        return attribute11;
    }

    /**
     * Sets the value of the attribute11 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setAttribute11(JAXBElement<String> value) {
        this.attribute11 = value;
    }

    /**
     * Gets the value of the attribute12 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getAttribute12() {
        return attribute12;
    }

    /**
     * Sets the value of the attribute12 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setAttribute12(JAXBElement<String> value) {
        this.attribute12 = value;
    }

    /**
     * Gets the value of the attribute13 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getAttribute13() {
        return attribute13;
    }

    /**
     * Sets the value of the attribute13 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setAttribute13(JAXBElement<String> value) {
        this.attribute13 = value;
    }

    /**
     * Gets the value of the attribute14 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getAttribute14() {
        return attribute14;
    }

    /**
     * Sets the value of the attribute14 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setAttribute14(JAXBElement<String> value) {
        this.attribute14 = value;
    }

    /**
     * Gets the value of the attribute15 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getAttribute15() {
        return attribute15;
    }

    /**
     * Sets the value of the attribute15 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setAttribute15(JAXBElement<String> value) {
        this.attribute15 = value;
    }

    /**
     * Gets the value of the attribute16 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getAttribute16() {
        return attribute16;
    }

    /**
     * Sets the value of the attribute16 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setAttribute16(JAXBElement<String> value) {
        this.attribute16 = value;
    }

    /**
     * Gets the value of the attribute17 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getAttribute17() {
        return attribute17;
    }

    /**
     * Sets the value of the attribute17 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setAttribute17(JAXBElement<String> value) {
        this.attribute17 = value;
    }

    /**
     * Gets the value of the attribute18 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getAttribute18() {
        return attribute18;
    }

    /**
     * Sets the value of the attribute18 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setAttribute18(JAXBElement<String> value) {
        this.attribute18 = value;
    }

    /**
     * Gets the value of the attribute19 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getAttribute19() {
        return attribute19;
    }

    /**
     * Sets the value of the attribute19 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setAttribute19(JAXBElement<String> value) {
        this.attribute19 = value;
    }

    /**
     * Gets the value of the attribute20 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getAttribute20() {
        return attribute20;
    }

    /**
     * Sets the value of the attribute20 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setAttribute20(JAXBElement<String> value) {
        this.attribute20 = value;
    }

    /**
     * Gets the value of the attributeDate1 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public JAXBElement<XMLGregorianCalendar> getAttributeDate1() {
        return attributeDate1;
    }

    /**
     * Sets the value of the attributeDate1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public void setAttributeDate1(JAXBElement<XMLGregorianCalendar> value) {
        this.attributeDate1 = value;
    }

    /**
     * Gets the value of the attributeDate2 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public JAXBElement<XMLGregorianCalendar> getAttributeDate2() {
        return attributeDate2;
    }

    /**
     * Sets the value of the attributeDate2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public void setAttributeDate2(JAXBElement<XMLGregorianCalendar> value) {
        this.attributeDate2 = value;
    }

    /**
     * Gets the value of the attributeDate3 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public JAXBElement<XMLGregorianCalendar> getAttributeDate3() {
        return attributeDate3;
    }

    /**
     * Sets the value of the attributeDate3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public void setAttributeDate3(JAXBElement<XMLGregorianCalendar> value) {
        this.attributeDate3 = value;
    }

    /**
     * Gets the value of the attributeDate4 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public JAXBElement<XMLGregorianCalendar> getAttributeDate4() {
        return attributeDate4;
    }

    /**
     * Sets the value of the attributeDate4 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public void setAttributeDate4(JAXBElement<XMLGregorianCalendar> value) {
        this.attributeDate4 = value;
    }

    /**
     * Gets the value of the attributeDate5 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public JAXBElement<XMLGregorianCalendar> getAttributeDate5() {
        return attributeDate5;
    }

    /**
     * Sets the value of the attributeDate5 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public void setAttributeDate5(JAXBElement<XMLGregorianCalendar> value) {
        this.attributeDate5 = value;
    }

    /**
     * Gets the value of the attributeNumber1 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}
     *     
     */
    public JAXBElement<BigDecimal> getAttributeNumber1() {
        return attributeNumber1;
    }

    /**
     * Sets the value of the attributeNumber1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}
     *     
     */
    public void setAttributeNumber1(JAXBElement<BigDecimal> value) {
        this.attributeNumber1 = value;
    }

    /**
     * Gets the value of the attributeNumber10 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}
     *     
     */
    public JAXBElement<BigDecimal> getAttributeNumber10() {
        return attributeNumber10;
    }

    /**
     * Sets the value of the attributeNumber10 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}
     *     
     */
    public void setAttributeNumber10(JAXBElement<BigDecimal> value) {
        this.attributeNumber10 = value;
    }

    /**
     * Gets the value of the attributeNumber2 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}
     *     
     */
    public JAXBElement<BigDecimal> getAttributeNumber2() {
        return attributeNumber2;
    }

    /**
     * Sets the value of the attributeNumber2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}
     *     
     */
    public void setAttributeNumber2(JAXBElement<BigDecimal> value) {
        this.attributeNumber2 = value;
    }

    /**
     * Gets the value of the attributeNumber3 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}
     *     
     */
    public JAXBElement<BigDecimal> getAttributeNumber3() {
        return attributeNumber3;
    }

    /**
     * Sets the value of the attributeNumber3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}
     *     
     */
    public void setAttributeNumber3(JAXBElement<BigDecimal> value) {
        this.attributeNumber3 = value;
    }

    /**
     * Gets the value of the attributeNumber4 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}
     *     
     */
    public JAXBElement<BigDecimal> getAttributeNumber4() {
        return attributeNumber4;
    }

    /**
     * Sets the value of the attributeNumber4 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}
     *     
     */
    public void setAttributeNumber4(JAXBElement<BigDecimal> value) {
        this.attributeNumber4 = value;
    }

    /**
     * Gets the value of the attributeNumber5 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}
     *     
     */
    public JAXBElement<BigDecimal> getAttributeNumber5() {
        return attributeNumber5;
    }

    /**
     * Sets the value of the attributeNumber5 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}
     *     
     */
    public void setAttributeNumber5(JAXBElement<BigDecimal> value) {
        this.attributeNumber5 = value;
    }

    /**
     * Gets the value of the attributeNumber6 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}
     *     
     */
    public JAXBElement<BigDecimal> getAttributeNumber6() {
        return attributeNumber6;
    }

    /**
     * Sets the value of the attributeNumber6 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}
     *     
     */
    public void setAttributeNumber6(JAXBElement<BigDecimal> value) {
        this.attributeNumber6 = value;
    }

    /**
     * Gets the value of the attributeNumber7 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}
     *     
     */
    public JAXBElement<BigDecimal> getAttributeNumber7() {
        return attributeNumber7;
    }

    /**
     * Sets the value of the attributeNumber7 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}
     *     
     */
    public void setAttributeNumber7(JAXBElement<BigDecimal> value) {
        this.attributeNumber7 = value;
    }

    /**
     * Gets the value of the attributeNumber8 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}
     *     
     */
    public JAXBElement<BigDecimal> getAttributeNumber8() {
        return attributeNumber8;
    }

    /**
     * Sets the value of the attributeNumber8 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}
     *     
     */
    public void setAttributeNumber8(JAXBElement<BigDecimal> value) {
        this.attributeNumber8 = value;
    }

    /**
     * Gets the value of the attributeNumber9 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}
     *     
     */
    public JAXBElement<BigDecimal> getAttributeNumber9() {
        return attributeNumber9;
    }

    /**
     * Sets the value of the attributeNumber9 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}
     *     
     */
    public void setAttributeNumber9(JAXBElement<BigDecimal> value) {
        this.attributeNumber9 = value;
    }

    /**
     * Gets the value of the attributeTimestamp1 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public JAXBElement<XMLGregorianCalendar> getAttributeTimestamp1() {
        return attributeTimestamp1;
    }

    /**
     * Sets the value of the attributeTimestamp1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public void setAttributeTimestamp1(JAXBElement<XMLGregorianCalendar> value) {
        this.attributeTimestamp1 = value;
    }

    /**
     * Gets the value of the attributeTimestamp2 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public JAXBElement<XMLGregorianCalendar> getAttributeTimestamp2() {
        return attributeTimestamp2;
    }

    /**
     * Sets the value of the attributeTimestamp2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public void setAttributeTimestamp2(JAXBElement<XMLGregorianCalendar> value) {
        this.attributeTimestamp2 = value;
    }

    /**
     * Gets the value of the attributeTimestamp3 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public JAXBElement<XMLGregorianCalendar> getAttributeTimestamp3() {
        return attributeTimestamp3;
    }

    /**
     * Sets the value of the attributeTimestamp3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public void setAttributeTimestamp3(JAXBElement<XMLGregorianCalendar> value) {
        this.attributeTimestamp3 = value;
    }

    /**
     * Gets the value of the attributeTimestamp4 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public JAXBElement<XMLGregorianCalendar> getAttributeTimestamp4() {
        return attributeTimestamp4;
    }

    /**
     * Sets the value of the attributeTimestamp4 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public void setAttributeTimestamp4(JAXBElement<XMLGregorianCalendar> value) {
        this.attributeTimestamp4 = value;
    }

    /**
     * Gets the value of the attributeTimestamp5 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public JAXBElement<XMLGregorianCalendar> getAttributeTimestamp5() {
        return attributeTimestamp5;
    }

    /**
     * Sets the value of the attributeTimestamp5 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public void setAttributeTimestamp5(JAXBElement<XMLGregorianCalendar> value) {
        this.attributeTimestamp5 = value;
    }

    /**
     * Gets the value of the packingUnit property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the packingUnit property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPackingUnit().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ContainerInterface }
     * 
     * 
     */
    public List<ContainerInterface> getPackingUnit() {
        if (packingUnit == null) {
            packingUnit = new ArrayList<ContainerInterface>();
        }
        return this.packingUnit;
    }

    /**
     * Gets the value of the shippingCost property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the shippingCost property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getShippingCost().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link FreightCostInterface }
     * 
     * 
     */
    public List<FreightCostInterface> getShippingCost() {
        if (shippingCost == null) {
            shippingCost = new ArrayList<FreightCostInterface>();
        }
        return this.shippingCost;
    }

    /**
     * Gets the value of the shipmentLine property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the shipmentLine property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getShipmentLine().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ShipmentLineInterface }
     * 
     * 
     */
    public List<ShipmentLineInterface> getShipmentLine() {
        if (shipmentLine == null) {
            shipmentLine = new ArrayList<ShipmentLineInterface>();
        }
        return this.shipmentLine;
    }

}

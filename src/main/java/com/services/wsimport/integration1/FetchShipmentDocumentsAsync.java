
package com.services.wsimport.integration1;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Shipment" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FromCreationDate" type="{http://xmlns.oracle.com/adf/svc/types/}dateTime-Timestamp" minOccurs="0"/>
 *         &lt;element name="ToCreationDate" type="{http://xmlns.oracle.com/adf/svc/types/}dateTime-Timestamp" minOccurs="0"/>
 *         &lt;element name="FromConfirmedDate" type="{http://xmlns.oracle.com/adf/svc/types/}dateTime-Timestamp" minOccurs="0"/>
 *         &lt;element name="ToConfirmedDate" type="{http://xmlns.oracle.com/adf/svc/types/}dateTime-Timestamp" minOccurs="0"/>
 *         &lt;element name="FromActualShipDate" type="{http://xmlns.oracle.com/adf/svc/types/}dateTime-Timestamp" minOccurs="0"/>
 *         &lt;element name="ToActualShipDate" type="{http://xmlns.oracle.com/adf/svc/types/}dateTime-Timestamp" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "shipment",
    "fromCreationDate",
    "toCreationDate",
    "fromConfirmedDate",
    "toConfirmedDate",
    "fromActualShipDate",
    "toActualShipDate"
})
@XmlRootElement(name = "fetchShipmentDocumentsAsync")
public class FetchShipmentDocumentsAsync {

    @XmlElementRef(name = "Shipment", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/types/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> shipment;
    @XmlElementRef(name = "FromCreationDate", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/types/", type = JAXBElement.class, required = false)
    protected JAXBElement<XMLGregorianCalendar> fromCreationDate;
    @XmlElementRef(name = "ToCreationDate", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/types/", type = JAXBElement.class, required = false)
    protected JAXBElement<XMLGregorianCalendar> toCreationDate;
    @XmlElementRef(name = "FromConfirmedDate", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/types/", type = JAXBElement.class, required = false)
    protected JAXBElement<XMLGregorianCalendar> fromConfirmedDate;
    @XmlElementRef(name = "ToConfirmedDate", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/types/", type = JAXBElement.class, required = false)
    protected JAXBElement<XMLGregorianCalendar> toConfirmedDate;
    @XmlElementRef(name = "FromActualShipDate", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/types/", type = JAXBElement.class, required = false)
    protected JAXBElement<XMLGregorianCalendar> fromActualShipDate;
    @XmlElementRef(name = "ToActualShipDate", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/types/", type = JAXBElement.class, required = false)
    protected JAXBElement<XMLGregorianCalendar> toActualShipDate;

    /**
     * Gets the value of the shipment property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getShipment() {
        return shipment;
    }

    /**
     * Sets the value of the shipment property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setShipment(JAXBElement<String> value) {
        this.shipment = value;
    }

    /**
     * Gets the value of the fromCreationDate property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public JAXBElement<XMLGregorianCalendar> getFromCreationDate() {
        return fromCreationDate;
    }

    /**
     * Sets the value of the fromCreationDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public void setFromCreationDate(JAXBElement<XMLGregorianCalendar> value) {
        this.fromCreationDate = value;
    }

    /**
     * Gets the value of the toCreationDate property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public JAXBElement<XMLGregorianCalendar> getToCreationDate() {
        return toCreationDate;
    }

    /**
     * Sets the value of the toCreationDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public void setToCreationDate(JAXBElement<XMLGregorianCalendar> value) {
        this.toCreationDate = value;
    }

    /**
     * Gets the value of the fromConfirmedDate property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public JAXBElement<XMLGregorianCalendar> getFromConfirmedDate() {
        return fromConfirmedDate;
    }

    /**
     * Sets the value of the fromConfirmedDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public void setFromConfirmedDate(JAXBElement<XMLGregorianCalendar> value) {
        this.fromConfirmedDate = value;
    }

    /**
     * Gets the value of the toConfirmedDate property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public JAXBElement<XMLGregorianCalendar> getToConfirmedDate() {
        return toConfirmedDate;
    }

    /**
     * Sets the value of the toConfirmedDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public void setToConfirmedDate(JAXBElement<XMLGregorianCalendar> value) {
        this.toConfirmedDate = value;
    }

    /**
     * Gets the value of the fromActualShipDate property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public JAXBElement<XMLGregorianCalendar> getFromActualShipDate() {
        return fromActualShipDate;
    }

    /**
     * Sets the value of the fromActualShipDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public void setFromActualShipDate(JAXBElement<XMLGregorianCalendar> value) {
        this.fromActualShipDate = value;
    }

    /**
     * Gets the value of the toActualShipDate property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public JAXBElement<XMLGregorianCalendar> getToActualShipDate() {
        return toActualShipDate;
    }

    /**
     * Sets the value of the toActualShipDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public void setToActualShipDate(JAXBElement<XMLGregorianCalendar> value) {
        this.toActualShipDate = value;
    }

}

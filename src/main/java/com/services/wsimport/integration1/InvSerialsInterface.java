
package com.services.wsimport.integration1;

import java.math.BigDecimal;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for InvSerialsInterface complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="InvSerialsInterface">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="FromSerialNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ToSerialNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SubinventoryCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Locator" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Attribute1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Attribute10" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Attribute11" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Attribute12" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Attribute13" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Attribute14" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Attribute15" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Attribute16" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Attribute17" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Attribute18" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Attribute19" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Attribute2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Attribute20" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Attribute3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Attribute4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Attribute5" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Attribute6" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Attribute7" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Attribute8" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Attribute9" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AttributeCategory" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AttributeDate1" type="{http://xmlns.oracle.com/adf/svc/types/}date-Date" minOccurs="0"/>
 *         &lt;element name="AttributeDate2" type="{http://xmlns.oracle.com/adf/svc/types/}date-Date" minOccurs="0"/>
 *         &lt;element name="AttributeDate3" type="{http://xmlns.oracle.com/adf/svc/types/}date-Date" minOccurs="0"/>
 *         &lt;element name="AttributeDate4" type="{http://xmlns.oracle.com/adf/svc/types/}date-Date" minOccurs="0"/>
 *         &lt;element name="AttributeDate5" type="{http://xmlns.oracle.com/adf/svc/types/}date-Date" minOccurs="0"/>
 *         &lt;element name="AttributeNumber1" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="AttributeNumber10" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="AttributeNumber2" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="AttributeNumber3" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="AttributeNumber4" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="AttributeNumber5" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="AttributeNumber6" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="AttributeNumber7" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="AttributeNumber8" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="AttributeNumber9" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="AttributeTimestamp1" type="{http://xmlns.oracle.com/adf/svc/types/}dateTime-Timestamp" minOccurs="0"/>
 *         &lt;element name="AttributeTimestamp2" type="{http://xmlns.oracle.com/adf/svc/types/}dateTime-Timestamp" minOccurs="0"/>
 *         &lt;element name="AttributeTimestamp3" type="{http://xmlns.oracle.com/adf/svc/types/}dateTime-Timestamp" minOccurs="0"/>
 *         &lt;element name="AttributeTimestamp4" type="{http://xmlns.oracle.com/adf/svc/types/}dateTime-Timestamp" minOccurs="0"/>
 *         &lt;element name="AttributeTimestamp5" type="{http://xmlns.oracle.com/adf/svc/types/}dateTime-Timestamp" minOccurs="0"/>
 *         &lt;element name="CAttribute1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CAttribute10" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CAttribute11" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CAttribute12" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CAttribute13" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CAttribute14" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CAttribute15" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CAttribute16" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CAttribute17" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CAttribute18" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CAttribute19" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CAttribute2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CAttribute20" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CAttribute3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CAttribute4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CAttribute5" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CAttribute6" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CAttribute7" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CAttribute8" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CAttribute9" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DAttribute1" type="{http://xmlns.oracle.com/adf/svc/types/}date-Date" minOccurs="0"/>
 *         &lt;element name="DAttribute10" type="{http://xmlns.oracle.com/adf/svc/types/}date-Date" minOccurs="0"/>
 *         &lt;element name="DAttribute2" type="{http://xmlns.oracle.com/adf/svc/types/}date-Date" minOccurs="0"/>
 *         &lt;element name="DAttribute3" type="{http://xmlns.oracle.com/adf/svc/types/}date-Date" minOccurs="0"/>
 *         &lt;element name="DAttribute4" type="{http://xmlns.oracle.com/adf/svc/types/}date-Date" minOccurs="0"/>
 *         &lt;element name="DAttribute5" type="{http://xmlns.oracle.com/adf/svc/types/}date-Date" minOccurs="0"/>
 *         &lt;element name="DAttribute6" type="{http://xmlns.oracle.com/adf/svc/types/}date-Date" minOccurs="0"/>
 *         &lt;element name="DAttribute7" type="{http://xmlns.oracle.com/adf/svc/types/}date-Date" minOccurs="0"/>
 *         &lt;element name="DAttribute8" type="{http://xmlns.oracle.com/adf/svc/types/}date-Date" minOccurs="0"/>
 *         &lt;element name="DAttribute9" type="{http://xmlns.oracle.com/adf/svc/types/}date-Date" minOccurs="0"/>
 *         &lt;element name="NAttribute1" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="NAttribute10" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="NAttribute2" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="NAttribute3" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="NAttribute4" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="NAttribute5" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="NAttribute6" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="NAttribute7" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="NAttribute8" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="NAttribute9" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="SerialAttributeCategory" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TAttribute1" type="{http://xmlns.oracle.com/adf/svc/types/}dateTime-Timestamp" minOccurs="0"/>
 *         &lt;element name="TAttribute2" type="{http://xmlns.oracle.com/adf/svc/types/}dateTime-Timestamp" minOccurs="0"/>
 *         &lt;element name="TAttribute3" type="{http://xmlns.oracle.com/adf/svc/types/}dateTime-Timestamp" minOccurs="0"/>
 *         &lt;element name="TAttribute4" type="{http://xmlns.oracle.com/adf/svc/types/}dateTime-Timestamp" minOccurs="0"/>
 *         &lt;element name="TAttribute5" type="{http://xmlns.oracle.com/adf/svc/types/}dateTime-Timestamp" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "InvSerialsInterface", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", propOrder = {
    "fromSerialNumber",
    "toSerialNumber",
    "subinventoryCode",
    "locator",
    "attribute1",
    "attribute10",
    "attribute11",
    "attribute12",
    "attribute13",
    "attribute14",
    "attribute15",
    "attribute16",
    "attribute17",
    "attribute18",
    "attribute19",
    "attribute2",
    "attribute20",
    "attribute3",
    "attribute4",
    "attribute5",
    "attribute6",
    "attribute7",
    "attribute8",
    "attribute9",
    "attributeCategory",
    "attributeDate1",
    "attributeDate2",
    "attributeDate3",
    "attributeDate4",
    "attributeDate5",
    "attributeNumber1",
    "attributeNumber10",
    "attributeNumber2",
    "attributeNumber3",
    "attributeNumber4",
    "attributeNumber5",
    "attributeNumber6",
    "attributeNumber7",
    "attributeNumber8",
    "attributeNumber9",
    "attributeTimestamp1",
    "attributeTimestamp2",
    "attributeTimestamp3",
    "attributeTimestamp4",
    "attributeTimestamp5",
    "cAttribute1",
    "cAttribute10",
    "cAttribute11",
    "cAttribute12",
    "cAttribute13",
    "cAttribute14",
    "cAttribute15",
    "cAttribute16",
    "cAttribute17",
    "cAttribute18",
    "cAttribute19",
    "cAttribute2",
    "cAttribute20",
    "cAttribute3",
    "cAttribute4",
    "cAttribute5",
    "cAttribute6",
    "cAttribute7",
    "cAttribute8",
    "cAttribute9",
    "dAttribute1",
    "dAttribute10",
    "dAttribute2",
    "dAttribute3",
    "dAttribute4",
    "dAttribute5",
    "dAttribute6",
    "dAttribute7",
    "dAttribute8",
    "dAttribute9",
    "nAttribute1",
    "nAttribute10",
    "nAttribute2",
    "nAttribute3",
    "nAttribute4",
    "nAttribute5",
    "nAttribute6",
    "nAttribute7",
    "nAttribute8",
    "nAttribute9",
    "serialAttributeCategory",
    "tAttribute1",
    "tAttribute2",
    "tAttribute3",
    "tAttribute4",
    "tAttribute5"
})
public class InvSerialsInterface {

    @XmlElement(name = "FromSerialNumber")
    protected String fromSerialNumber;
    @XmlElementRef(name = "ToSerialNumber", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> toSerialNumber;
    @XmlElementRef(name = "SubinventoryCode", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> subinventoryCode;
    @XmlElementRef(name = "Locator", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> locator;
    @XmlElementRef(name = "Attribute1", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> attribute1;
    @XmlElementRef(name = "Attribute10", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> attribute10;
    @XmlElementRef(name = "Attribute11", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> attribute11;
    @XmlElementRef(name = "Attribute12", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> attribute12;
    @XmlElementRef(name = "Attribute13", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> attribute13;
    @XmlElementRef(name = "Attribute14", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> attribute14;
    @XmlElementRef(name = "Attribute15", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> attribute15;
    @XmlElementRef(name = "Attribute16", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> attribute16;
    @XmlElementRef(name = "Attribute17", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> attribute17;
    @XmlElementRef(name = "Attribute18", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> attribute18;
    @XmlElementRef(name = "Attribute19", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> attribute19;
    @XmlElementRef(name = "Attribute2", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> attribute2;
    @XmlElementRef(name = "Attribute20", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> attribute20;
    @XmlElementRef(name = "Attribute3", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> attribute3;
    @XmlElementRef(name = "Attribute4", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> attribute4;
    @XmlElementRef(name = "Attribute5", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> attribute5;
    @XmlElementRef(name = "Attribute6", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> attribute6;
    @XmlElementRef(name = "Attribute7", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> attribute7;
    @XmlElementRef(name = "Attribute8", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> attribute8;
    @XmlElementRef(name = "Attribute9", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> attribute9;
    @XmlElementRef(name = "AttributeCategory", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> attributeCategory;
    @XmlElementRef(name = "AttributeDate1", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<XMLGregorianCalendar> attributeDate1;
    @XmlElementRef(name = "AttributeDate2", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<XMLGregorianCalendar> attributeDate2;
    @XmlElementRef(name = "AttributeDate3", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<XMLGregorianCalendar> attributeDate3;
    @XmlElementRef(name = "AttributeDate4", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<XMLGregorianCalendar> attributeDate4;
    @XmlElementRef(name = "AttributeDate5", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<XMLGregorianCalendar> attributeDate5;
    @XmlElementRef(name = "AttributeNumber1", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<BigDecimal> attributeNumber1;
    @XmlElementRef(name = "AttributeNumber10", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<BigDecimal> attributeNumber10;
    @XmlElementRef(name = "AttributeNumber2", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<BigDecimal> attributeNumber2;
    @XmlElementRef(name = "AttributeNumber3", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<BigDecimal> attributeNumber3;
    @XmlElementRef(name = "AttributeNumber4", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<BigDecimal> attributeNumber4;
    @XmlElementRef(name = "AttributeNumber5", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<BigDecimal> attributeNumber5;
    @XmlElementRef(name = "AttributeNumber6", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<BigDecimal> attributeNumber6;
    @XmlElementRef(name = "AttributeNumber7", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<BigDecimal> attributeNumber7;
    @XmlElementRef(name = "AttributeNumber8", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<BigDecimal> attributeNumber8;
    @XmlElementRef(name = "AttributeNumber9", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<BigDecimal> attributeNumber9;
    @XmlElementRef(name = "AttributeTimestamp1", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<XMLGregorianCalendar> attributeTimestamp1;
    @XmlElementRef(name = "AttributeTimestamp2", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<XMLGregorianCalendar> attributeTimestamp2;
    @XmlElementRef(name = "AttributeTimestamp3", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<XMLGregorianCalendar> attributeTimestamp3;
    @XmlElementRef(name = "AttributeTimestamp4", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<XMLGregorianCalendar> attributeTimestamp4;
    @XmlElementRef(name = "AttributeTimestamp5", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<XMLGregorianCalendar> attributeTimestamp5;
    @XmlElementRef(name = "CAttribute1", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> cAttribute1;
    @XmlElementRef(name = "CAttribute10", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> cAttribute10;
    @XmlElementRef(name = "CAttribute11", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> cAttribute11;
    @XmlElementRef(name = "CAttribute12", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> cAttribute12;
    @XmlElementRef(name = "CAttribute13", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> cAttribute13;
    @XmlElementRef(name = "CAttribute14", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> cAttribute14;
    @XmlElementRef(name = "CAttribute15", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> cAttribute15;
    @XmlElementRef(name = "CAttribute16", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> cAttribute16;
    @XmlElementRef(name = "CAttribute17", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> cAttribute17;
    @XmlElementRef(name = "CAttribute18", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> cAttribute18;
    @XmlElementRef(name = "CAttribute19", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> cAttribute19;
    @XmlElementRef(name = "CAttribute2", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> cAttribute2;
    @XmlElementRef(name = "CAttribute20", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> cAttribute20;
    @XmlElementRef(name = "CAttribute3", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> cAttribute3;
    @XmlElementRef(name = "CAttribute4", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> cAttribute4;
    @XmlElementRef(name = "CAttribute5", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> cAttribute5;
    @XmlElementRef(name = "CAttribute6", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> cAttribute6;
    @XmlElementRef(name = "CAttribute7", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> cAttribute7;
    @XmlElementRef(name = "CAttribute8", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> cAttribute8;
    @XmlElementRef(name = "CAttribute9", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> cAttribute9;
    @XmlElementRef(name = "DAttribute1", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<XMLGregorianCalendar> dAttribute1;
    @XmlElementRef(name = "DAttribute10", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<XMLGregorianCalendar> dAttribute10;
    @XmlElementRef(name = "DAttribute2", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<XMLGregorianCalendar> dAttribute2;
    @XmlElementRef(name = "DAttribute3", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<XMLGregorianCalendar> dAttribute3;
    @XmlElementRef(name = "DAttribute4", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<XMLGregorianCalendar> dAttribute4;
    @XmlElementRef(name = "DAttribute5", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<XMLGregorianCalendar> dAttribute5;
    @XmlElementRef(name = "DAttribute6", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<XMLGregorianCalendar> dAttribute6;
    @XmlElementRef(name = "DAttribute7", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<XMLGregorianCalendar> dAttribute7;
    @XmlElementRef(name = "DAttribute8", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<XMLGregorianCalendar> dAttribute8;
    @XmlElementRef(name = "DAttribute9", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<XMLGregorianCalendar> dAttribute9;
    @XmlElementRef(name = "NAttribute1", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<BigDecimal> nAttribute1;
    @XmlElementRef(name = "NAttribute10", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<BigDecimal> nAttribute10;
    @XmlElementRef(name = "NAttribute2", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<BigDecimal> nAttribute2;
    @XmlElementRef(name = "NAttribute3", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<BigDecimal> nAttribute3;
    @XmlElementRef(name = "NAttribute4", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<BigDecimal> nAttribute4;
    @XmlElementRef(name = "NAttribute5", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<BigDecimal> nAttribute5;
    @XmlElementRef(name = "NAttribute6", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<BigDecimal> nAttribute6;
    @XmlElementRef(name = "NAttribute7", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<BigDecimal> nAttribute7;
    @XmlElementRef(name = "NAttribute8", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<BigDecimal> nAttribute8;
    @XmlElementRef(name = "NAttribute9", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<BigDecimal> nAttribute9;
    @XmlElementRef(name = "SerialAttributeCategory", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<String> serialAttributeCategory;
    @XmlElementRef(name = "TAttribute1", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<XMLGregorianCalendar> tAttribute1;
    @XmlElementRef(name = "TAttribute2", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<XMLGregorianCalendar> tAttribute2;
    @XmlElementRef(name = "TAttribute3", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<XMLGregorianCalendar> tAttribute3;
    @XmlElementRef(name = "TAttribute4", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<XMLGregorianCalendar> tAttribute4;
    @XmlElementRef(name = "TAttribute5", namespace = "http://xmlns.oracle.com/apps/scm/shipping/shipConfirm/deliveries/shipmentService/", type = JAXBElement.class, required = false)
    protected JAXBElement<XMLGregorianCalendar> tAttribute5;

    /**
     * Gets the value of the fromSerialNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFromSerialNumber() {
        return fromSerialNumber;
    }

    /**
     * Sets the value of the fromSerialNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFromSerialNumber(String value) {
        this.fromSerialNumber = value;
    }

    /**
     * Gets the value of the toSerialNumber property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getToSerialNumber() {
        return toSerialNumber;
    }

    /**
     * Sets the value of the toSerialNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setToSerialNumber(JAXBElement<String> value) {
        this.toSerialNumber = value;
    }

    /**
     * Gets the value of the subinventoryCode property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getSubinventoryCode() {
        return subinventoryCode;
    }

    /**
     * Sets the value of the subinventoryCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setSubinventoryCode(JAXBElement<String> value) {
        this.subinventoryCode = value;
    }

    /**
     * Gets the value of the locator property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getLocator() {
        return locator;
    }

    /**
     * Sets the value of the locator property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setLocator(JAXBElement<String> value) {
        this.locator = value;
    }

    /**
     * Gets the value of the attribute1 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getAttribute1() {
        return attribute1;
    }

    /**
     * Sets the value of the attribute1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setAttribute1(JAXBElement<String> value) {
        this.attribute1 = value;
    }

    /**
     * Gets the value of the attribute10 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getAttribute10() {
        return attribute10;
    }

    /**
     * Sets the value of the attribute10 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setAttribute10(JAXBElement<String> value) {
        this.attribute10 = value;
    }

    /**
     * Gets the value of the attribute11 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getAttribute11() {
        return attribute11;
    }

    /**
     * Sets the value of the attribute11 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setAttribute11(JAXBElement<String> value) {
        this.attribute11 = value;
    }

    /**
     * Gets the value of the attribute12 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getAttribute12() {
        return attribute12;
    }

    /**
     * Sets the value of the attribute12 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setAttribute12(JAXBElement<String> value) {
        this.attribute12 = value;
    }

    /**
     * Gets the value of the attribute13 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getAttribute13() {
        return attribute13;
    }

    /**
     * Sets the value of the attribute13 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setAttribute13(JAXBElement<String> value) {
        this.attribute13 = value;
    }

    /**
     * Gets the value of the attribute14 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getAttribute14() {
        return attribute14;
    }

    /**
     * Sets the value of the attribute14 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setAttribute14(JAXBElement<String> value) {
        this.attribute14 = value;
    }

    /**
     * Gets the value of the attribute15 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getAttribute15() {
        return attribute15;
    }

    /**
     * Sets the value of the attribute15 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setAttribute15(JAXBElement<String> value) {
        this.attribute15 = value;
    }

    /**
     * Gets the value of the attribute16 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getAttribute16() {
        return attribute16;
    }

    /**
     * Sets the value of the attribute16 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setAttribute16(JAXBElement<String> value) {
        this.attribute16 = value;
    }

    /**
     * Gets the value of the attribute17 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getAttribute17() {
        return attribute17;
    }

    /**
     * Sets the value of the attribute17 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setAttribute17(JAXBElement<String> value) {
        this.attribute17 = value;
    }

    /**
     * Gets the value of the attribute18 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getAttribute18() {
        return attribute18;
    }

    /**
     * Sets the value of the attribute18 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setAttribute18(JAXBElement<String> value) {
        this.attribute18 = value;
    }

    /**
     * Gets the value of the attribute19 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getAttribute19() {
        return attribute19;
    }

    /**
     * Sets the value of the attribute19 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setAttribute19(JAXBElement<String> value) {
        this.attribute19 = value;
    }

    /**
     * Gets the value of the attribute2 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getAttribute2() {
        return attribute2;
    }

    /**
     * Sets the value of the attribute2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setAttribute2(JAXBElement<String> value) {
        this.attribute2 = value;
    }

    /**
     * Gets the value of the attribute20 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getAttribute20() {
        return attribute20;
    }

    /**
     * Sets the value of the attribute20 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setAttribute20(JAXBElement<String> value) {
        this.attribute20 = value;
    }

    /**
     * Gets the value of the attribute3 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getAttribute3() {
        return attribute3;
    }

    /**
     * Sets the value of the attribute3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setAttribute3(JAXBElement<String> value) {
        this.attribute3 = value;
    }

    /**
     * Gets the value of the attribute4 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getAttribute4() {
        return attribute4;
    }

    /**
     * Sets the value of the attribute4 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setAttribute4(JAXBElement<String> value) {
        this.attribute4 = value;
    }

    /**
     * Gets the value of the attribute5 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getAttribute5() {
        return attribute5;
    }

    /**
     * Sets the value of the attribute5 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setAttribute5(JAXBElement<String> value) {
        this.attribute5 = value;
    }

    /**
     * Gets the value of the attribute6 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getAttribute6() {
        return attribute6;
    }

    /**
     * Sets the value of the attribute6 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setAttribute6(JAXBElement<String> value) {
        this.attribute6 = value;
    }

    /**
     * Gets the value of the attribute7 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getAttribute7() {
        return attribute7;
    }

    /**
     * Sets the value of the attribute7 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setAttribute7(JAXBElement<String> value) {
        this.attribute7 = value;
    }

    /**
     * Gets the value of the attribute8 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getAttribute8() {
        return attribute8;
    }

    /**
     * Sets the value of the attribute8 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setAttribute8(JAXBElement<String> value) {
        this.attribute8 = value;
    }

    /**
     * Gets the value of the attribute9 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getAttribute9() {
        return attribute9;
    }

    /**
     * Sets the value of the attribute9 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setAttribute9(JAXBElement<String> value) {
        this.attribute9 = value;
    }

    /**
     * Gets the value of the attributeCategory property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getAttributeCategory() {
        return attributeCategory;
    }

    /**
     * Sets the value of the attributeCategory property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setAttributeCategory(JAXBElement<String> value) {
        this.attributeCategory = value;
    }

    /**
     * Gets the value of the attributeDate1 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public JAXBElement<XMLGregorianCalendar> getAttributeDate1() {
        return attributeDate1;
    }

    /**
     * Sets the value of the attributeDate1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public void setAttributeDate1(JAXBElement<XMLGregorianCalendar> value) {
        this.attributeDate1 = value;
    }

    /**
     * Gets the value of the attributeDate2 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public JAXBElement<XMLGregorianCalendar> getAttributeDate2() {
        return attributeDate2;
    }

    /**
     * Sets the value of the attributeDate2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public void setAttributeDate2(JAXBElement<XMLGregorianCalendar> value) {
        this.attributeDate2 = value;
    }

    /**
     * Gets the value of the attributeDate3 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public JAXBElement<XMLGregorianCalendar> getAttributeDate3() {
        return attributeDate3;
    }

    /**
     * Sets the value of the attributeDate3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public void setAttributeDate3(JAXBElement<XMLGregorianCalendar> value) {
        this.attributeDate3 = value;
    }

    /**
     * Gets the value of the attributeDate4 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public JAXBElement<XMLGregorianCalendar> getAttributeDate4() {
        return attributeDate4;
    }

    /**
     * Sets the value of the attributeDate4 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public void setAttributeDate4(JAXBElement<XMLGregorianCalendar> value) {
        this.attributeDate4 = value;
    }

    /**
     * Gets the value of the attributeDate5 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public JAXBElement<XMLGregorianCalendar> getAttributeDate5() {
        return attributeDate5;
    }

    /**
     * Sets the value of the attributeDate5 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public void setAttributeDate5(JAXBElement<XMLGregorianCalendar> value) {
        this.attributeDate5 = value;
    }

    /**
     * Gets the value of the attributeNumber1 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}
     *     
     */
    public JAXBElement<BigDecimal> getAttributeNumber1() {
        return attributeNumber1;
    }

    /**
     * Sets the value of the attributeNumber1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}
     *     
     */
    public void setAttributeNumber1(JAXBElement<BigDecimal> value) {
        this.attributeNumber1 = value;
    }

    /**
     * Gets the value of the attributeNumber10 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}
     *     
     */
    public JAXBElement<BigDecimal> getAttributeNumber10() {
        return attributeNumber10;
    }

    /**
     * Sets the value of the attributeNumber10 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}
     *     
     */
    public void setAttributeNumber10(JAXBElement<BigDecimal> value) {
        this.attributeNumber10 = value;
    }

    /**
     * Gets the value of the attributeNumber2 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}
     *     
     */
    public JAXBElement<BigDecimal> getAttributeNumber2() {
        return attributeNumber2;
    }

    /**
     * Sets the value of the attributeNumber2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}
     *     
     */
    public void setAttributeNumber2(JAXBElement<BigDecimal> value) {
        this.attributeNumber2 = value;
    }

    /**
     * Gets the value of the attributeNumber3 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}
     *     
     */
    public JAXBElement<BigDecimal> getAttributeNumber3() {
        return attributeNumber3;
    }

    /**
     * Sets the value of the attributeNumber3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}
     *     
     */
    public void setAttributeNumber3(JAXBElement<BigDecimal> value) {
        this.attributeNumber3 = value;
    }

    /**
     * Gets the value of the attributeNumber4 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}
     *     
     */
    public JAXBElement<BigDecimal> getAttributeNumber4() {
        return attributeNumber4;
    }

    /**
     * Sets the value of the attributeNumber4 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}
     *     
     */
    public void setAttributeNumber4(JAXBElement<BigDecimal> value) {
        this.attributeNumber4 = value;
    }

    /**
     * Gets the value of the attributeNumber5 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}
     *     
     */
    public JAXBElement<BigDecimal> getAttributeNumber5() {
        return attributeNumber5;
    }

    /**
     * Sets the value of the attributeNumber5 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}
     *     
     */
    public void setAttributeNumber5(JAXBElement<BigDecimal> value) {
        this.attributeNumber5 = value;
    }

    /**
     * Gets the value of the attributeNumber6 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}
     *     
     */
    public JAXBElement<BigDecimal> getAttributeNumber6() {
        return attributeNumber6;
    }

    /**
     * Sets the value of the attributeNumber6 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}
     *     
     */
    public void setAttributeNumber6(JAXBElement<BigDecimal> value) {
        this.attributeNumber6 = value;
    }

    /**
     * Gets the value of the attributeNumber7 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}
     *     
     */
    public JAXBElement<BigDecimal> getAttributeNumber7() {
        return attributeNumber7;
    }

    /**
     * Sets the value of the attributeNumber7 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}
     *     
     */
    public void setAttributeNumber7(JAXBElement<BigDecimal> value) {
        this.attributeNumber7 = value;
    }

    /**
     * Gets the value of the attributeNumber8 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}
     *     
     */
    public JAXBElement<BigDecimal> getAttributeNumber8() {
        return attributeNumber8;
    }

    /**
     * Sets the value of the attributeNumber8 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}
     *     
     */
    public void setAttributeNumber8(JAXBElement<BigDecimal> value) {
        this.attributeNumber8 = value;
    }

    /**
     * Gets the value of the attributeNumber9 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}
     *     
     */
    public JAXBElement<BigDecimal> getAttributeNumber9() {
        return attributeNumber9;
    }

    /**
     * Sets the value of the attributeNumber9 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}
     *     
     */
    public void setAttributeNumber9(JAXBElement<BigDecimal> value) {
        this.attributeNumber9 = value;
    }

    /**
     * Gets the value of the attributeTimestamp1 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public JAXBElement<XMLGregorianCalendar> getAttributeTimestamp1() {
        return attributeTimestamp1;
    }

    /**
     * Sets the value of the attributeTimestamp1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public void setAttributeTimestamp1(JAXBElement<XMLGregorianCalendar> value) {
        this.attributeTimestamp1 = value;
    }

    /**
     * Gets the value of the attributeTimestamp2 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public JAXBElement<XMLGregorianCalendar> getAttributeTimestamp2() {
        return attributeTimestamp2;
    }

    /**
     * Sets the value of the attributeTimestamp2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public void setAttributeTimestamp2(JAXBElement<XMLGregorianCalendar> value) {
        this.attributeTimestamp2 = value;
    }

    /**
     * Gets the value of the attributeTimestamp3 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public JAXBElement<XMLGregorianCalendar> getAttributeTimestamp3() {
        return attributeTimestamp3;
    }

    /**
     * Sets the value of the attributeTimestamp3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public void setAttributeTimestamp3(JAXBElement<XMLGregorianCalendar> value) {
        this.attributeTimestamp3 = value;
    }

    /**
     * Gets the value of the attributeTimestamp4 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public JAXBElement<XMLGregorianCalendar> getAttributeTimestamp4() {
        return attributeTimestamp4;
    }

    /**
     * Sets the value of the attributeTimestamp4 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public void setAttributeTimestamp4(JAXBElement<XMLGregorianCalendar> value) {
        this.attributeTimestamp4 = value;
    }

    /**
     * Gets the value of the attributeTimestamp5 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public JAXBElement<XMLGregorianCalendar> getAttributeTimestamp5() {
        return attributeTimestamp5;
    }

    /**
     * Sets the value of the attributeTimestamp5 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public void setAttributeTimestamp5(JAXBElement<XMLGregorianCalendar> value) {
        this.attributeTimestamp5 = value;
    }

    /**
     * Gets the value of the cAttribute1 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCAttribute1() {
        return cAttribute1;
    }

    /**
     * Sets the value of the cAttribute1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCAttribute1(JAXBElement<String> value) {
        this.cAttribute1 = value;
    }

    /**
     * Gets the value of the cAttribute10 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCAttribute10() {
        return cAttribute10;
    }

    /**
     * Sets the value of the cAttribute10 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCAttribute10(JAXBElement<String> value) {
        this.cAttribute10 = value;
    }

    /**
     * Gets the value of the cAttribute11 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCAttribute11() {
        return cAttribute11;
    }

    /**
     * Sets the value of the cAttribute11 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCAttribute11(JAXBElement<String> value) {
        this.cAttribute11 = value;
    }

    /**
     * Gets the value of the cAttribute12 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCAttribute12() {
        return cAttribute12;
    }

    /**
     * Sets the value of the cAttribute12 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCAttribute12(JAXBElement<String> value) {
        this.cAttribute12 = value;
    }

    /**
     * Gets the value of the cAttribute13 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCAttribute13() {
        return cAttribute13;
    }

    /**
     * Sets the value of the cAttribute13 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCAttribute13(JAXBElement<String> value) {
        this.cAttribute13 = value;
    }

    /**
     * Gets the value of the cAttribute14 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCAttribute14() {
        return cAttribute14;
    }

    /**
     * Sets the value of the cAttribute14 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCAttribute14(JAXBElement<String> value) {
        this.cAttribute14 = value;
    }

    /**
     * Gets the value of the cAttribute15 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCAttribute15() {
        return cAttribute15;
    }

    /**
     * Sets the value of the cAttribute15 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCAttribute15(JAXBElement<String> value) {
        this.cAttribute15 = value;
    }

    /**
     * Gets the value of the cAttribute16 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCAttribute16() {
        return cAttribute16;
    }

    /**
     * Sets the value of the cAttribute16 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCAttribute16(JAXBElement<String> value) {
        this.cAttribute16 = value;
    }

    /**
     * Gets the value of the cAttribute17 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCAttribute17() {
        return cAttribute17;
    }

    /**
     * Sets the value of the cAttribute17 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCAttribute17(JAXBElement<String> value) {
        this.cAttribute17 = value;
    }

    /**
     * Gets the value of the cAttribute18 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCAttribute18() {
        return cAttribute18;
    }

    /**
     * Sets the value of the cAttribute18 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCAttribute18(JAXBElement<String> value) {
        this.cAttribute18 = value;
    }

    /**
     * Gets the value of the cAttribute19 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCAttribute19() {
        return cAttribute19;
    }

    /**
     * Sets the value of the cAttribute19 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCAttribute19(JAXBElement<String> value) {
        this.cAttribute19 = value;
    }

    /**
     * Gets the value of the cAttribute2 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCAttribute2() {
        return cAttribute2;
    }

    /**
     * Sets the value of the cAttribute2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCAttribute2(JAXBElement<String> value) {
        this.cAttribute2 = value;
    }

    /**
     * Gets the value of the cAttribute20 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCAttribute20() {
        return cAttribute20;
    }

    /**
     * Sets the value of the cAttribute20 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCAttribute20(JAXBElement<String> value) {
        this.cAttribute20 = value;
    }

    /**
     * Gets the value of the cAttribute3 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCAttribute3() {
        return cAttribute3;
    }

    /**
     * Sets the value of the cAttribute3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCAttribute3(JAXBElement<String> value) {
        this.cAttribute3 = value;
    }

    /**
     * Gets the value of the cAttribute4 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCAttribute4() {
        return cAttribute4;
    }

    /**
     * Sets the value of the cAttribute4 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCAttribute4(JAXBElement<String> value) {
        this.cAttribute4 = value;
    }

    /**
     * Gets the value of the cAttribute5 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCAttribute5() {
        return cAttribute5;
    }

    /**
     * Sets the value of the cAttribute5 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCAttribute5(JAXBElement<String> value) {
        this.cAttribute5 = value;
    }

    /**
     * Gets the value of the cAttribute6 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCAttribute6() {
        return cAttribute6;
    }

    /**
     * Sets the value of the cAttribute6 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCAttribute6(JAXBElement<String> value) {
        this.cAttribute6 = value;
    }

    /**
     * Gets the value of the cAttribute7 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCAttribute7() {
        return cAttribute7;
    }

    /**
     * Sets the value of the cAttribute7 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCAttribute7(JAXBElement<String> value) {
        this.cAttribute7 = value;
    }

    /**
     * Gets the value of the cAttribute8 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCAttribute8() {
        return cAttribute8;
    }

    /**
     * Sets the value of the cAttribute8 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCAttribute8(JAXBElement<String> value) {
        this.cAttribute8 = value;
    }

    /**
     * Gets the value of the cAttribute9 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCAttribute9() {
        return cAttribute9;
    }

    /**
     * Sets the value of the cAttribute9 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCAttribute9(JAXBElement<String> value) {
        this.cAttribute9 = value;
    }

    /**
     * Gets the value of the dAttribute1 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public JAXBElement<XMLGregorianCalendar> getDAttribute1() {
        return dAttribute1;
    }

    /**
     * Sets the value of the dAttribute1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public void setDAttribute1(JAXBElement<XMLGregorianCalendar> value) {
        this.dAttribute1 = value;
    }

    /**
     * Gets the value of the dAttribute10 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public JAXBElement<XMLGregorianCalendar> getDAttribute10() {
        return dAttribute10;
    }

    /**
     * Sets the value of the dAttribute10 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public void setDAttribute10(JAXBElement<XMLGregorianCalendar> value) {
        this.dAttribute10 = value;
    }

    /**
     * Gets the value of the dAttribute2 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public JAXBElement<XMLGregorianCalendar> getDAttribute2() {
        return dAttribute2;
    }

    /**
     * Sets the value of the dAttribute2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public void setDAttribute2(JAXBElement<XMLGregorianCalendar> value) {
        this.dAttribute2 = value;
    }

    /**
     * Gets the value of the dAttribute3 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public JAXBElement<XMLGregorianCalendar> getDAttribute3() {
        return dAttribute3;
    }

    /**
     * Sets the value of the dAttribute3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public void setDAttribute3(JAXBElement<XMLGregorianCalendar> value) {
        this.dAttribute3 = value;
    }

    /**
     * Gets the value of the dAttribute4 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public JAXBElement<XMLGregorianCalendar> getDAttribute4() {
        return dAttribute4;
    }

    /**
     * Sets the value of the dAttribute4 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public void setDAttribute4(JAXBElement<XMLGregorianCalendar> value) {
        this.dAttribute4 = value;
    }

    /**
     * Gets the value of the dAttribute5 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public JAXBElement<XMLGregorianCalendar> getDAttribute5() {
        return dAttribute5;
    }

    /**
     * Sets the value of the dAttribute5 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public void setDAttribute5(JAXBElement<XMLGregorianCalendar> value) {
        this.dAttribute5 = value;
    }

    /**
     * Gets the value of the dAttribute6 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public JAXBElement<XMLGregorianCalendar> getDAttribute6() {
        return dAttribute6;
    }

    /**
     * Sets the value of the dAttribute6 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public void setDAttribute6(JAXBElement<XMLGregorianCalendar> value) {
        this.dAttribute6 = value;
    }

    /**
     * Gets the value of the dAttribute7 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public JAXBElement<XMLGregorianCalendar> getDAttribute7() {
        return dAttribute7;
    }

    /**
     * Sets the value of the dAttribute7 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public void setDAttribute7(JAXBElement<XMLGregorianCalendar> value) {
        this.dAttribute7 = value;
    }

    /**
     * Gets the value of the dAttribute8 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public JAXBElement<XMLGregorianCalendar> getDAttribute8() {
        return dAttribute8;
    }

    /**
     * Sets the value of the dAttribute8 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public void setDAttribute8(JAXBElement<XMLGregorianCalendar> value) {
        this.dAttribute8 = value;
    }

    /**
     * Gets the value of the dAttribute9 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public JAXBElement<XMLGregorianCalendar> getDAttribute9() {
        return dAttribute9;
    }

    /**
     * Sets the value of the dAttribute9 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public void setDAttribute9(JAXBElement<XMLGregorianCalendar> value) {
        this.dAttribute9 = value;
    }

    /**
     * Gets the value of the nAttribute1 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}
     *     
     */
    public JAXBElement<BigDecimal> getNAttribute1() {
        return nAttribute1;
    }

    /**
     * Sets the value of the nAttribute1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}
     *     
     */
    public void setNAttribute1(JAXBElement<BigDecimal> value) {
        this.nAttribute1 = value;
    }

    /**
     * Gets the value of the nAttribute10 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}
     *     
     */
    public JAXBElement<BigDecimal> getNAttribute10() {
        return nAttribute10;
    }

    /**
     * Sets the value of the nAttribute10 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}
     *     
     */
    public void setNAttribute10(JAXBElement<BigDecimal> value) {
        this.nAttribute10 = value;
    }

    /**
     * Gets the value of the nAttribute2 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}
     *     
     */
    public JAXBElement<BigDecimal> getNAttribute2() {
        return nAttribute2;
    }

    /**
     * Sets the value of the nAttribute2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}
     *     
     */
    public void setNAttribute2(JAXBElement<BigDecimal> value) {
        this.nAttribute2 = value;
    }

    /**
     * Gets the value of the nAttribute3 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}
     *     
     */
    public JAXBElement<BigDecimal> getNAttribute3() {
        return nAttribute3;
    }

    /**
     * Sets the value of the nAttribute3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}
     *     
     */
    public void setNAttribute3(JAXBElement<BigDecimal> value) {
        this.nAttribute3 = value;
    }

    /**
     * Gets the value of the nAttribute4 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}
     *     
     */
    public JAXBElement<BigDecimal> getNAttribute4() {
        return nAttribute4;
    }

    /**
     * Sets the value of the nAttribute4 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}
     *     
     */
    public void setNAttribute4(JAXBElement<BigDecimal> value) {
        this.nAttribute4 = value;
    }

    /**
     * Gets the value of the nAttribute5 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}
     *     
     */
    public JAXBElement<BigDecimal> getNAttribute5() {
        return nAttribute5;
    }

    /**
     * Sets the value of the nAttribute5 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}
     *     
     */
    public void setNAttribute5(JAXBElement<BigDecimal> value) {
        this.nAttribute5 = value;
    }

    /**
     * Gets the value of the nAttribute6 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}
     *     
     */
    public JAXBElement<BigDecimal> getNAttribute6() {
        return nAttribute6;
    }

    /**
     * Sets the value of the nAttribute6 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}
     *     
     */
    public void setNAttribute6(JAXBElement<BigDecimal> value) {
        this.nAttribute6 = value;
    }

    /**
     * Gets the value of the nAttribute7 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}
     *     
     */
    public JAXBElement<BigDecimal> getNAttribute7() {
        return nAttribute7;
    }

    /**
     * Sets the value of the nAttribute7 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}
     *     
     */
    public void setNAttribute7(JAXBElement<BigDecimal> value) {
        this.nAttribute7 = value;
    }

    /**
     * Gets the value of the nAttribute8 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}
     *     
     */
    public JAXBElement<BigDecimal> getNAttribute8() {
        return nAttribute8;
    }

    /**
     * Sets the value of the nAttribute8 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}
     *     
     */
    public void setNAttribute8(JAXBElement<BigDecimal> value) {
        this.nAttribute8 = value;
    }

    /**
     * Gets the value of the nAttribute9 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}
     *     
     */
    public JAXBElement<BigDecimal> getNAttribute9() {
        return nAttribute9;
    }

    /**
     * Sets the value of the nAttribute9 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}
     *     
     */
    public void setNAttribute9(JAXBElement<BigDecimal> value) {
        this.nAttribute9 = value;
    }

    /**
     * Gets the value of the serialAttributeCategory property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getSerialAttributeCategory() {
        return serialAttributeCategory;
    }

    /**
     * Sets the value of the serialAttributeCategory property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setSerialAttributeCategory(JAXBElement<String> value) {
        this.serialAttributeCategory = value;
    }

    /**
     * Gets the value of the tAttribute1 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public JAXBElement<XMLGregorianCalendar> getTAttribute1() {
        return tAttribute1;
    }

    /**
     * Sets the value of the tAttribute1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public void setTAttribute1(JAXBElement<XMLGregorianCalendar> value) {
        this.tAttribute1 = value;
    }

    /**
     * Gets the value of the tAttribute2 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public JAXBElement<XMLGregorianCalendar> getTAttribute2() {
        return tAttribute2;
    }

    /**
     * Sets the value of the tAttribute2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public void setTAttribute2(JAXBElement<XMLGregorianCalendar> value) {
        this.tAttribute2 = value;
    }

    /**
     * Gets the value of the tAttribute3 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public JAXBElement<XMLGregorianCalendar> getTAttribute3() {
        return tAttribute3;
    }

    /**
     * Sets the value of the tAttribute3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public void setTAttribute3(JAXBElement<XMLGregorianCalendar> value) {
        this.tAttribute3 = value;
    }

    /**
     * Gets the value of the tAttribute4 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public JAXBElement<XMLGregorianCalendar> getTAttribute4() {
        return tAttribute4;
    }

    /**
     * Sets the value of the tAttribute4 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public void setTAttribute4(JAXBElement<XMLGregorianCalendar> value) {
        this.tAttribute4 = value;
    }

    /**
     * Gets the value of the tAttribute5 property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public JAXBElement<XMLGregorianCalendar> getTAttribute5() {
        return tAttribute5;
    }

    /**
     * Sets the value of the tAttribute5 property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public void setTAttribute5(JAXBElement<XMLGregorianCalendar> value) {
        this.tAttribute5 = value;
    }

}

package com.integration.utils;

import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.apache.log4j.Logger;

public class PropertiesReaderUtil {
	
    private static Logger log = Logger.getLogger(PropertiesReaderUtil.class);
    private static Properties settingsProps;

    public static String getProperty(String key) {
        if (settingsProps == null) {
            settingsProps = getPropertiesFile("sakintegration.properties");
        }
        return settingsProps.getProperty(key);
    }

    public static Map<String, String> getMappingAsMap(
            String mappingPropertiesFile) {
        Map<String, String> mapping = new HashMap<>();
        Properties props = getPropertiesFile(mappingPropertiesFile);

        for (String key : props.stringPropertyNames()) {
            String value = props.getProperty(key);
            mapping.put(key, value);
        }

        return mapping;
    }

    private static Properties getPropertiesFile(String propertiesFile) {
        try {
            Properties props = new Properties();
            InputStream is = Thread.currentThread().getContextClassLoader()
                    .getResourceAsStream(propertiesFile);
            props.load(is);
            return props;
        } catch (Exception e) {
            log.fatal(propertiesFile + " not found."+ e);
        }
		return settingsProps;
    }

}

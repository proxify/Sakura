package com.integration.osc;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import com.integration.utils.PropertiesReaderUtil;

public class CreateOrg {

	public String OrgCreation (String orgName) throws Exception {
		// TODO Auto-generated method stub
		String soapXml = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\">\r\n" + 
				"   <soapenv:Header>\r\n" + 
				"      <ns7:ClientInfoHeader soapenv:mustUnderstand=\"0\" xmlns:ns7=\"urn:messages.ws.rightnow.com/v1_4\">\r\n" + 
				"         <ns7:AppID>Basic Create</ns7:AppID>\r\n" + 
				"      </ns7:ClientInfoHeader>\r\n" + 
				"      <wsse:Security mustUnderstand=\"1\" xmlns:wsse=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd\">\r\n" + 
				"         <wsse:UsernameToken>\r\n" + 
				"            <wsse:Username>"+PropertiesReaderUtil.getProperty("sakintegration.username")+"</wsse:Username>\r\n" + 
				"            <wsse:Password Type=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText\">"+
				PropertiesReaderUtil.getProperty("sakintegration.password")+"</wsse:Password>\r\n" + 
				"         </wsse:UsernameToken>\r\n" + 
				"      </wsse:Security>\r\n" + 
				"   </soapenv:Header>\r\n" + 
				"   <soapenv:Body>\r\n" + 
				"      <ns7:Create xmlns:ns7=\"urn:messages.ws.rightnow.com/v1_4\">\r\n" + 
				"         <ns7:RNObjects xsi:type=\"ns4:Organization\" xmlns:ns4=\"urn:objects.ws.rightnow.com/v1_4\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">\r\n" + 
				"            <ns4:Name>"+ orgName +"</ns4:Name>\r\n" + 
				"         </ns7:RNObjects>\r\n" + 
				"         <ns7:ProcessingOptions>\r\n" + 
				"            <ns7:SuppressExternalEvents>false</ns7:SuppressExternalEvents>\r\n" + 
				"            <ns7:SuppressRules>false</ns7:SuppressRules>\r\n" + 
				"         </ns7:ProcessingOptions>\r\n" + 
				"      </ns7:Create>\r\n" + 
				"   </soapenv:Body>\r\n" + 
				"</soapenv:Envelope>";
		URL url = new URL(
				"https://sakuraus-rnow--tst1.custhelp.com/services/soap/connect/soap");
//		String auth = Base64.getEncoder().encodeToString((username + ":" + password).getBytes());
		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		conn.setRequestMethod("POST");
		conn.setDoOutput(true);
		conn.setDoInput(true);
		conn.setRequestProperty("User-Agent", "Apache-HttpClient/4.1.1 (java 1.5)");
//		conn.setRequestProperty("Authorization", "Basic " + auth);
		conn.setRequestProperty("Host", "sakuraus-rnow--tst1.custhelp.com");
		conn.setRequestProperty("Connection", "Keep-Alive");
		conn.setRequestProperty("Content-Length", String.valueOf(soapXml.getBytes().length));
		conn.setRequestProperty("Content-Type", "text/xml;charset=UTF-8");
		conn.setRequestProperty("SOAPAction","Create");
		try (DataOutputStream wr = new DataOutputStream(conn.getOutputStream())) {
			wr.write(soapXml.getBytes());
		}
		// Read the response
		BufferedReader rd;
		System.out.println("conn.getResponseCode()-->" + conn.getResponseCode());
		if (200 <= conn.getResponseCode() && conn.getResponseCode() <= 299) {
			rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
		} else {
			rd = new BufferedReader(new InputStreamReader(conn.getErrorStream()));
		}
		String line;
		String id=null;
		while ((line = rd.readLine()) != null) {
			System.out.println(line);
			if(line.indexOf("id=\"")!=-1) {
				id=line.substring(line.indexOf("id=\"")+4, line.indexOf("\"", line.indexOf("id=\"")+4));
			}
		}
		return id;
	}
	
	public static void main(String args[]) throws Exception {
		CreateOrg c = new CreateOrg();
		String result =c.OrgCreation("testorg1");
		System.out.println("result--->"+result);
	}
}

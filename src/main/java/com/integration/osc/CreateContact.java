package com.integration.osc;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;

import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.integration.utils.PropertiesReaderUtil;

public class CreateContact {

	private static final Logger log = Logger.getLogger(CreateContact.class);
	public String[] idValues = new String[2];
	int counter = 0;

	public String[] contactCreation(String fName, String lName, String email, String description, String orgId)
			throws Exception {
		// TODO Auto-generated method stub
		String soapXml = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\">\r\n"
				+ "    <soapenv:Header>\r\n"
				+ "        <ns7:ClientInfoHeader xmlns:ns7=\"urn:messages.ws.rightnow.com/v1_4\" soapenv:mustUnderstand=\"0\">\r\n"
				+ "            <ns7:AppID>Basic Create</ns7:AppID>\r\n" + "        </ns7:ClientInfoHeader>\r\n"
				+ "        <wsse:Security xmlns:wsse=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd\" mustUnderstand=\"1\">\r\n"
				+ "            <wsse:UsernameToken>\r\n" + "                <wsse:Username>"
				+ PropertiesReaderUtil.getProperty("sakintegration.username") + "</wsse:Username>\r\n"
				+ "                <wsse:Password Type=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText\">"
				+ PropertiesReaderUtil.getProperty("sakintegration.password") + "</wsse:Password>\r\n"
				+ "            </wsse:UsernameToken>\r\n" + "        </wsse:Security>\r\n" + "    </soapenv:Header>\r\n"
				+ "    <soapenv:Body>\r\n" + "        <ns7:Create xmlns:ns7=\"urn:messages.ws.rightnow.com/v1_4\">\r\n"
				+ "            <ns7:RNObjects xmlns:ns4=\"urn:objects.ws.rightnow.com/v1_4\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:type=\"ns4:Contact\">\r\n"
				+ "                <ns4:Emails>\r\n" + "                    <ns4:EmailList action=\"add\">\r\n"
				+ "                        <ns4:Address>" + email + "</ns4:Address>\r\n"
				+ "                        <ns4:AddressType>\r\n"
				+ "                            <ID xmlns=\"urn:base.ws.rightnow.com/v1_4\" id=\"0\" />\r\n"
				+ "                        </ns4:AddressType>\r\n" + "                    </ns4:EmailList>\r\n"
				+ "                </ns4:Emails>\r\n" + "                <ns4:Name>\r\n"
				+ "                    <ns4:First>" + fName + "</ns4:First>\r\n" + "                    <ns4:Last>"
				+ lName + "</ns4:Last>\r\n" + "                </ns4:Name>\r\n<ns4:Organization action=\"add\">\r\n"
				+ "               <ID id=\""+ orgId +"\" xmlns=\"urn:base.ws.rightnow.com/v1_4\"/>\r\n"
				+ "            </ns4:Organization>\r\n" + "" + "            </ns7:RNObjects>\r\n"
				+ "            <ns7:RNObjects xmlns:ns4=\"urn:objects.ws.rightnow.com/v1_4\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:type=\"ns4:Incident\">\r\n"
				+ "                <ns4:PrimaryContact>\r\n" + "                    <ns4:Contact>\r\n"
				+ "                        <ID xmlns=\"urn:base.ws.rightnow.com/v1_4\" id=\"16\" />\r\n"
				+ "                    </ns4:Contact>\r\n" + "                </ns4:PrimaryContact>\r\n"
				+ "                <ns4:Subject>" + description + "</ns4:Subject>\r\n"
				+ "            </ns7:RNObjects>\r\n" + "            <ns7:ProcessingOptions>\r\n"
				+ "                <ns7:SuppressExternalEvents>false</ns7:SuppressExternalEvents>\r\n"
				+ "                <ns7:SuppressRules>false</ns7:SuppressRules>\r\n"
				+ "            </ns7:ProcessingOptions>\r\n" + "        </ns7:Create>\r\n" + "    </soapenv:Body>\r\n"
				+ "</soapenv:Envelope>";
		URL url = new URL("https://sakuraus-rnow--tst1.custhelp.com/services/soap/connect/soap");
		// String auth = Base64.getEncoder().encodeToString((username + ":" +
		// password).getBytes());
		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		conn.setRequestMethod("POST");
		conn.setDoOutput(true);
		conn.setDoInput(true);
		conn.setRequestProperty("User-Agent", "Apache-HttpClient/4.1.1 (java 1.5)");
		// conn.setRequestProperty("Authorization", "Basic " + auth);
		conn.setRequestProperty("Host", "sakuraus-rnow--tst1.custhelp.com");
		conn.setRequestProperty("Connection", "Keep-Alive");
		conn.setRequestProperty("Content-Length", String.valueOf(soapXml.getBytes().length));
		conn.setRequestProperty("Content-Type", "text/xml;charset=UTF-8");
		conn.setRequestProperty("SOAPAction", "Create");
		try (DataOutputStream wr = new DataOutputStream(conn.getOutputStream())) {
			wr.write(soapXml.getBytes());
		}
		// Read the response
		BufferedReader rd;
		System.out.println("conn.getResponseCode()-->" + conn.getResponseCode());
		if (200 <= conn.getResponseCode() && conn.getResponseCode() <= 299) {
			parseresult(conn.getInputStream());
			rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
		} else {
			rd = new BufferedReader(new InputStreamReader(conn.getErrorStream()));
		}
		System.out.println("idValues-------->" + idValues[0] + "--" + idValues[1]);
		return idValues;
	}

	private boolean parseresult(InputStream is) {
		log.info("Starting copy process");
		try {
			Document doc;
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			doc = dBuilder.parse(is);
			doc.getDocumentElement().normalize();
			Element rootExisting = doc.getDocumentElement();
			doParse(rootExisting);
		} catch (Exception e) {
			e.printStackTrace();
			log.error("Error in copying from input file to output " + e.getMessage());
			return false;
		}
		log.info("Ending copy process");
		return true;
	}

	public static void main(String args[]) throws Exception {
		CreateContact c = new CreateContact();
		c.contactCreation("firstname", "lastname", "test32@gmail.com", "Description3","");
	}

	private void doParse(Node node) {

		// Applying removal criteria
		// log.info(node.getNodeName());
		if (node.getNodeName().contains("RNObjects")) {
			if (node.hasChildNodes()) {
				NodeList nodeList = node.getChildNodes();
				log.info("length--->" + nodeList.getLength());
				for (int i = 0; i < nodeList.getLength(); i++) {
					Node currentNode = nodeList.item(i);
					log.info(currentNode.getNodeName());
					log.info("i--->" + i);
					Node idNode = currentNode.getChildNodes().item(0);
					Node idValue = idNode.getAttributes().getNamedItem("id");
					System.out.println("idValue-------->" + idValue);
					System.out.println("idValue-------->" + idValue.getNodeValue());
					idValues[counter++] = idValue.getNodeValue();
				}
			}
			return;
		}
		// Traversing child nodes
		if (node.hasChildNodes()) {
			NodeList nodeList = node.getChildNodes();
			for (int i = 0; i < nodeList.getLength(); i++) {
				Node currentNode = nodeList.item(i);
				if (currentNode.getNodeType() == Node.ELEMENT_NODE) {
					doParse(currentNode);
				}
			}
		}
	}
}

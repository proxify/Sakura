package com.integration.controller;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;

import com.integration.osc.CreateContact;
import com.integration.osc.CreateOrg;
import com.integration.services.response.Response;

@Path("/services")
public class IntegrationController {
	@GET
	@Path("/Createorg")
	public Response Createorg(@QueryParam("orgName") String orgName) throws Exception {
		Response response = new Response();
		if (true) {
			response.setMessage((new CreateOrg()).OrgCreation(orgName));
			response.setStatus(true);
		} else
			response.setMessage("Failed!");
		return response;
	}

	@GET
	@Path("/Createcontact")
	public Response[] Createcontact(@QueryParam("firstName") String firstName,@QueryParam("lastName") String lastName,
			@QueryParam("email") String email, @QueryParam("subject") String subject, @QueryParam("orgId") String orgId) throws Exception {
		Response[] responses = new Response[2];
		int counter =0;
		CreateContact conCreation = new CreateContact();
		String[] result = conCreation.contactCreation(firstName, lastName, email, subject, orgId);
		if (result.length >0) {
			for(String res : result) {
				Response response = new Response();
				response.setMessage(result[counter]);
				response.setStatus(true);
				responses[counter++]=response;
			}
		} else {
			Response response = new Response();
			response.setMessage("Failed!");
			responses[counter++]=response;
		}
		return responses;
	}

}

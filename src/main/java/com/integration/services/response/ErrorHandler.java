package com.integration.services.response;

public class ErrorHandler {

	private String errorMessage;
	private Object exceptionObj;
	private Integer errorCode;
	
	public ErrorHandler(String errorMessage, Object exceptionObj, Integer errorCode) {
		this.errorMessage = errorMessage;
		this.exceptionObj = exceptionObj;
		this.errorCode = errorCode;
	}
	
	public ErrorHandler(String errorMessage, Object exceptionObj) {
		this.errorMessage = errorMessage;
		this.exceptionObj = exceptionObj;
	}

	public ErrorHandler(String errorMessage, Integer errorCode) {
		this.errorMessage = errorMessage;
		this.errorCode = errorCode;
	}
	
	public ErrorHandler(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public String getErrorMessage() {
		return errorMessage;
	}
	
	public Object getExceptionObj() {
		return exceptionObj;
	}
	
	public Integer getErrorCode() {
		return errorCode;
	}
	
}

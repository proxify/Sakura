package com.integration.services.response;

import javax.ws.rs.core.Response;

public class ResponseHandler {

	public static Response prepareResponse(Object object) {
		if(object instanceof ErrorHandler) {
			ErrorHandler error = (ErrorHandler) object;
			if(error.getErrorCode() == null) {
				return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(object).build();
			} else {
				return Response.status(error.getErrorCode()).entity(object).build();
			}
		} else {
			return Response.status(Response.Status.OK).entity(object).build();
		}
	}
	
}
